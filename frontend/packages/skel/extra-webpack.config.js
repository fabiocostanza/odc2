const p = require("plug-webpack-plugin");

var plug = new p.PlugPlugin({
    src: './packages/skel/src/app/controllers/**/*.component.ts',
    base: './packages/skel/src/app/controllers'
});

module.exports = {
    plugins: [plug]
};