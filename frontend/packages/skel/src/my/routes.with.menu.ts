import { Routes } from '@angular/router';
import { HomepageComponent } from '../app/controllers/homepage/homepage.component';

export const myRoutes: Routes = [
//    { path: '', redirectTo: 'home', pathMatch: 'full'  },
    { path: 'settings', component: HomepageComponent, data: { label: 'Home BUBU', selector: 'my' } }
];
