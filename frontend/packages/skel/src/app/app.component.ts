import { Component, AfterViewInit } from '@angular/core';
import { AclCanActivateChild, DataState } from 'core';
import { Location } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { EnvironmentService } from 'core';

import _ from 'lodash';
import { AcldService } from 'core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  
  title = 'skel';
  headerClass = 'hidden';
  appName = environment.appName;
  currentSpinner = 'timer';
  @Select(DataState.loading) loading$: Observable<boolean>;
  constructor(
    public acldService: AcldService,
    public loc: Location,
    private guard: AclCanActivateChild,
    private spinnerService: NgxSpinnerService,
    public environmentService: EnvironmentService
  ) {
   
  }

  getEnvName() {
    const env = this.environmentService.environment;

    let tmp = '';

    switch (env) {
      case 'dev':
        tmp = 'Sviluppo';
        break;
      case 'sta':
        tmp = 'Collaudo';
        break;
      case 'pro':
        tmp = 'Produzione';
        break;
      default:
      // tmp = 'Sconosciuto';
    }
    return tmp;
  }
  getClassName() {
    const env = this.environmentService.environment;

    if (!env) {
      return 'envPlug';
    }

    return _.capitalize(env);
  }

  ngAfterViewInit() {
    /**
     * Dopo la navigazione causata dai controlli di abilitazione del menu si redirige alla route iniziale
     * (vedi AclCanActivateChild).
     * Questo è dovuto al fatto che la navigazione del router utilizza uno switchMap e quindi la navigazione
     * di default e sovrascritta da quella fatta per il controllo del menu
     */
    this.acldService.acldHasChanged.subscribe(() => {
      const path = this.loc.path();

      this.guard.urls.next(path);
      this.loading$.subscribe(x => {
        if (!x) {
          setTimeout(() => {
            this.headerClass = '';
          });
          this.spinnerService.hide();
        } else {
          this.headerClass = 'hidden';
          this.spinnerService.show();
        }
      });
    });
  }
}
