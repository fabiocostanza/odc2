
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector, Injectable, APP_INITIALIZER } from '@angular/core';

import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {
  AclCanActivateChild,
  setAppInjector,
  HttpInterceptorProvider,
  AcldService,
  DataState,
  CoreModule,
  STORE_SERVICE_CONFIG,
  DATA_FAIL,
  ACL_PERMISSIONS_DISABLE
} from 'core';

import {
  LayoutModule,
  layoutMenu,
  MenuService,
  ResourcesService,
  Resource,
  DialogsModule,
  DialogsService
} from 'layout';

import { appRoutes } from '../routes.with.menu';
import { myRoutes } from '../../my/routes.with.menu';
import { Observable } from 'rxjs';
import { concat, take, reduce } from 'rxjs/operators';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { NgxsModule } from '@ngxs/store';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { EnvironmentService } from 'core';
// const appResources = require('./assets/json/resources.json');
// const appResources = [];

export function init(acld: AcldService, environment: EnvironmentService) {
  const x = () => {
    acld.refresh();
    environment.refresh();
  }
  return x;
}

const routes = [
  {
    path: '',
    canActivateChild: [AclCanActivateChild],
    children: [...layoutMenu, ...appRoutes]
    /*  [
      { path: 'layout', children: layoutMenu },
      { path: 'app', children: appRoutes }
    ]*/
  }
];

@Injectable({
  providedIn: 'root'
})
export class ResourcesServiceStub extends ResourcesService {
  constructor(protected http: HttpClient) {
    super(http);
  }
  getResources(): Observable<Resource[]> {
    //    return this.http.get('assets/json/plug-resources.json');
    return super.getResources().pipe(
      concat(this.http.get<Resource[]>('assets/json/resources.json')),
      take(2),
      reduce((acc, resources) => (Array.isArray(acc) && Array.isArray(resources) ? [...acc, ...resources] : resources))
    );
  }
}

@Injectable({
  providedIn: 'root'
})
export class MenuServiceStub extends MenuService {
  getMenu(): Routes {
    const menu = super.getMenu();
    const r = [
      {
        path: '',
        data: {
          label: '<span class="bnl bnl-navicon-light bnl-2x topMenu"></span>',
          selector: '*'
        },
        children: [...menu, ...appRoutes, ...myRoutes]
      }
    ];
    console.log(r);
    return r;
  }
  getRoutes(): Routes {
    return routes;
  }
}

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    BrowserModule,
    LayoutModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,
    FormlyModule.forRoot(),
    FormlyMaterialModule,
    NgxsModule.forRoot([DataState]),
    CoreModule,
    NgxDatatableModule,
    DialogsModule
  ],
  exports: [
    LayoutModule
  ],
  providers: [
    { provide: MenuService, useClass: MenuServiceStub },
    { provide: ResourcesService, useClass: ResourcesServiceStub },
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorProvider, multi: true },
    { provide: APP_INITIALIZER, useFactory: init, deps: [AcldService, EnvironmentService], multi: true },
    { provide: STORE_SERVICE_CONFIG, useValue: { replace: true, url: '/api' } },
   { provide: ACL_PERMISSIONS_DISABLE, useValue: true },
    { provide: DATA_FAIL, useClass: DialogsService, multi: true }
  ]
})
export class BaseAppModule {
  constructor(injector: Injector) {
    setAppInjector(injector);
  }
}
