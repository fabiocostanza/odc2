import { Component, ViewChild, ViewContainerRef } from '@angular/core';
import { FieldType } from '@ngx-formly/core';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent extends FieldType {
  step = 0;

  /**
   * Callback to invoke when an active tab is collapsed by clicking on the header.
   * @param $event Tabview event
   */
  onClose(event) {
    if (this.to.onClose) {
      this.to.onClose(event);
    }
  }

  /**
   * Callback to invoke when a tab gets expanded.
   * @param $event Tabview event
   */
  onOpen(event) {
    if (this.to.onOpen) {
      this.to.onOpen(event);
    }
  }

  scrollToTop() {
    window.scrollTo(0, 0);
  }

  removeMonth(){
    console.log("removeMonth()");
  }

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

}
