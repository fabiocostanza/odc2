import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogsService, FormComponent, DataFormOptions, DataFieldConfig } from 'layout';
import { FormGroup } from '@angular/forms';
import { Location } from '@angular/common';
import { MutationType } from 'core';

@Component({
  selector: 'app-doc-create-edit',
  templateUrl: './doc-create-edit.component.html',
  styleUrls: ['./doc-create-edit.component.css']
})
export class DocCreateEditComponent implements OnInit {
  constructor(public location: Location, public router: Router, public route: ActivatedRoute, public dialogs: DialogsService) {}

  id_documento = '';
  id_doc_info = '';
  mutation: MutationType = 'create';

  @ViewChild(FormComponent) formComponent: FormComponent;

  form = new FormGroup({});
  model: any = {};
  options: DataFormOptions = {
    data: {
      model: 'doccensiti', 
      mutation: () => this.mutation,
     // params: () => (this.id_documento ? { id_documento: this.id_documento } : {})
      params: () => ({ id_documento: this.id_documento, id_doc_info: this.id_doc_info})
    }
  };
  fields: DataFieldConfig[] = [
    {
      fieldGroupClassName: 'row display-flex',
      fieldGroup: [
        // { key: 'id_riunione', defaultValue: '1' },
        {
          key: 'titolo',
          type: 'textarea',
          className: 'col-lg-7 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Titolo documento',
            label: 'Titolo documento',
            rows: 3,
            required: true
          }
        },
        {
          key: 'id_doc_info', 
          type: 'input',
          hide: true       
        },
        {
          key: 'id_documento', 
          type: 'input',
          hide: true       
        },        
        {
          key: 'periodicita',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: '0',
          templateOptions: {
            label: 'Periodicita',
             data:{
               model: 'hperiodicita'
             },
             change: (field, $event) => {                
                  //azzero il valore dei campi in base alla tipologia di periodicita
                  if (field.form.controls.periodicita.value === '0' || field.form.controls.periodicita.value === '1' ) {
                    field.form.controls.presentazione.setValue('');
                  }  
            },
            labelProp: 'descrizione_periodicita',
            valueProp: 'id_periodicita',
            required: true
          }
        },
        {
          key: 'presentazione',
          type: 'select',
          className: 'col-lg-3 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          expressionProperties: {
            'templateOptions.disabled': 'model.periodicita === "0" || model.periodicita === "1"',
          },
          templateOptions: {
            label: 'Mesi presentazione',
            options: [
              { descrizione_mese: 'Gennaio', mese: '1' },
              { descrizione_mese: 'Febbraio', mese: '2' },
              { descrizione_mese: 'Marzo', mese: '3' },
              { descrizione_mese: 'Aprile', mese: '4' },
              { descrizione_mese: 'Maggio', mese: '5' },
              { descrizione_mese: 'Giugno', mese: '6' },
              { descrizione_mese: 'Luglio', mese: '7' },
              { descrizione_mese: 'Agosto', mese: '8' },
              { descrizione_mese: 'Settembre', mese: '9' },
              { descrizione_mese: 'Ottobre', mese: '10' },
              { descrizione_mese: 'Novembre', mese: '11' },
              { descrizione_mese: 'Dicembre', mese: '12' }
            ],
            labelProp: 'descrizione_mese',
            valueProp: 'mese',
            multiple: true
          }
        },
        {
          key: 'funzioni',
          type: 'select',
          className: 'col-lg-7 col-md-6 col-sm-12 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Funzioni',            
             data:{
               model: 'hfunzioni'
              },
            labelProp: 'descrizione',
            valueProp: 'id_funzione',
            multiple: true
          }
        }, 
        {
          key: 'organi',
          type: 'select',
          className: 'col-lg-3 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Organo',
             data:{
               model: 'horgani'
             },        
            labelProp: 'descrizione_organo',
            valueProp: 'id_organo',
            multiple: true
          }
        },   
        {
          key: 'sessioni',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          // expressionProperties: {
          //   'templateOptions.disabled': 'model.organi != "1"',
          // },
          templateOptions: {
            label: 'Sessione',
             data:{
               model: 'hsessioni'
             },           
            labelProp: 'descrizione',
            valueProp: 'id_sessione',
          }
        },         
        {
          key: 'descrizione',
          type: 'textarea',
          className: 'col-lg-12 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Descrizione',
            label: 'Descrizione',
            rows: 3
          }
        },
        {
          key: 'norma_interna',
          type: 'input',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Norma interna',
            label: 'Norma interna',
          }
        },
        {
          key: 'norma_esterna',
          type: 'input',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Norma esterna',
            label: 'Norma esterna',
          }
        },
        {
          key: 'finalita',
          type: 'textarea',
          className: 'col-lg-12 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Finalità',
            label: 'Finalità',
            rows: 3,
          }
        },
      ]
    }
  ];

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      this.id_documento = params.id_documento;
      this.id_doc_info = params.id_doc_info;
       if (this.id_documento) {
         this.mutation = 'update';
       }
    });
  }

  submitAction() {
    if (this.form.valid) {
      this.formComponent.flush().subscribe(() => {
        this.dialogs.success('Salvataggio avvenuto con successo').subscribe(ref => {
          this.location.back();
        });
      });
    } else {
      this.dialogs.warning('Tutti i campi obbligatori devono essere compilati correttamente');
    }
  }
}
