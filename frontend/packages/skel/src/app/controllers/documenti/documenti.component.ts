import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { TableColumn } from '@swimlane/ngx-datatable';
import { ActivatedRoute } from '@angular/router';
import {
  CellTemplateComponent,
  InputFilterComponent,
  DialogsService,
  FormComponent,
  DataFormOptions,
  DataFieldConfig,
  PLUG_DATA_TABLE_CHECKBOXES,
  TableComponent,
  SelectFilterComponent,
  SelectFilterContext
} from 'layout';
import { FormGroup } from '@angular/forms';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatDialogRef } from '@angular/material';
import { StateDataService, MutationType, CookiesService } from 'core';
import { ExcelServiceService } from '../../services/excel-service.service';

@Component({
  selector: "gestioneDocumenti",
  templateUrl: './documenti.component.html',
  styleUrls: ['./documenti.component.scss']
})
export class DocumentiComponent implements OnInit, OnDestroy {
  @ViewChild(TableComponent) table: TableComponent;

  columns: TableColumn[] = [
    PLUG_DATA_TABLE_CHECKBOXES,
    { name: 'Cod.', prop: 'codiceDocumento', maxWidth: 100, sortable: true, headerComponent: InputFilterComponent },
    {
      name: 'Titolo',
      prop: 'titolo',
      sortable: true,
      headerComponent: InputFilterComponent,
      headerClass: 'myHeaderClass'
    },
    {
      name: 'Organi',
      prop: 'descrizione_organo',
      sortable: true,
      headerComponent: SelectFilterComponent,
      headerClass: 'myHeaderClass',
      customContext: {
        filter: {
          model: '/horgani',
          prop: 'descrizione_organo',
          value: 'descrizione_organo',
          label: 'descrizione_organo'
        } as SelectFilterContext
      }
    },
    {
      name: 'Funzioni',
      prop: 'descrizione_funzioni',
      sortable: true,
      headerComponent: SelectFilterComponent,
      headerClass: 'myHeaderClass',
      customContext: {
        filter: {
          model: '/hfunzioni',
          prop: 'descrizione_funzioni',
          value: 'descrizione',
          label: 'descrizione'
        } as SelectFilterContext
      }
    },
    {
      name: 'Periodicità',
      prop: 'descrizione_periodicita',
      sortable: true,
      headerComponent: SelectFilterComponent,
      headerClass: 'myHeaderClass',
      customContext: {
        filter: {
          model: '/hperiodicita',
          prop: 'descrizione_periodicita',
          value: 'descrizione_periodicita',
          label: 'descrizione_periodicita'
        } as SelectFilterContext
      }
    },
    {
      name: 'Mesi pres.',
      prop: 'mesi',
      sortable: true,
      headerComponent: SelectFilterComponent,
      headerClass: 'myHeaderClass',
      customContext: {
        filter: {
          model: '/hmesi',
          prop: 'mesi',
        //  multi: true,
          value: 'nome_mese',
          label: 'nome_mese'
        } as SelectFilterContext
      }
    },
    {
      name: 'Attivo',
      prop: 'attivo',
      sortable: true,
      cellComponent: CellTemplateComponent, customContext: { 
        filter: {
          model: '/hattivo',
          prop: 'attivo',
          value: 'attivo_str',
          label: 'attivo_str'
        } as SelectFilterContext,
        cell: { template: 'statusTemplate' } },
      headerComponent: SelectFilterComponent,
      headerClass: 'myHeaderClass'
    },  
    {
      name: '',
      cellComponent: CellTemplateComponent,
      maxWidth: 60,
      customContext: { cell: { template: 'actionsTemplate' } },
      sortable: false
    }
  ];


  constructor(public dialogs: DialogsService, public router: ActivatedRoute, private http: StateDataService, private cookie: CookiesService, private excel: ExcelServiceService) { 

    

  }

  id_documento = '';
  id_doc_info = '';
  codiceDocumento = '';
  destroy$ = new Subject();
  selected = [];
  mutation: MutationType = 'create';

  form = new FormGroup({});
  model: any = {};
  //list_azioni = [{"descrizione":"Approva","id_azione":"1"},{"descrizione":"Prende visione","id_azione":"2"},{"descrizione":"Parere","id_azione":"3"},{"descrizione":"Delibera","id_azione":"4"},{"descrizione":"Valida","id_azione":"5"}];
  list_azioni: any;


  @ViewChild('addDocument') addDocumentForm: FormComponent;
  @ViewChild('editDocument') EditDocumentForm: FormComponent;

  options: DataFormOptions = {
    data: {
      model: '/doccensitilist',
      mutation: 'update',
      params: () => ({
        id_documento: this.id_documento,
        id_doc_info: this.id_doc_info
      })
    }
  };

  optionsaddDoc: DataFormOptions = {
    data: {
      model: 'doccensiti',
      mutation: () => this.mutation,
      // params: () => (this.id_documento ? { id_documento: this.id_documento } : {})
      params: () => ({ id_documento: this.id_documento, id_doc_info: this.id_doc_info })
    }
  };

  optionseditDoc: DataFormOptions = {
    data: {
      model: 'doccensiti',
      mutation: () => 'update',
      // params: () => (this.id_documento ? { id_documento: this.id_documento } : {})
      params: () => ({ id_documento: this.id_documento, id_doc_info: this.id_doc_info })
    }
  };

  dialogFields: DataFieldConfig[] = [
    {
      fieldGroupClassName: 'row display-flex',
      fieldGroup: [
        {
          key: 'cod_tipo_doc',
          type: 'select',
          className: 'col-lg-7 col-md-6 col-sm-12 p-2',
          defaultValue: 'd',
          templateOptions: {
            label: 'Tipo documento',
            data: {
              model: 'htipodoc'
            },
            labelProp: 'descrizione',
            valueProp: 'id_tipo_doc',
            required: true
          }
        },
        {
          key: 'id_doc_info',
          type: 'input',
          hide: true
        },
        {
          key: 'id_documento',
          type: 'input',
          hide: true
        },
        {
          key: 'titolo',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Titolo documento',
            label: 'Titolo documento',
            rows: 3,
            required: true
          }
        },
        {
          key: 'finalita',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Finalità',
            label: 'Finalità',
            rows: 3,
          }
        },
        {
          key: 'periodicita',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: '0',
          templateOptions: {
            label: 'Periodicita',
            data: {
              model: 'hperiodicita'
            },
            change: (field, $event) => {
              // azzero il valore dei campi in base alla tipologia di periodicita
              if (field.form.controls.periodicita.value === '0' || field.form.controls.periodicita.value === '1') {
                field.form.controls.presentazione.setValue('');
              }
            },
            labelProp: 'descrizione_periodicita',
            valueProp: 'id_periodicita',
            required: true
          }
        },
        {
          key: 'presentazione',
          type: 'select',
          className: 'col-lg-3 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          expressionProperties: {
            'templateOptions.disabled': 'model.periodicita === "0" || model.periodicita === "1"',
          },
          templateOptions: {
            label: 'Mesi presentazione',
            options: [
              { descrizione_mese: 'Gennaio', mese: '1' },
              { descrizione_mese: 'Febbraio', mese: '2' },
              { descrizione_mese: 'Marzo', mese: '3' },
              { descrizione_mese: 'Aprile', mese: '4' },
              { descrizione_mese: 'Maggio', mese: '5' },
              { descrizione_mese: 'Giugno', mese: '6' },
              { descrizione_mese: 'Luglio', mese: '7' },
              { descrizione_mese: 'Agosto', mese: '8' },
              { descrizione_mese: 'Settembre', mese: '9' },
              { descrizione_mese: 'Ottobre', mese: '10' },
              { descrizione_mese: 'Novembre', mese: '11' },
              { descrizione_mese: 'Dicembre', mese: '12' }
            ],
            labelProp: 'descrizione_mese',
            valueProp: 'mese',
            multiple: true
          }
        },
        {
          key: 'note_presentazione',
          type: 'textareahtml',
          className: 'col-lg-7 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Note periodicità',
            label: 'Note periodicità',
            rows: 3
          }
        },
        {
          key: 'funzioni',
          type: 'select',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Funzioni',
            data: {
              model: 'hfunzioni'
            },
            labelProp: 'descrizione',
            valueProp: 'id_funzione',
            multiple: true
          }
        },
        {
          key: 'contributrici',
          type: 'select',
          className: 'col-lg-4 col-md-12 col-sm-12 p-2',
          defaultValue: [],
          templateOptions: {
            placeholder: 'Funzioni contributrici',
            label: 'Funzioni contributrici',
            data: {
              model: 'hfunzioni'
            },
            labelProp: 'descrizione',
            valueProp: 'id_funzione',
            multiple: true
          }
        },
        {
          key: 'destinatari',
          type: 'select',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Destinatari',
            data: {
              model: 'hdestinatari'
            },
            labelProp: 'descrizione_destinatario',
            valueProp: 'id_destinatario',
            multiple: true,
          }
        },
        {
          key: 'organo1',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'CCIR / Sessione OdV (CCIR)',
            // data: {
            //   model: 'azioni'
            // },
            options: [],  
            labelProp: 'descrizione',
            valueProp: 'id_azione',
            multiple: true
          }
        },
        {
          key: 'sessioni',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          expressionProperties: {
            // 'templateOptions.disabled': '!model.organo1.includes("1")',
            'templateOptions.disabled': 'model.organo1.length == 0',
          },
          // hideExpression: function (model: any) {
          //   console.log(model.organo1.length);
          //   if (model.organo1) {
          //     return false;
          //   }            
          //   return true;
          // },
          templateOptions: {
            label: 'Sessione',
            data: {
              model: 'hsessioni'
            },
            labelProp: 'descrizione',
            valueProp: 'id_sessione',
            multiple: true
          }
        },
        {
          key: 'organo2',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          templateOptions: {
           label: 'Collegio Sindacale (CS)',
           options: [],           
           labelProp: 'descrizione',
           valueProp: 'id_azione',
           multiple: true
          },
        },
        {
          key: 'organo3',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Consiglio di Amministrazione (CDA)',
            options: [],  
            labelProp: 'descrizione',
            valueProp: 'id_azione',
            multiple: true
          }
        },
        {
          key: 'organo4',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Comitato Nomine (CN)',
            options: [],  
            labelProp: 'descrizione',
            valueProp: 'id_azione',
            multiple: true
          }
        },
        {
          key: 'organo5',
          type: 'select',
          className: 'col-lg-2 col-md-4 col-sm-6 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Comitato Remunerazioni (CR)',
            options: [],  
            labelProp: 'descrizione',
            valueProp: 'id_azione',
            multiple: true
          }
        },
        {
          key: 'norma_interna',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Norma interna',
            label: 'Norma interna',
            rows: 3
          }
        },
        {
          key: 'norma_esterna',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Norma esterna',
            label: 'Norma esterna',
            rows: 3
          }
        },
        {
          key: 'note',
          type: 'textareahtml',
          className: 'col-lg-12 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Note',
            label: 'Note',
            rows: 3
          }
        }
      ]
    }
  ];

  tableChange() {
    console.log('tabella cambiata');
  }

  ngOnInit() {
    this.router.params.subscribe(param => {
      this.id_documento = param.id_documento;
    });
  }

  ngAfterViewInit() {
    this.http.read('hazioni')
             .subscribe(res => {
               this.list_azioni = res.data;
               this.dialogFields[0].fieldGroup[11].templateOptions.options = this.list_azioni;
               this.dialogFields[0].fieldGroup[13].templateOptions.options = this.list_azioni;
               this.dialogFields[0].fieldGroup[14].templateOptions.options = this.list_azioni;
               this.dialogFields[0].fieldGroup[15].templateOptions.options = this.list_azioni;
               this.dialogFields[0].fieldGroup[16].templateOptions.options = this.list_azioni;
             })
   }

  ngOnDestroy() {
    this.destroy$.next(true);
  }

  removeAction() {
    this.dialogs
      .confirm('Sei sicuro di voler rimuovere gli elementi selezionati?')
      .subscribe(res => {
        if (res) {
          this.table.data
            .delete('/doccensiti', { id: this.table.selected.map(r => r.id) })
            .pipe(takeUntil(this.destroy$))
            .subscribe(x => {
              this.table.selected = [];
              this.table.data.refresh();
            });
        }
      });
  }

  scaricaDati() {
    this.http.read('doccensitiexport')
      .subscribe(res => {
        this.excel.exportAsExcelFile(Object.values(res.data), 'documenti');
      });
  }

  addDocAction(ref: MatDialogRef<any, any>) {
    if (this.form.valid) {
      this.addDocumentForm
        .flush()
        .subscribe((x) => {
          this.router.params.subscribe((params: any) => {
            this.http.refresh();
          });
          this.table.selected = [];
          this.dialogs.success('Documento aggiunto nel database!').subscribe(() => {
            this.table.data.refresh();
            this.closeForm(ref, true)});
        });
    } else {
      this.dialogs.warning('Compilare i dati obbligatori!');
    }
  }

  editDocAction(ref: MatDialogRef<any, any>) {
    if (this.form.valid) {
      // setto mese e dati per inserimento documento nella riunione
      // this.form.controls.mese.setValue(this.month);
      // this.form.controls.anno.setValue(this.year);
      // this.form.controls.id_organo.setValue(this.idorgano);
      // this.form.controls.uid_modifica.setValue(this.cookie.user.refoguid);
      this.EditDocumentForm
        .flush()
        .subscribe((x) => {
          this.router.params.subscribe((params: any) => {
            this.http.refresh();
          });
          this.dialogs.success('Documento modificato nel database!').subscribe(() => {
            this.table.data.refresh();
            this.closeForm(ref, true)});
        });
    } else {
      this.dialogs.warning('Compilare i dati obbligatori!');
    }
  }

  closeForm(ref: MatDialogRef<any, any>, ret?: any) {
    ref.close(ret);
  }

  paramDialog(id_documento: string, id_doc_info: string, codiceDocumento: string) {
    this.id_documento = id_documento;
    this.id_doc_info = id_doc_info;
    this.codiceDocumento = codiceDocumento;
  }

  // salvo in db lo stato del documento censito se è attivo
  setDocCensitoAttivo(id_doc_info: string, attivo: string): void {
    const params = {
      id_doc_info: id_doc_info,
      attivo: attivo,
      uid_modifica: this.cookie.user.refoguid.toString()
    };
    this.http
      .create('doccensitisetattivo', params, [])
      .subscribe(() => {
        this.table.data.refresh(); 
       });
  }
}
