import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocCreateEditComponent } from './doc-create-edit.component';

describe('DocCreateEditComponent', () => {
  let component: DocCreateEditComponent;
  let fixture: ComponentFixture<DocCreateEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocCreateEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocCreateEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
