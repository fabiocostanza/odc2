import { Component, OnInit } from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-organi',
  templateUrl: './organi.component.html',
  styleUrls: ['./organi.component.css']
})

export class OrganiComponent implements OnInit {
columns: TableColumn[] = [
  {name: 'codice',  prop:'codice_organo', sortable: true},
  {name: 'descrizione', prop:'descrizione_organo', sortable: true},
  {name: 'ordine', sortable: true}

];

  constructor() { }

  ngOnInit() {
  }

}
