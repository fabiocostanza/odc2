import { Component, ViewChild } from '@angular/core';
//import { FieldType } from '@ngx-formly/core';
import { FieldType } from '@ngx-formly/material';



@Component({
  selector: 'formly-field-custom-input',
  template: `
    <quill-editor placeholder="{{to.label}}  {{to.required? '*':''}}" [sanitize]="true" [modules]="{toolbar: false}" [formControl]="formControl" [formlyAttributes]="field">
    </quill-editor>
  `,
})



export class FieldQuillType extends FieldType {    
  get labelProp(): string { return this.to.labelProp || 'label'; }  
  get valueProp(): string { return this.to.valueProp || 'value'; }

  editorOptions = {
   // theme: 'snow',  // or 'bubble'
    toolbar: [
        ['bold', 'italic', 'underline'],        // toggled buttons
        // ['blockquote'],  
        // [{'header': 1}, {'header': 2}],               // custom button values
         [{'list': 'ordered'}, {'list': 'bullet'}],
        // [{'indent': '-1'}, {'indent': '+1'}],          // outdent/indent  
        // [{'size': ['small', false, 'large', 'huge']}],  // custom dropdown
        // [{'header': [1, 2, 3, 4, 5, 6, false]}],  
        // [{'color': []}, {'background': []}],          // dropdown with defaults from theme
        // [{'font': []}],
        // [{'align': []}],  
        ['clean']                                       // remove formatting button  
        // ['link']                   // link and image, video  
    ]
  };

  

 // [modules]="{toolbar: false}"


}

