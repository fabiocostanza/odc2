import {
    Component,
    ViewChild,
    ViewContainerRef,
    TemplateRef,
    AfterViewInit,
    Input,
    OnInit,
    OnDestroy
  } from '@angular/core';
  import { FieldWrapper } from '@ngx-formly/core';
  import { MatDialog, MatDialogConfig } from '@angular/material';
  import { ComponentType } from '@angular/cdk/portal';
  import { StateDataService } from 'core';
  import { Subscription } from 'rxjs';
  
  export interface DialogOptions {
    componentOrTemplate: ComponentType<any> | TemplateRef<any> | string;
    config?: MatDialogConfig;
    icon?: string;
  }
  
  @Component({
    selector: 'mat-dialog-wrapper',
    template: `
      <ng-container #fieldComponent></ng-container>
      <ng-template #matSuffix>
        <mat-icon *ngIf="target" (click)="openDialog()">{{ icon }}</mat-icon>
      </ng-template>
    `
  })
  export class MatDialogWrapperComponent extends FieldWrapper implements OnInit, OnDestroy, AfterViewInit {
    @ViewChild('fieldComponent', { read: ViewContainerRef }) fieldComponent: ViewContainerRef;
    @ViewChild('matSuffix') matSuffix: TemplateRef<any>;
    @Input()
    target: ComponentType<any> | TemplateRef<any>;
    @Input()
    icon = 'search';
    @Input()
    config: MatDialogConfig = { data: this.field, width: '100%', height: '100%' };
    protected sub: Subscription;
    constructor(public data: StateDataService, public dialog: MatDialog) {
      super();
    }
    ngOnInit() {
      if (this.to.dialog) {
        const dialogo: DialogOptions = this.to.dialog as DialogOptions;
        if (!dialogo.componentOrTemplate) {
          throw new Error('You must supply a compoent or a template!');
        }
        if (typeof dialogo.componentOrTemplate === 'string') {
          this.sub = this.data.getTemplate(dialogo.componentOrTemplate).subscribe(t => {
            this.target = t;
          });
        } else {
          this.target = dialogo.componentOrTemplate;
        }
        if (dialogo.icon) {
          this.icon = dialogo.icon;
        }
        if (dialogo.config) {
          this.config = dialogo.config;
        }
      }
    }
    ngAfterViewInit() {
      if (this.matSuffix) {
        setTimeout(() => (this.to.suffix = this.matSuffix));
      }
    }
    ngOnDestroy() {
      if (this.sub) {
        this.sub.unsubscribe();
      }
    }
    openDialog() {
      let dialogRef = this.dialog.open(this.target, this.config);
      dialogRef.afterClosed().subscribe(result => {
        if (result !== undefined) {
         // this.field.formControl.patchValue(result);
          this.form.patchValue({
            id_documento: result.id_documento,
            id_doc_info: result.id_doc_info,
            titolo: result.titolo,
            descrizione: result.descrizione,
            norma_interna: result.norma_interna,
            norma_esterna: result.norma_esterna,
            periodicita: result.periodicita,
            organi: result.organi,
         //   funzioni: result.funzioni,
            finalita: result.finalita,
            // other form fields
          })

        }
        dialogRef = null;
      });
    }
  }