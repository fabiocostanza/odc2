import { Injectable } from "@angular/core";
import {
  MatSnackBar,
  MatSnackBarConfig,
  // MatSnackBarHorizontalPosition,
  // MatSnackBarVerticalPosition,
  MatSnackBarRef
} from "@angular/material";

@Injectable()
export class SnackbarComponent {
  private snackBarConfig: MatSnackBarConfig;
  private snackBarRef: MatSnackBarRef<any>;
  private snackBarAutoHide = "4000"; //milliseconds for notification , 4 secs

  constructor(private sb: MatSnackBar) {}

  openSnackBar(message, type) {
    const _snackType = type !== undefined ? type : 'success';

    this.snackBarConfig = new MatSnackBarConfig();   
    this.snackBarConfig.horizontalPosition = 'end';
    this.snackBarConfig.verticalPosition = 'top';
    this.snackBarConfig.duration = parseInt(this.snackBarAutoHide, 0);
      this.sb.open(message, "", this.snackBarConfig);
  }
}