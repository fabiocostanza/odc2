import { Component, OnInit, OnDestroy, AfterViewInit, Input, Output, ViewChild, EventEmitter, ContentChildren, QueryList, ElementRef, Renderer2, ChangeDetectionStrategy, ChangeDetectorRef, TemplateRef } from '@angular/core';
import { DragDrop, DropListRef, DragRef, moveItemInArray } from '@angular/cdk/drag-drop';
import { Subject, of } from 'rxjs';
import { merge, takeUntil, startWith, map, pairwise, take } from 'rxjs/operators';

import { contentExpansion } from './collapse-panel-animations';
import { CollapseItemComponent } from '../collapse-item/collapse-item.component';
import { DragDropRegistryService } from '../../../services/drag-drop-registry.service';
import { DialogsService, FormComponent, DataFormOptions, DataFieldConfig, TableComponent, InputFilterComponent, CellTemplateComponent } from 'layout';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MatDialogConfig } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { StateDataService, CookiesService } from 'core';
import { DatePipe } from '@angular/common';
import { SnackbarComponent } from '../../snackbar/snackbar.component';
import { TableColumn } from '@swimlane/ngx-datatable';
import { DocumentoServiceService } from '../../../services/documento-service.service';




const REGISTRY_KEY = 'collapse-panels';

@Component({
  selector: 'app-collapse-panel',
  templateUrl: './collapse-panel.component.html',
  styleUrls: ['./collapse-panel.component.scss'],
  animations: [ contentExpansion ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CollapsePanelComponent<T = any> implements OnInit, AfterViewInit, OnDestroy {

  @Input() title: string;
  @Input() index: number;
  @Input() month: number;
  @Input() year: number;
  @Input() dateriunione: string;
  @Input() idriunione: number;
  @Input() idorgano: number;
  @Input() n_verbale: number;
  @Input() opened = false;
  @Input() arrow = false;
  @Input() dropListData: T[];
  @Output() dropListDropped: EventEmitter<any> = new EventEmitter();
  @Output() toggle = new Subject();

  @ViewChild('meseRiunione') meseRiunione: ElementRef;

  @ViewChild('body') body: ElementRef;
  @ViewChild('header') header: ElementRef;
  @ContentChildren(CollapseItemComponent) collapseItems: QueryList<CollapseItemComponent>;
  private bodyDropList: DropListRef<T[]>;
  private headerDropList: DropListRef<T[]>;
  private dragItems: DragRef<T>[] = [];
  private lastDroppedIndex: number;
  private lastExitedIndex: number;
  private destroyed$ = new Subject();
  private todayDate: Date = new Date();
  private positionDocument = [];
  public statoRiunione: string;
  public iconaRiunione: string;

  @ViewChild('documentilistTable') documentilistTable: TemplateRef<any>; 
  

  constructor(private renderer: Renderer2, private dragDrop: DragDrop, private dragDropRegistryService: DragDropRegistryService, 
    private cd: ChangeDetectorRef, private route: ActivatedRoute, private http: StateDataService, private datePipe: DatePipe,
    private cookie: CookiesService, private dialogs: DialogsService,  private sb: SnackbarComponent, public service: DocumentoServiceService
    ) { }


  // editorOptions= {
  //   toolbar: false
  // };

  formDataRiunione = new FormGroup({
    data_riunione: new FormControl((new Date()).toISOString(), [Validators.required]),
    num_verbale: new FormControl('')
  });



  form = new FormGroup({});
  //model: any = {};
  @ViewChild('addDocument') addDocumentForm: FormComponent;

  options: DataFormOptions = {
    data: {
      model: '/homedocnew',
      mutation: 'update',
      params: () => ({ mese: this.month.toString(), anno: this.year.toString(), id_organo: this.idorgano.toString(), uid_modifica: this.cookie.user.refoguid.toString()}),
    }
  };


  dialogFields: DataFieldConfig[] = [
    {
      fieldGroupClassName: 'row display-flex',
      fieldGroup: [       
        {
           key: 'mese', 
           type: 'input',
           hide: true      
        },
        {
          key: 'anno',
          type: 'input',
          hide: true        
        },
        {
          key: 'id_organo',
          type: 'input',
          hide: true        
        },
        {
          key: 'uid_modifica', 
          type: 'input',
           hide: true         
        }, 
        {
          key: 'id_doc_info', 
          type: 'input',
          hide: true       
        },
        {
          key: 'cod_tipo_doc',
          type: 'select',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          defaultValue: 'd',
          templateOptions: {
            label: 'Tipo documento',
             data: {
               model: 'htipodoc'
              },
            labelProp: 'descrizione',
            valueProp: 'id_tipo_doc',
            required: true
          }
        },
        {
          key: 'titolo',
         // type: 'textareahtml',
          type: 'input',
          wrappers: ['form-field', 'customdialog'],
          
          className: 'col-lg-8 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Titolo',
            label: 'Titolo',
            required: true,
            dialog: {
              componentOrTemplate: 'documentiTable'
            }    
           // rows: 3
          }
        },
        {
          key: 'descrizione',
          type: 'textareahtml',
          className: 'col-lg-12 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Descrizione',
            label: 'Descrizione',
            rows: 3
          }
        },
        {
          key: 'funzioni',
          type: 'select',
          className: 'col-lg-9 col-md-6 col-sm-12 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Funzioni',
             data: {
               model: 'hfunzioni'
              },
            labelProp: 'descrizione',
            valueProp: 'id_funzione',
            multiple: true,
          }
        },
        {
          key: 'sessioni',
          type: 'select',
          className: 'col-lg-3 col-md-4 col-sm-6 p-2',
          defaultValue: '',
          templateOptions: {
            label: 'Sessione',
             data: {
               model: 'hsessioni'
             },
            labelProp: 'descrizione',
            valueProp: 'id_sessione',
          }
        },
        {
          key: 'destinatari',
          type: 'select',
          className: 'col-lg-12 col-md-6 col-sm-12 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Destinatari',
             data: {
               model: 'hdestinatari'
              },
            labelProp: 'descrizione_destinatario',
            valueProp: 'id_destinatario',
            multiple: true,
          }
        },
        {
          key: 'norma_interna',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Norma interna',
            label: 'Norma interna',
            rows: 3
          }
        },
        {
          key: 'norma_esterna',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Norma esterna',
            label: 'Norma esterna',
            rows: 3
          }
        },
        {
          key: 'finalita',
          type: 'textareahtml',
          className: 'col-lg-12 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Finalità',
            label: 'Finalità',
            rows: 3
          }
        },
      ]
    }
  ];

  @ViewChild('documentiTable') table: TableComponent;
  columns: TableColumn[] = [
    { name: 'Id', prop: 'id_doc_info', headerComponent: InputFilterComponent },
    {
      name: 'Cod.',
      prop: 'codiceDocumento',
      headerComponent: InputFilterComponent
    },
    {
      name: 'Titolo',
      prop: 'titolo',
      headerComponent: InputFilterComponent
    },
    {
      name: 'Descrizione',
      prop: 'descrizione_organo',
      headerComponent: InputFilterComponent
    },

    {
      cellComponent: CellTemplateComponent,
      customContext: { cell: { template: 'choose' } },
      maxWidth: 150
    }
  ];

  choose(ref: MatDialogRef<any, any>, row: any) {
    //this.addDocumentForm.options.resetModel({});
    ref.close(row);

  //  of(this.getOrgani()).subscribe(organi => {
  //   this.organi = organi;
  //   this.form.controls.organi.patchValue(this.organi[0].id);
  // });

  }



 
  sendAction(ref: MatDialogRef<any, any>) {
    
    if (this.form.valid) {        
      //setto mese e dati per inserimento documento nella riunione
      // this.form.controls.mese.setValue(this.month);    
      // this.form.controls.anno.setValue(this.year);   
      // this.form.controls.id_organo.setValue(this.idorgano); 
      // this.form.controls.uid_modifica.setValue(this.cookie.user.refoguid);
      
      this.addDocumentForm
      .flush()
      .pipe(takeUntil(this.destroyed$))
      .subscribe((x) => {
        this.route.params.subscribe((params: any) => {
          this.service.load(params.id);
          //this.http.refresh();
          this.cd.detectChanges();
        });
        this.dialogs.success('Documento aggiunto alla riunione!').subscribe(() => this.closeForm(ref, true));
      });     
    } else {     
      this.dialogs.warning('Compilare i dati obbligatori!');
    }   
  }

  closeForm(ref: MatDialogRef<any, any>, ret?: any) {
    this.addDocumentForm.options.resetModel({});
    ref.close(ret);
  }

  
  openDialogAction() {
    // do nothing...
  }


  ngOnInit() {

    this.statoRiunione = 'primary';
    this.iconaRiunione = 'done_all';
    this.toggle
        .pipe(takeUntil(this.destroyed$))
        .subscribe(() => {this.opened = !this.opened; this.arrow = !this.arrow;
        });

      // apro l'accordion del mese corrente
    if (this.index === this.todayDate.getMonth()) {
          this.opened = true;
          this.arrow = true;
      }    

    this.formDataRiunione.patchValue({
      data_riunione: this.dateriunione,
      num_verbale: this.n_verbale,
    });    
   
  }

  ngAfterViewInit() {
    this.setDragItems();
    this.listenToDragEvents(); 
    this.setColorSemaforoRiunione(); 
  }


   // colore semaforo riunione in base ai documenti
  private setColorSemaforoRiunione(): void {
    this.collapseItems.toArray().forEach(item => {
      if (item.ricevuto ===  '0') {
        this.statoRiunione = 'warn';
        this.iconaRiunione = 'clear';

        this.cd.detectChanges();
        return false;
      }
    });
  }

  private setDragItems(): void {
    this.collapseItems.toArray().forEach(item => {
      this.dragItems.push(this.dragDrop.createDrag(item.elRef));
    });
  }

  private listenToDragEvents(): void {
    this.bodyDropList = this.dragDrop.createDropList(this.body);
    this.headerDropList = this.dragDrop.createDropList(this.header);

    this.dragDropRegistryService.add(REGISTRY_KEY, this.bodyDropList);
    this.dragDropRegistryService.add(REGISTRY_KEY, this.headerDropList);

    this.bodyDropList.data = this.dropListData;
    this.headerDropList.data = this.dropListData;

    this.headerDropList.enterPredicate = this.preventBodyItemEntrance.bind(this);

    this.bodyDropList.withItems(this.dragItems);
    this.headerDropList.withItems([]);

    this.bodyDropList.exited
      .pipe(takeUntil(this.destroyed$))
      .subscribe(event => this.onBodyExited(event));

    this.bodyDropList.dropped.pipe(
      merge(this.headerDropList.dropped),
      takeUntil(this.destroyed$)
    ).subscribe(event => this.onDropped(event));

    this.bodyDropList.beforeStarted
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.onBodyBeforeStarted());

    this.headerDropList.entered
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.onHeaderEntered());

    this.headerDropList.exited
      .pipe(takeUntil(this.destroyed$))
      .subscribe(() => this.onHeaderExited());

    this.collapseItems.changes.pipe(
      takeUntil(this.destroyed$),
      startWith(this.collapseItems),
      map((change: QueryList<any>) => change.length),
      pairwise()
    ).subscribe(lengths => this.onCollapseItemsChange(...lengths));
  }

  private onDropped(event): void {
    this.lastDroppedIndex = event.currentIndex;
    this.dropListDropped.emit(event);

    //foreach per avere la lista di tutti i documenti nel mese draggato.
    this.collapseItems.forEach((item, i) => {
      // console.log('id_organo ' + JSON.stringify(item.idorgano));
      // console.log('item-mese ' + JSON.stringify(item.month));
      // console.log('item-anno ' + JSON.stringify(item.year));
      // console.log('codicedoc ' + JSON.stringify(event.container.data[i].codice_doc));
      // console.log('uid_modifica ' + this.cookie.user.refoguid.toString());
      // console.log('position:  ' + i);

      this.positionDocument.push({
        id_organo: item.idorgano,
        mese: item.month, 
        anno: item.year, 
        id_doc_info: event.container.data[i].id_doc_info, 
        peso: i + 1,
        uid_modifica: this.cookie.user.refoguid.toString()});
      });


    if (event.previousContainer === event.container) {
      moveItemInArray(this.dragItems, event.previousIndex, event.currentIndex);
      // https://github.com/angular/material2/pull/15103
      // Call withItems to keep everything in sync when an item is dropped.
      this.bodyDropList.withItems(this.dragItems);
    }


    console.log('array documenti:  ' + JSON.stringify(this.positionDocument));

    this.renderer.removeClass(this.header.nativeElement, 'entered');
    this.renderer.removeClass(this.body.nativeElement, 'cdk-drop-list-dragging');
  }

  private onBodyBeforeStarted(): void {
    this.renderer.addClass(this.body.nativeElement, 'cdk-drop-list-dragging');
  }

  private onBodyExited(event: { item: DragRef, container: DropListRef<T> }): void {
    this.lastExitedIndex = this.dragItems.findIndex(item => item === event.item);
    this.renderer.removeClass(this.body.nativeElement, 'cdk-drop-list-dragging');
  }

  private onCollapseItemsChange(previousLength, currentLength): void {
    if (previousLength < currentLength) {
      const dragItem = this.dragDrop.createDrag(this.collapseItems.toArray()[this.lastDroppedIndex].elRef);
      this.dragItems.splice(this.lastDroppedIndex, 0, dragItem);

      // https://github.com/angular/material2/pull/15103
      // Call withItems to keep everything in sync when an item is dropped.
      this.bodyDropList.withItems(this.dragItems);
    } else if (previousLength > currentLength) {
      this.dragItems[this.lastExitedIndex].dispose();
      this.dragItems.splice(this.lastExitedIndex, 1);

      // https://github.com/angular/material2/pull/15103
      // Call withItems to keep everything in sync when an item is dropped.
      this.bodyDropList.withItems(this.dragItems);
    }
  }

  private onHeaderEntered(): void {
    this.renderer.addClass(this.header.nativeElement, 'entered');
  }

  private onHeaderExited(): void {
    this.renderer.removeClass(this.header.nativeElement, 'entered');
  }

  private preventBodyItemEntrance(dragItem: DragRef): boolean {
    return this.bodyDropList.getItemIndex(dragItem) === -1;
  }


  setDataRiunione() {

    if (this.formDataRiunione.valid) {
      if (this.formDataRiunione.controls.data_riunione.value) {
        this.formDataRiunione.controls.data_riunione.setValue(this.datePipe.transform(this.formDataRiunione.controls.data_riunione.value, 'yyyy-MM-dd'));
      }
      const params = {
        id_organo: this.idorgano.toString(),
        mese: this.month.toString(),
        anno: this.year.toString(),
        uid_modifica: this.cookie.user.refoguid.toString(),
        data_riunione: this.formDataRiunione.controls.data_riunione.value,
        n_verbale: this.formDataRiunione.controls.num_verbale.value,
      };
      this.http
      .create('homedocsetdatariunione', params, [])
      .subscribe(() => {
       // this.sb.openSnackBar("Data riunione inserita");
        this.sb.openSnackBar('Data riunione inserita', 'success');
       });
    } else {
      this.dialogs.warning('Campo data riunione è obbligatorio.');
    }
  }


  ngOnDestroy() {
    this.destroyed$.next(true);
    this.dragDropRegistryService.remove(REGISTRY_KEY, this.bodyDropList);
    this.dragDropRegistryService.remove(REGISTRY_KEY, this.headerDropList);
  }
}
