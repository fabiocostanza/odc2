import { animate, state, style, transition, trigger } from '@angular/animations';

const ANIMATION_TIMINGS = '300ms cubic-bezier(0.645, 0.045, 0.355, 1)';

export const contentExpansion = trigger('contentExpansion', [
  state('collapsed', style({height: '0px', visibility: 'hidden'})),
  state('expanded', style({height: '*', visibility: 'visible'})),
  transition('expanded <=> collapsed', animate(ANIMATION_TIMINGS)),
])