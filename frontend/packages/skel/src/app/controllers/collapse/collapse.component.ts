import { Component, Input, ContentChildren, QueryList, AfterViewInit, AfterContentInit, OnDestroy, TemplateRef, ViewChild  } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { CollapsePanelComponent } from './collapse-panel/collapse-panel.component';


 
 @Component({
   selector: 'app-collapse',
   template: '<ng-content></ng-content>'
 })



export class CollapseComponent implements OnDestroy {
  @Input() accordion: boolean;
  @ContentChildren(CollapsePanelComponent) collapsePanels: QueryList<CollapsePanelComponent>;

  //@ContentChildren(CollapsePanelComponent, { descendants: true }) collapsePanels: QueryList<CollapsePanelComponent>;
  @Input() opened = false;
  @Input() arrow = false;

  constructor() { }

  private destroyed$ = new Subject();

  // ngAfterContentInit() {}

  // ngAfterViewInit() {}

  togglePanels(panel: CollapsePanelComponent) {
    if (!this.accordion) {
      panel.opened = !panel.opened;
      panel.arrow = !panel.arrow;
      return;
    }

     // close all panels
    this.collapsePanels.forEach(panel => {
      panel.opened = false; 
      panel.arrow = false;
    });

     // open the selected panel
    panel.arrow = true;
    panel.opened = true;
  }

  ngOnDestroy() {
    this.destroyed$.next();
  }
}