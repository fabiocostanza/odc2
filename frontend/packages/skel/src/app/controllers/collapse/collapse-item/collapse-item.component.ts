import { Component, Input, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { DialogsService, FormComponent, DataFieldConfig, DataFormOptions } from 'layout';
import { FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { DocumentoServiceService } from '../../../services/documento-service.service';
import { StateDataService, CookiesService } from 'core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-collapse-item',
  templateUrl: './collapse-item.component.html',
  styleUrls: ['./collapse-item.component.scss']
})
export class CollapseItemComponent implements OnDestroy  {
  @Input() title: string;
  @Input() text: string;
  @Input() coddoc: string;
  @Input() iddoc: string;
  @Input() ricevuto: string;
  @Input() month: number;
  @Input() year: number;
  @Input() idorgano: number;

  constructor(public elRef: ElementRef, private route: ActivatedRoute, private http: StateDataService, private dialogs: DialogsService, private cookie: CookiesService, public service: DocumentoServiceService ) { }
  
  destroy$ = new Subject();
  form = new FormGroup({});
  model: any = {};
  @ViewChild('sendDocument') SendDocumentForm: FormComponent;

  options: DataFormOptions = {
    data: {
      // model: '/adddocriunione',
      model: '/homedocedit',
      mutation: 'update',
      params: () => ({ id_doc_info: this.iddoc.toString(), mese: this.month.toString(), anno: this.year.toString(), id_organo: this.idorgano.toString() }),
      // mutation: () => this.mutation,
      // params: () => (this.id ? { id: this.id } : {})
      // read: (x) => {console.log(x);
      //   this.options.resetModel();
      //   console.log('iddoc: ' + this.iddoc);   return x;}
    }
  };
  dialogFields: DataFieldConfig[] = [
    {
      fieldGroupClassName: 'row display-flex',
      fieldGroup: [
        {
          key: 'mese', 
          type: 'input',
          hide: true      
       },
       {
         key: 'anno',
         type: 'input',
         hide: true        
       },
       {
         key: 'id_organo',
         type: 'input',
         hide: true        
       },
       {
         key: 'uid_modifica', 
         type: 'input',
          hide: true         
       }, 
       {
         key: 'id_doc_info', 
         type: 'input',
         hide: true       
       },
        {
          key: 'cod_tipo_doc',
          type: 'select',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          defaultValue: 'd',
          templateOptions: {
            label: 'Tipo documento',
             data: {
               model: 'htipodoc'
              },
            labelProp: 'descrizione',
            valueProp: 'id_tipo_doc',
            required: true
          }
        },    
        {
          key: 'titolo',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Titolo documento',
            label: 'Titolo documento',            
            required: true,
            rows: 3
          }
        },
        {
          key: 'finalita',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-12 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Finalità',
            label: 'Finalità',
            rows: 3
          }
        },
        {
          key: 'funzioni',
          type: 'select',
          className: 'col-lg-9 col-md-6 col-sm-12 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Funzioni',
             data: {
               model: 'hfunzioni'
              },
            labelProp: 'descrizione',
            valueProp: 'id_funzione',
            multiple: true,
            required: true
          }
        },
        {
          key: 'sessioni',
          type: 'select',
          className: 'col-lg-3 col-md-4 col-sm-6 p-2',
          // expressionProperties: {
          //   'templateOptions.disabled': 'model.organi != "1"',
          // },
          templateOptions: {
            label: 'Sessione',
             data: {
               model: 'hsessioni'
             },
            labelProp: 'descrizione',
            valueProp: 'id_sessione'
          }
        },
        {
          key: 'destinatari',
          type: 'select',
          className: 'col-lg-12 col-md-6 col-sm-12 p-2',
          defaultValue: [],
          templateOptions: {
            label: 'Destinatari',
            data: {
              model: 'hdestinatari'
              },
            labelProp: 'descrizione_destinatario',
            valueProp: 'id_destinatario',
            multiple: true,
          }
        },
        {
          key: 'norma_interna',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Norma interna',
            label: 'Norma interna',
            rows: 3
          }
        },
        {
          key: 'norma_esterna',
          type: 'textareahtml',
          className: 'col-lg-6 col-md-6 col-sm-12 p-2',
          templateOptions: {
            placeholder: 'Norma esterna',
            label: 'Norma esterna',
            rows: 3
          }
        },
      ]
    }
  ];


  options2: DataFormOptions = {
    data: {
      model: '/homedocedit',
      mutation: 'update',
      params: () => ({ id_doc_info: this.iddoc.toString(), mese: this.month.toString(), anno: this.year.toString(), id_organo: this.idorgano.toString() }),
    }
  };
  dialogFields2: DataFieldConfig[] = [
    {
      fieldGroupClassName: 'row display-flex',
      fieldGroup: [
        {
          key: 'prendeatto',
          type: 'checkbox',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          templateOptions: {
            label: 'Prende atto',
          }
        },
        {
          key: 'data_prende_atto',
          type: 'datepicker',
          className: 'col-lg-5 col-md-4 col-sm-6 p-2',
          templateOptions: {
            label: 'Data',
          }
        },
        {
          key: 'prende_visione',
          type: 'checkbox',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          templateOptions: {
            label: 'Prende visione',
          }
        },
        {
          key: 'data_prende_visione',
          type: 'datepicker',
          className: 'col-lg-5 col-md-4 col-sm-6 p-2',
          templateOptions: {
            label: 'Data',
          }
        },
        {
          key: 'approva',
          type: 'checkbox',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          templateOptions: {
            label: 'Approva',
          }
        },
        {
          key: 'data_approva',
          type: 'datepicker',
          className: 'col-lg-5 col-md-4 col-sm-6 p-2',
          templateOptions: {
            label: 'Data',
          }
        },
        {
          key: 'delibera',
          type: 'checkbox',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          templateOptions: {
            label: 'Delibera',
          }
        },
        {
          key: 'data_delibera',
          type: 'datepicker',
          className: 'col-lg-5 col-md-4 col-sm-6 p-2',
          templateOptions: {
            label: 'Data',
          }
        },
        {
          key: 'parere',
          type: 'checkbox',
          className: 'col-lg-4 col-md-6 col-sm-12 p-2',
          templateOptions: {
            label: 'Parere',
          }
        },
        {
          key: 'data_parere',
          type: 'datepicker',
          className: 'col-lg-5 col-md-4 col-sm-6 p-2',
          templateOptions: {
            label: 'Data',
          }
        },
      ]
    }
  ];

  ngOnDestroy() {
    this.destroy$.next(true);
  }


  sendAction(ref: MatDialogRef<any, any>) {
    if (this.form.valid) {  
      this.SendDocumentForm
      .flush()
      .pipe(takeUntil(this.destroy$))
      .subscribe((x) => {
        this.route.params.subscribe((params: any) => {
          this.service.load(params.id);
        });    
        this.dialogs.success('Invio avvenuto con successo').subscribe(() => this.closeForm(ref, true));
      });
    } else {     
      this.dialogs.warning('Compilare i dati obbligatori!');
    } 
    
  }

  closeForm(ref: MatDialogRef<any, any>, ret?: any) { 
      this.SendDocumentForm.options.resetModel({});
      ref.close(ret);
  }


  // salvo in db lo stato del flagricevuto e applico il colore verde
  setFlagRicevuto(): void {
    const params = {
      id_doc_info: this.iddoc.toString(),
      id_organo: this.idorgano.toString(),
      mese: this.month.toString(),
      anno: this.year.toString(),
      uid_modifica: this.cookie.user.refoguid.toString(),
      flag_ricevuto: this.ricevuto.toString()
    };
    this.http
      .create('homedocsetflagricevuto', params, [])
      .subscribe(() => {
        //  // this.dialogs.success('Documento aggiornato con successo!').subscribe(() => this.data.refresh());
        if ( this.ricevuto == '0') {
          this.ricevuto = '1';
        } else {
          this.ricevuto = '0';
        }
       });
  }


}
