import { Component, OnInit } from '@angular/core';
import {TableColumn} from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-funzioni',
  templateUrl: './funzioni.component.html',
  styleUrls: ['./funzioni.component.css']
})
export class FunzioniComponent implements OnInit {
columns: TableColumn[] = [
  {name: 'descrizione', prop: 'descrizione', sortable: true},
  {name: 'sigla', prop: 'sigla', sortable: true}

];

  constructor() { }

  ngOnInit() {
  }

}
