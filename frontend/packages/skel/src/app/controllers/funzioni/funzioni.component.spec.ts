import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunzioniComponent } from './funzioni.component';

describe('FunzioniComponent', () => {
  let component: FunzioniComponent;
  let fixture: ComponentFixture<FunzioniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunzioniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunzioniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
