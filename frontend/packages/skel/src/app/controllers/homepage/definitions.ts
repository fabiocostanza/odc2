export interface Documento {
  id_documento: number;
  titolo_documento: string;
  funzione: string;
  periodicita: number;

  
  title: string;
  text: string;
}


export interface Riunione {
  id_riunione: number;
  titoloRiunione : string;
  data_riunione : string;
  id_organo: number;
  name: string;
  documenti: Documento[];
}