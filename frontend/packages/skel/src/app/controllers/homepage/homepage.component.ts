import { Component, OnInit, ViewChild, Input, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MutationType } from 'core';


import { DialogsService, FormComponent, DataFormOptions, DataFieldConfig } from 'layout';
import { FormGroup } from '@angular/forms';






import { CdkDragDrop, DragRef, transferArrayItem, moveItemInArray, copyArrayItem } from '@angular/cdk/drag-drop';
import { MatExpansionPanel } from '@angular/material';
import { MatDialogRef } from '@angular/material';
import { DocumentoServiceService } from '../../services/documento-service.service';


import { Riunione } from './definitions';
import { Subject } from 'rxjs';



@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit, OnDestroy {
  public result: any;
  public dateriunione: Date;
  id: string;
  mutation: MutationType = 'create';

  constructor(private route: ActivatedRoute, private dialogs: DialogsService, public service: DocumentoServiceService) { }

  @ViewChild(FormComponent) formComponent: FormComponent;

  form = new FormGroup({});
  @Input() model: any = {};
  organi: string;
  options: DataFormOptions = {};
  fields: DataFieldConfig[] = [
    {
      fieldGroupClassName: 'display-flex',
      fieldGroup: [
        {
          key: 'filtro_organi',
          type: 'radio',
          className: 'col-lg-12 col-md-4 col-sm-6 p-2',
          filter: 'id_organo',
          templateOptions: {
            placeholder: '',
            label: '',
            required: true,
            defaultValue: '1',
            data: {
              model: '/horgani'
            },
            labelProp: 'descrizione_organo',
            valueProp: 'id_organo',
            //click:  (field) =>  { console.log(this.formComponent.model.filtro_organi)}, 
            // change: (field) =>  {
            //   this.service.load(field.formControl.value);
            //   this.fields[0].fieldGroup[0].templateOptions.options.forEach(radio => {
            //     if (radio['id_organo'] === this.model['filtro_organi']) {
            //       this.organi = radio['descrizione_organo'];
            //       return false;
            //     }
            //   });
            // },
          },
        },
      ]
    },
  ];




  @Input() dropListData: any[];
  private dragItems: DragRef<any>[] = [];
  private destroyed$ = new Subject();


  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      this.id = params.id;
      // if (this.id) {
      //   this.mutation = 'update';
      // }
    });
    
    this.form.valueChanges.subscribe(data => this.onSelectOrganoFormValueChange(data));    
   // this.fields[0].fieldGroup[0].templateOptions.clickEvent = (data) => this.onSelectOrganoFormValueChange(data);
  }

  ngOnDestroy() {
    this.destroyed$.next(true);
  }


  onSelectOrganoFormValueChange(data) {
    if (data.filtro_organi !== null) {
      this.service.load(data.filtro_organi);
    }    
  }




  

  // // onDropped(event: CdkDragDrop<any>) {
  onDropped(event: CdkDragDrop<string[]>, custom: any) {

    // //console.log('mese ' + JSON.stringify(custom));
    
    
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      
    } else {
      // anzichè spostare l'elemento lo duplico
      transferArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
      //  copyArrayItem(
      //   event.previousContainer.data,
      //   event.container.data,
      //   event.previousIndex,
      //   event.currentIndex
      // );
    }
  }

  // // cdkDropListDroppedHandler(event: CdkDragDrop<any[]>) {
  // //   transferArrayItem(
  // //     event.previousContainer.data,
  // //     event.container.data,
  // //     event.previousIndex,
  // //     event.currentIndex);
  // // }

  // // drop(event: CdkDragDrop<any[]>) {
  // //   console.log(event);
  // //   moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
  // // }

}
