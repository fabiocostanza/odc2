import { NgModule, Injector } from '@angular/core';

import { BaseAppModule } from './base/base-app-module.module';

import { FormlyModule } from '@ngx-formly/core';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule, MatButtonModule, MatInputModule, MatIconModule, MatSnackBar, MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatListModule } from '@angular/material/list';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { AppComponent } from './app.component';
import { HomepageComponent } from './controllers/homepage/homepage.component';
import { OrganiComponent } from './controllers/organi/organi.component';
import { FunzioniComponent } from './controllers/funzioni/funzioni.component';

import { DocCreateEditComponent } from './controllers/documenti/doc-create-edit.component';

import { AccordionComponent } from './controllers/accordion/accordion.component';


import { CollapseComponent } from './controllers/collapse/collapse.component';
import { CollapsePanelComponent } from './controllers/collapse/collapse-panel/collapse-panel.component';
import { CollapseItemComponent } from './controllers/collapse/collapse-item/collapse-item.component';
import { DocumentiComponent } from './controllers/documenti/documenti.component';

import { QuillModule } from 'ngx-quill';
import { FieldQuillType } from './controllers/textarea-html/textareahtml.type';
import { DatePipe } from '@angular/common';
import { ExcelServiceService } from './services/excel-service.service';

import { SnackbarComponent } from './controllers/snackbar/snackbar.component';
import { MatDialogWrapperComponent } from './controllers/form/wrapper/wrapper.component';


@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    OrganiComponent,
    FunzioniComponent,
    DocCreateEditComponent,

    CollapseComponent,
    CollapsePanelComponent,
    CollapseItemComponent,

    FieldQuillType,
    AccordionComponent,
    DocumentiComponent,
    MatDialogWrapperComponent
  ],

  imports: [
    BaseAppModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatIconModule,
    MatButtonModule,
    MatDatepickerModule,
    MatInputModule,
    MatListModule,
    MatSnackBarModule,
    DragDropModule,

    QuillModule.forRoot(),
    FormlyModule.forRoot({
      types: [{
        name: 'accordion',
        component: AccordionComponent,
        wrappers: ['form-field'],
      },
      {
        name: 'textareahtml',
        component: FieldQuillType,
        wrappers: ['form-field'],
      },
      ],

      // validationMessages: [
      //   {
      //     name: 'required',
      //     message: (error, field) => `${field.templateOptions.label} richiesto.`
      //   }],
      wrappers: [{ name: 'customdialog', component: MatDialogWrapperComponent }]
    }),

  ],
  providers: [DatePipe, SnackbarComponent, ExcelServiceService],
  bootstrap: [AppComponent]
})
export class AppModule {

}

