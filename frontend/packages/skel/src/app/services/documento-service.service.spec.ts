import { TestBed } from '@angular/core/testing';

import { DocumentoServiceService } from './documento-service.service';

describe('DocumentoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DocumentoServiceService = TestBed.get(DocumentoServiceService);
    expect(service).toBeTruthy();
  });
});
