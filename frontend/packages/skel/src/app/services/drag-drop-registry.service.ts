import { Injectable } from '@angular/core';
import { DropListRef } from '@angular/cdk/drag-drop';

@Injectable({ providedIn: 'root' })
export class DragDropRegistryService {
  private dropConnections = new Map<string, Set<DropListRef<any>>>();

  public add(key: string, list: DropListRef<any>): void {
    if (!this.dropConnections.has(key)) {
      this.dropConnections.set(key, new Set());
    }

    const dropListSet = this.dropConnections.get(key);

    dropListSet.add(list);
    dropListSet.forEach(list => list.connectedTo(Array.from(dropListSet)));
  }

  public remove(key: string, list: DropListRef<any>): void {
    if (!this.dropConnections.has(key)) {
      this.dropConnections.set(key, new Set());
    }

    const dropListSet = this.dropConnections.get(key);

    dropListSet.delete(list);
    dropListSet.forEach(list => list.connectedTo(Array.from(dropListSet)));
  }
}