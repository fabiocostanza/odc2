import { Injectable } from '@angular/core';
import { StoreDataService, Result, Entity, StateDataService, PayloadData } from 'core';
import { Router } from '@angular/router';
import { ArrayType } from '@angular/compiler';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { DialogsService, Resource } from 'layout';

@Injectable({
  providedIn: 'root'
})
export class DocumentoServiceService {

  public model: any;

  public loggedUser: Array<object>;

  public result$: Subject<any[]> = new Subject();

  constructor(public store: StoreDataService, public router: Router, public data: StateDataService, private dialogs: DialogsService) { }

  // load(id_organo: string) {
  //   this.data.read('docmese', { id_organo: id_organo }).subscribe((res: any) => {
  //     this.model = res.data[0];
  //   })
  // }

  // const getCircularReplacer = () => {
  //   const seen = new WeakSet();
  //   return (key, value) => {
  //     if (typeof value === "object" && value !== null) {
  //       if (seen.has(value)) {
  //         return;
  //       }
  //       seen.add(value);
  //     }
  //     return value;
  //   };
  // };

  load(id_organo: string) {
    this.data.read('homedoclistmesi', { organo: id_organo }).subscribe((res: any) => {
      
      // var years = "2020";
      // var months = new Array(12);
      //   months[1] = "Gennaio";
      //   months[2] = "Febbraio";
      //   months[3] = "Marzo";
      //   months[4] = "Aprile";
      //   months[5] = "Maggio";
      //   months[6] = "Giugno";
      //   months[7] = "Luglio";
      //   months[8] = "Agosto";
      //   months[9] = "Settembre";
      //   months[10] = "Ottobre";
      //   months[11] = "Novembre";
      //   months[12] = "Dicembre";

      //  const d = res.data instanceof Array ? res.data : [res.data];

      //  this.model = res.data.map(r => { 
      //    r.nomemese = months[r.mese] + ' ' + years; 
      //    r.years = years; 

      //    return r;});
      this.result$.next(Object.values(res.data));
    }) 
    //return this.model;
  }





  loadAll() {
    this.data.read('homedoclistmesi').subscribe((res: any) => {
      var years = "2020";
      var months = new Array(12);
        months[1] = "Gennaio";
        months[2] = "Febbraio";
        months[3] = "Marzo";
        months[4] = "Aprile";
        months[5] = "Maggio";
        months[6] = "Giugno";
        months[7] = "Luglio";
        months[8] = "Agosto";
        months[9] = "Settembre";
        months[10] = "Ottobre";
        months[11] = "Novembre";
        months[12] = "Dicembre";

       this.model = res.data.map(r => { 
         //aggiungo il nome del mese in base all'indice del mese 
         r.nomemese = months[r.mese] + ' ' + years; 
         return r;});

     // this.model = res.data;
      this.result$.next(this.model);
    })

  }

  filter_organo(id_organo: string) {
    //this.data.filter({ id_organo: id_organo });
    this.data.filter({ id_organo: id_organo }).subscribe((res: any) => {
      this.model = res.data;
      this.result$.next(this.model);
    });

  }

  refresh() {
    this.data.refresh();
  }


}
