import { Routes } from '@angular/router';
import { HomepageComponent } from './controllers/homepage/homepage.component';
import { OrganiComponent } from './controllers/organi/organi.component';
import { FunzioniComponent } from './controllers/funzioni/funzioni.component';
import { DocCreateEditComponent } from './controllers/documenti/doc-create-edit.component';
import { DocumentiComponent } from './controllers/documenti/documenti.component';

export const appRoutes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full'  },
    { path: 'home', component: HomepageComponent, data: { label: 'Home' } },
    { path: 'funzioni', component: FunzioniComponent, data: { label: 'Funzioni' } },
    { path: 'organi', component: OrganiComponent, data: { label: 'Organi' } },    
    // { path: 'documenti', component: DocCreateEditComponent, data: { label: 'Aggiungi Documenti' } },

    { path: 'documenti', component: DocumentiComponent, data: { label: 'Documenti' } },
    { path: 'documenti/new', component: DocCreateEditComponent, data: { breadcrumb: 'Aggiungi documento' } },
    { path: 'documenti/edit/:id_documento/:id_doc_info', component: DocCreateEditComponent, data: { breadcrumb: 'Modifica documento' } },   
    
];

