- [STRUTTURA DELLA DIRECTORY](#struttura-della-directory)
  - [PACKAGES](#packages)


## STRUTTURA DELLA DIRECTORY
```
plug/
|---packages/
|   |---core/ (core library)
|   |   |---src/
|   |   |   |---lib/
|   |   |   |   |---(source and test files)
|   |   |   |---assets/
|   |   |   |   |---(optional assets files and directory)
|   |   |   |---(public api & optional package test file)
|   |   |---(config files)
|   |---layout/ (layout library)
|   |   |---src/
|   |   |   |---lib/
|   |   |   |   |---(source and test files)
|   |   |   |---assets/
|   |   |   |   |---(optional assets files and directories)
|   |   |   |---(public api & optional package test file)
|   |   |---(config files)
|   |---skel
|   |   |---src/
|   |   |   |---app/
|   |   |   |   |---(source and test files)
|   |   |   |---assets/
|   |   |   |   |---(optional assets files and directories)
|   |   |   |---environments/
|   |   |   |   |---(environments files)
|   |   |   |---(public api & optional package test file)
|   |   |---(config files)
|   |---skel-e2e
|       |---src/
|       |   |---(integration test files)
|       |---(config files)
|---(config files)
```

### PACKAGES
