# odc2

## Configurazione ambiente Windows

**0. Installare utility**

**1. Aggiungere nella varibili d'ambiente di windows**

    HTTP_PROXY=http://swfb:8080
    HTTPs_PROXY=http://swfb:8080
    NO_PROXY=gitlab.webteam-bnl.staging.echonet
    HTTP_PROXY_REQUEST_FULL_URI=1

**2. Lanciare il comando e poi aggiungere nella configurazione globale di git**

    git config --global -e

    [http]
        sslverify = false
    [remote "bnl"]
            proxy =
    [remote "origin"]
            proxy =

**3. Aggiungere negli etc/hosts**

    10.232.112.138 s00vl9938167 swfb db.wire
    10.232.112.170 s00vl9938165 cwfe gitlab.webteam-bnl.staging.echonet

**4. Modificare php.ini di php versione >= 7**

    //cercare "error_reporting = " e sostituire tutto quello dopo l'uguale con
    E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED

**5. Composer disabilitare ssl obbligatorio**

    //da console lanciare questi due comandi
    composer config --global disable-tls true
    composer config --global secure-http false

**6. configurare il frontend**

    cd frontend
    npm config edit

    //aggiungere le seguenti righe nella configurazione e salvare
    registry=http://10.238.11.9:9081/nexus/content/groups/npmjs
    noproxy=10.238.11.9
    https-proxy=http://swfb:8080
    http-proxy=http://swfb:8080
    proxy=http://swfb:8080

    //controllare se sono stati inseriti correttamente
    npm config list 

## Configurazione App

 **1. Aggiungere la seguente config in wamp http-vhosts.conf salva e restart wamp**

    <VirtualHost *:8088>

        SetEnv APPLICATION_ENV "dev"
        
        ServerName localhost
        #ServerAlias per l'autenticazione
        ServerAlias wire-skel.bnl.echonet
        Alias /sso-plug-and-wire "C:/webteam/apps/odc2/backend/public"   
        DocumentRoot "C:/webteam/apps/odc2/backend/public"
        <Directory  "C:/webteam/apps/odc2/backend/public">
            Options +Indexes +Includes +FollowSymLinks +MultiViews
            AllowOverride All
            Require local
            Allow from All
        </Directory>
    </VirtualHost>

**2. Se non possiedi già l'app.ini**

    - copia configs-wire/odc2/app.ini.dist nella cartella /c/webteam/configs-wire/odc2 rinominandolo in app.ini
    - crea le configs-wire anche per FARM e IAM, o altri DB a cui si connette la tua applicazione

**3. Creare database applicazione locale**

    - eseguire structure.sql
    - eseguire data.sql

**4. Scaricamento pacchetti front e back end**

    cd frontend
    npm install

    cd backend
    composer install

    #per aggiungere i file lock al progetto
    git add .
    git commit -m "init"
    git push

## Avvio applicazione

    //wamp deve essere avviato con php versione >= 7
    cd frontend
    ng serve

## Avvio applicazione swfb

    1. Rilascio applicazione con lo script Releaser e Propagator
    2. Aggiungere nel file degli hosts 
        10.232.112.138 s00vl9938167 (altri nomi...) odc2_swfb