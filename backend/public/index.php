<?php

declare(strict_types = 1);

// Delegate static file requests back to the PHP built-in webserver
if (PHP_SAPI === 'cli-server' && $_SERVER['SCRIPT_FILENAME'] !== __FILE__) {
    return false;
}

chdir(dirname(__DIR__));
require 'vendor/autoload.php';

// Load configuration
$config = require 'config/config.php';

// the Service Manager Container
/** @var \Zend\ServiceManager\ServiceManager $serviceContainer */
$serviceContainer = Wire\Bootstrap::getInstance($config)->init();

/** @var \Zend\Expressive\Application $app */
$app = $serviceContainer->get(\Zend\Expressive\Application::class);
$factory = $serviceContainer->get(\Zend\Expressive\MiddlewareFactory::class);

(require realpath(__DIR__) . '/../config/pipeline.php')($app, $factory, $serviceContainer);
(require realpath(__DIR__) . '/../config/routes.php')($app, $factory, $serviceContainer);

$app->run();
