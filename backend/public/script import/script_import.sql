TRUNCATE destinatari;
insert into destinatari (descrizione_destinatario)
(select distinct d.descrizione from assoc_doc_info_destinatari d order by d.descrizione);

TRUNCATE funzioni;
insert into funzioni (descrizione)
(select s.descrizione from (
select distinct d.descrizione from assoc_doc_info_contributrici d
union 
select distinct d.descrizione  from assoc_doc_info_funzioni d) s order by s.descrizione);

TRUNCATE mesi;
insert into mesi (nome_mese)
(select distinct d.descrizione from assoc_doc_info_presentazione d order by d.descrizione);

TRUNCATE sessioni;
insert into sessioni (descrizione)
(select distinct d.descrizione from assoc_doc_info_sessioni d order by d.descrizione);



TRUNCATE azioni;
insert into azioni (descrizione)
(select distinct d.desc_azione from assoc_doc_info_organi d order by d.desc_azione);


TRUNCATE periodicita;
insert into periodicita (descrizione_periodicita )
(select distinct d.desc_periodicita from doc_info d order by d.desc_periodicita);

update assoc_doc_info_destinatari ad
inner join destinatari d on d.descrizione_destinatario = ad.descrizione
set ad.id_destinatario = d.id_destinatario;

update assoc_doc_info_contributrici ad
inner join funzioni d on d.descrizione = ad.descrizione
set ad.id_funzione = d.id_funzione;

update assoc_doc_info_funzioni ad
inner join funzioni d on d.descrizione = ad.descrizione
set ad.id_funzione = d.id_funzione;


update assoc_doc_info_presentazione ad
inner join mesi d on d.nome_mese = ad.descrizione
set ad.numero_mese = d.mese;



update assoc_doc_info_sessioni ad
inner join sessioni d on d.descrizione = ad.descrizione
set ad.id_sessione = d.id_sessione;  

update assoc_doc_info_organi ad
inner join azioni d on d.descrizione = ad.desc_azione
set ad.id_azione = d.id_azione;   

update assoc_doc_info_organi ad
inner join organi d on d.id_organo = ad.id_organo
set ad.desc_organo = d.descrizione_organo;  

update doc_info ad
inner join periodicita d on d.descrizione_periodicita = ad.desc_periodicita
set ad.periodicita = d.id_periodicita;  



