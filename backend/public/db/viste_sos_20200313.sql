CREATE or replace VIEW _vdoccensitilist AS 
SELECT di.attivo AS doc_attivo,        
		  di.flag_copia AS flag_copia,
        di.id_documento AS id_documento,
        di.id_doc_info AS id_doc_info,
        di.titolo AS titolo,
        di.descrizione AS note,
         di.desc_periodicita as desc_periodicita,
         di.note_presentazione as note_presentazione,
        di.norma_esterna AS norma_esterna,
        di.norma_interna AS norma_interna,
        di.finalita AS finalita,
        di.attivo AS attivo,
        di.periodicita AS periodicita,
        Group_concat(DISTINCT fu.sigla ORDER BY fu.sigla ASC SEPARATOR  ',') AS  sigla,
        Group_concat(DISTINCT fu.id_funzione ORDER BY fu.id_funzione ASC SEPARATOR ',')  AS funzioni,
        Group_concat(DISTINCT fuc.id_funzione ORDER BY fuc.id_funzione ASC SEPARATOR ',')  AS contributrici,
        Group_concat(DISTINCT adid.id_destinatario ORDER BY adid.id_destinatario ASC SEPARATOR ',') AS destinatari,
        Group_concat(DISTINCT adis.id_sessione ORDER BY adis.id_sessione ASC SEPARATOR ',') AS sessioni,
  	   	Group_concat(DISTINCT fu.descrizione ORDER BY fu.descrizione ASC SEPARATOR '<br>') AS descrizione_funzioni,
  	   	Group_concat(DISTINCT fuc.descrizione ORDER BY fuc.descrizione ASC SEPARATOR '<br>') AS descrizione_contributrici,
        Group_concat(DISTINCT Concat(adio.id_organo, '#',adio.id_azione) ORDER BY adio.id_organo ASC SEPARATOR ',') AS organi,
 		Group_concat(DISTINCT Concat(o.descrizione_organo, ' - ',ao.descrizione) ORDER BY o.id_organo ASC SEPARATOR '<br>') AS descrizione_organo,
 		Group_concat(DISTINCT adip.numero_mese ORDER BY adip.numero_mese ASC SEPARATOR ',') AS presentazione,
 		 Group_concat(DISTINCT m.nome_mese ORDER BY m.mese ASC SEPARATOR '<br>') AS mesi,
		 p.descrizione_periodicita AS descrizione_periodicita,
        Concat(di.id_documento, 'Rev',di.id_doc_info) AS codiceDocumento 
FROM  doc_info di                 
	 left join assoc_doc_info_presentazione adip ON di.id_doc_info = adip.id_doc_info AND Isnull(adip.id_riunione)
	 left join assoc_doc_info_organi adio ON di.id_doc_info = adio.id_doc_info AND Isnull(adio.id_riunione)  
	  left join azioni ao ON   ao.id_azione = adio.id_azione
	 left join assoc_doc_info_funzioni adif ON di.id_doc_info = adif.id_doc_info AND Isnull(adif.id_riunione)  
	 left join assoc_doc_info_contributrici adic ON di.id_doc_info = adic.id_doc_info AND Isnull(adic.id_riunione) 
	 left join assoc_doc_info_destinatari adid ON di.id_doc_info = adid.id_doc_info AND Isnull(adid.id_riunione)  
	 left join assoc_doc_info_sessioni adis ON di.id_doc_info = adis.id_doc_info  AND Isnull(adis.id_riunione) 
	 left join funzioni fu on fu.id_funzione = adif.id_funzione
	 left join funzioni fuc on fuc.id_funzione = adic.id_funzione
	 left join mesi m ON   m.id = adip.numero_mese 
	 left join organi o ON   o.id_organo = adio.id_organo
	 left join periodicita p ON   di.periodicita = p.id_periodicita 
 GROUP  BY di.id_documento, di.id_doc_info 
 ORDER  BY di.id_doc_info;
 
 
 CREATE or replace VIEW _vgetdocinfoprevisti AS 
 SELECT 'p' AS tipo,
		di.cod_tipo_doc AS cod_tipo_doc,
		di.id_documento AS id_documento,
		NULL AS anno,
		adip.numero_mese AS mese,
		NULL AS n_verbale,
		NULL AS data_riunione,
		adio.id_organo AS id_organo,
		NULL AS id_riunione,
		di.id_doc_info AS id_doc_info,
		di.titolo AS titolo,
		di.periodicita AS periodicita,
		di.norma_esterna AS norma_esterna,
		di.norma_interna AS norma_interna,
		di.finalita AS finalita,
		di.descrizione AS descrizione,
		NULL AS flag_ricevuto,
		NULL AS peso,
		GROUP_CONCAT(DISTINCT adif.id_funzione ORDER BY adif.id_funzione ASC SEPARATOR ',') AS funzioni,
		GROUP_CONCAT(DISTINCT adid.id_destinatario ORDER BY adid.id_destinatario ASC SEPARATOR ',') AS sessioni,
		GROUP_CONCAT(DISTINCT adis.id_sessione ORDER BY adis.id_sessione ASC SEPARATOR ',') AS destinatari
FROM doc_info di
	LEFT JOIN assoc_doc_info_funzioni adif ON di.id_doc_info = adif.id_doc_info AND ISNULL(adif.id_riunione)
	LEFT JOIN assoc_doc_info_destinatari adid ON di.id_doc_info = adid.id_doc_info AND ISNULL(adid.id_riunione)
	LEFT JOIN assoc_doc_info_organi adio ON di.id_doc_info = adio.id_doc_info AND ISNULL(adio.id_riunione)
	LEFT JOIN assoc_doc_info_presentazione adip ON di.id_doc_info = adip.id_doc_info AND ISNULL(adip.id_riunione)
	LEFT JOIN assoc_doc_info_sessioni adis ON di.id_doc_info = adis.id_doc_info AND ISNULL(adis.id_riunione)
WHERE di.attivo = 1
GROUP BY di.id_doc_info,
adip.numero_mese,
adio.id_organo
ORDER BY di.id_doc_info;



CREATE or replace VIEW _vgetdocinforiunione AS 
SELECT 'r' AS tipo,
	di.cod_tipo_doc AS cod_tipo_doc,
	di.id_documento AS id_documento,
	r.anno AS anno,
	r.n_verbale AS n_verbale,
	r.mese AS mese,
	r.data_ora AS data_riunione,
	r.id_organo AS id_organo,
	r.id_riunione AS id_riunione,
	di.id_doc_info AS id_doc_info,
	di.titolo AS titolo,
	di.periodicita AS periodicita,
	di.norma_esterna AS norma_esterna,
	di.norma_interna AS norma_interna,
	di.finalita AS finalita,
	di.descrizione AS descrizione,
	adir.flag_ricevuto AS flag_ricevuto,
	adir.peso AS peso,
	GROUP_CONCAT(DISTINCT adif.id_funzione ORDER BY adif.id_funzione ASC SEPARATOR ',') AS funzioni,
	GROUP_CONCAT(DISTINCT adid.id_destinatario ORDER BY adid.id_destinatario ASC SEPARATOR ',') AS sessioni,
	GROUP_CONCAT(DISTINCT adis.id_sessione ORDER BY adis.id_sessione ASC SEPARATOR ',') AS destinatari
FROM doc_info_riunioni di
	LEFT JOIN assoc_riunioni_doc_info adir ON adir.id_doc_info = di.id_doc_info
	LEFT JOIN riunioni r ON r.id_riunione = adir.id_riunione
	LEFT JOIN assoc_doc_info_funzioni adif ON di.id_doc_info = adif.id_doc_info AND adif.id_riunione = r.id_riunione 
	LEFT JOIN assoc_doc_info_destinatari adid ON di.id_doc_info = adid.id_doc_info AND adid.id_riunione = r.id_riunione 
	LEFT JOIN assoc_doc_info_sessioni adis ON di.id_doc_info = adis.id_doc_info AND adis.id_riunione = r.id_riunione 
WHERE  adir.attivo = 1
GROUP BY di.id_doc_info,r.mese,r.id_organo
ORDER BY di.id_doc_info;

 
CREATE or replace VIEW _vgetdocriunioni AS 
select 'r' AS tipo,
di.cod_tipo_doc AS cod_tipo_doc,
di.id_documento AS id_documento,
r.anno AS anno,
r.n_verbale AS n_verbale,
r.mese AS mese,
r.data_ora AS data_riunione,
r.id_organo AS id_organo,
r.id_riunione AS id_riunione,
di.id_doc_info AS id_doc_info,
di.titolo AS titolo,
adir.flag_ricevuto AS flag_ricevuto,
adir.peso AS peso,
fu.sigla AS sigla,
se.descrizione AS sessione 
from riunioni r 
 left join doc_info_riunioni di on r.id_riunione = di.id_riunione  
 left join assoc_riunioni_doc_info adir on adir.id_doc_info = di.id_doc_info and adir.id_riunione = di.id_riunione  
 left join assoc_doc_info_funzioni adif on di.id_doc_info = adif.id_doc_info and adif.id_riunione = di.id_riunione  
 left join funzioni fu on fu.id_funzione = adif.id_funzione 
 left join assoc_doc_info_sessioni adis on di.id_doc_info = adis.id_doc_info and adis.id_riunione = di.id_riunione  
 left join sessioni se on se.id_sessione = adis.id_sessione;


CREATE or replace VIEW _vgetdocprevisti AS 
select 'p' AS tipo,
di.cod_tipo_doc AS cod_tipo_doc,
di.id_documento AS id_documento,
ri.anno AS anno,
NULL AS n_verbale,
adip.numero_mese AS mese,
NULL AS data_riunione,
adio.id_organo AS id_organo,
NULL AS id_riunione,
di.id_doc_info AS id_doc_info,
di.titolo AS titolo,
0 AS flag_ricevuto,
0 AS peso,
fu.sigla AS sigla,
se.descrizione AS sessione 
from doc_info di 
join assoc_doc_info_presentazione adip on di.id_doc_info = adip.id_doc_info 
join assoc_doc_info_organi adio on di.id_doc_info = adio.id_doc_info 
join assoc_doc_info_funzioni adif on di.id_doc_info = adif.id_doc_info 
join funzioni fu on fu.id_funzione = adif.id_funzione 
left join assoc_doc_info_sessioni adis on di.id_doc_info = adis.id_doc_info and isnull(adis.id_riunione)
left join sessioni se on se.id_sessione = adis.id_sessione 
left join riunioni ri on ri.mese = adip.numero_mese and ri.id_organo = adio.id_organo 
left join assoc_riunioni_doc_info ar on ar.id_doc_info = di.id_doc_info and ri.id_riunione = ar.id_riunione 
where 1 = 1 and isnull(ar.id_doc_info) and di.attivo = 1;



CREATE or replace VIEW _vgetdocinfoprevisti AS 
select 'p' AS tipo,
di.cod_tipo_doc AS cod_tipo_doc,
di.id_documento AS id_documento,
NULL AS anno,
adip.numero_mese AS mese,
NULL AS n_verbale,
NULL AS data_riunione,
adio.id_organo AS id_organo,
NULL AS id_riunione,
di.id_doc_info AS id_doc_info,
di.titolo AS titolo,
di.periodicita AS periodicita,
di.norma_esterna AS norma_esterna,
di.norma_interna AS norma_interna,
di.finalita AS finalita,
di.descrizione AS descrizione,
NULL AS flag_ricevuto,
NULL AS peso,
group_concat(distinct adif.id_funzione order by adif.id_funzione ASC separator ',') AS funzioni,
group_concat(distinct adid.id_destinatario order by adid.id_destinatario ASC separator ',') AS sessioni,
group_concat(distinct adis.id_sessione order by adis.id_sessione ASC separator ',') AS destinatari 
from doc_info di 
left join assoc_doc_info_funzioni adif on di.id_doc_info = adif.id_doc_info and isnull(adif.id_riunione)
left join assoc_doc_info_destinatari adid on di.id_doc_info = adid.id_doc_info and isnull(adid.id_riunione) 
left join assoc_doc_info_organi adio on di.id_doc_info = adio.id_doc_info
left join assoc_doc_info_presentazione adip on di.id_doc_info = adip.id_doc_info
left join assoc_doc_info_sessioni adis on di.id_doc_info = adis.id_doc_info and isnull(adis.id_riunione)
where di.attivo = 1 group by di.id_doc_info,
adip.numero_mese,
adio.id_organo order by di.id_doc_info;



 CREATE or replace VIEW _vdoctot AS 
 SELECT vdp.tipo AS tipo,
		vdp.cod_tipo_doc AS cod_tipo_doc,
		vdp.id_documento AS id_documento,
		vdp.anno AS anno,
		vdp.n_verbale AS n_verbale,
		vdp.mese AS mese,
		vdp.data_riunione AS data_riunione,
		vdp.id_organo AS id_organo,
		vdp.id_riunione AS id_riunione,
		vdp.id_doc_info AS id_doc_info,
		vdp.titolo AS titolo,
		vdp.flag_ricevuto AS flag_ricevuto,
		vdp.peso AS peso,
		vdp.sigla AS sigla,
	    vdp.sessione AS sessione
FROM _vgetdocriunioni vdp 

UNION

SELECT vdr.tipo AS tipo,
		vdr.cod_tipo_doc AS cod_tipo_doc,
		vdr.id_documento AS id_documento,
		vdr.anno AS anno,
		vdr.n_verbale AS n_verbale,
		vdr.mese AS mese,
		vdr.data_riunione AS data_riunione,
		vdr.id_organo AS id_organo,
		vdr.id_riunione AS id_riunione,
		vdr.id_doc_info AS id_doc_info,
		vdr.titolo AS titolo,
		vdr.flag_ricevuto AS flag_ricevuto,
		vdr.peso AS peso,
		vdr.sigla AS sigla,
		vdr.sessione AS sessione
FROM _vgetdocprevisti vdr;



CREATE or replace VIEW _vgetdocinfo AS 
SELECT s.idx AS idx,
	s.tipo AS tipo,
	s.id_documento AS id_documento,
	s.anno AS anno,
	s.n_verbale AS n_verbale,
	s.mese AS mese,
	s.data_riunione AS data_riunione,
	s.id_organo AS id_organo,
	s.id_riunione AS id_riunione,
	s.id_doc_info AS id_doc_info,
	s.titolo AS titolo,
	s.periodicita AS periodicita,
	s.norma_esterna AS norma_esterna,
	s.norma_interna AS norma_interna,
	s.finalita AS finalita,
	s.descrizione AS descrizione,
	s.flag_ricevuto AS flag_ricevuto,
	s.peso AS peso,
	s.id_funzione AS id_funzione,
	s.id_destinatario AS id_destinatario,
	s.id_sessione AS id_sessione
FROM (
		SELECT 1 AS idx,
		r.tipo AS tipo,
		r.id_documento AS id_documento,
		r.anno AS anno,
		r.n_verbale AS n_verbale,
		r.mese AS mese,
		r.data_riunione AS data_riunione,
		r.id_organo AS id_organo,
		r.id_riunione AS id_riunione,
		r.id_doc_info AS id_doc_info,
		r.titolo AS titolo,
		r.periodicita AS periodicita,
		r.norma_esterna AS norma_esterna,
		r.norma_interna AS norma_interna,
		r.finalita AS finalita,
		r.descrizione AS descrizione,
		r.flag_ricevuto AS flag_ricevuto,
		r.peso AS peso,
		r.funzioni AS id_funzione,
		r.destinatari AS id_destinatario,
		r.sessioni AS id_sessione
		FROM _vgetdocinforiunione r 
	UNION
		SELECT 0 AS idx,
		r.tipo AS tipo,
		r.id_documento AS id_documento,
		r.anno AS anno,
		r.mese AS mese,
		r.n_verbale AS n_verbale,
		r.data_riunione AS data_riunione,
		r.id_organo AS id_organo,
		r.id_riunione AS id_riunione,
		r.id_doc_info AS id_doc_info,
		r.titolo AS titolo,
		r.periodicita AS periodicita,
		r.norma_esterna AS norma_esterna,
		r.norma_interna AS norma_interna,
		r.finalita AS finalita,
		r.descrizione AS descrizione,
		r.flag_ricevuto AS flag_ricevuto,
		r.peso AS peso,
		r.funzioni AS id_funzione,
		r.destinatari AS id_destinatario,
		r.sessioni AS id_sessione
		FROM _vgetdocinfoprevisti r) s
		WHERE 1
ORDER BY s.idx DESC;


