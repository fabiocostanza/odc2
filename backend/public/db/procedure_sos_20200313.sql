DELIMITER $$
 
CREATE FUNCTION `strip_tags`($str text)
RETURNS TEXT
BEGIN
    DECLARE $start, $end INT DEFAULT 1;
    LOOP
        SET $start = LOCATE("<", $str, $start);
        IF (!$start) THEN RETURN $str; END IF;
        SET $end = LOCATE(">", $str, $start);
        IF (!$end) THEN SET $end = $start; END IF;
        SET $str = INSERT($str, $start, $end - $start + 1, "");
    END LOOP;
END$$
DELIMITER ;


-- Dump della struttura di procedura odc2._pinsert_doc_riunione
DELIMITER //
CREATE DEFINER=`SQLwebteam`@`%` PROCEDURE `_pinsert_doc_riunione`(
	IN `p_id_doc_info` INT,
	IN `p_id_organo` INT,
	IN `p_mese` INT,
	IN `p_anno` INT








,
	OUT `o_id_riunione` INT


)
BEGIN
DECLARE v_id_riunione int DEFAULT NULL;
DECLARE n_id_riunione int DEFAULT NULL;
DECLARE v_doc_info_riunioni int DEFAULT NULL;

START TRANSACTION;

SET v_id_riunione := (select id_riunione from riunioni r where r.id_organo=p_id_organo and r.mese = p_mese and r.anno = p_anno);

IF(v_id_riunione IS NULL) THEN
    insert into riunioni (id_organo, mese, anno) values (p_id_organo,p_mese,p_anno);
END IF;

SET n_id_riunione := (select id_riunione from riunioni r where r.id_organo=p_id_organo and r.mese = p_mese and r.anno = p_anno);
SET o_id_riunione := n_id_riunione;
-- SELECT concat('n_id_riunione is ', n_id_riunione);
SET v_doc_info_riunioni := (select id from doc_info_riunioni r where r.id_doc_info = p_id_doc_info and r.id_riunione = n_id_riunione);

IF(v_doc_info_riunioni IS NULL) THEN    

-- SELECT concat('v_doc_info_riunioni is ', v_doc_info_riunioni);

delete from doc_info_riunioni where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO doc_info_riunioni
(id_doc_info, id_riunione, id_documento, periodicita, titolo, descrizione, norma_esterna, norma_interna, finalita, uid_modifica, data_modifica,cod_tipo_doc)
(SELECT di.id_doc_info, n_id_riunione id_riunione, di.id_documento, di.periodicita, di.titolo, di.descrizione, di.norma_esterna, di.norma_interna, di.finalita, di.uid_modifica, di.data_modifica, 
di.cod_tipo_doc
FROM  doc_info di 
where di.id_doc_info = p_id_doc_info);

delete from assoc_riunioni_doc_info where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO assoc_riunioni_doc_info (id_riunione, id_doc_info, uid_modifica)
values (n_id_riunione, p_id_doc_info,'B07128');

delete from assoc_doc_info_funzioni where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO assoc_doc_info_funzioni (id_riunione, id_funzione, id_doc_info, uid_modifica)
(SELECT n_id_riunione id_riunione, di.id_funzione,di.id_doc_info,'B07128'
FROM assoc_doc_info_funzioni di 
where di.id_doc_info = p_id_doc_info and di.id_riunione is null);

delete from assoc_doc_info_destinatari where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO assoc_doc_info_destinatari 
(id_riunione, id_destinatario,id_doc_info, uid_modifica)
(SELECT
n_id_riunione id_riunione, di.id_destinatario,di.id_doc_info,'B07128'
FROM assoc_doc_info_destinatari di 
where di.id_doc_info = p_id_doc_info and di.id_riunione is null);

delete from assoc_doc_info_sessioni where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO assoc_doc_info_sessioni
(id_riunione, id_sessione,id_doc_info, uid_modifica)
(SELECT n_id_riunione id_riunione, di.id_sessione ,di.id_doc_info,'B07128'
FROM assoc_doc_info_sessioni di
inner join sessioni se on se.id_sessione = di.id_sessione and se.id_organo = p_id_organo
where di.id_doc_info = p_id_doc_info and di.id_riunione is null);

END IF;
COMMIT;

END//
DELIMITER ;