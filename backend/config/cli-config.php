<?php

declare(strict_types = 1);

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\EntityManager;
use Wire\Config\ConfigFactory;

require 'vendor/autoload.php';

/** @var \Interop\Container\ContainerInterface $container */
$config = require 'config/config.php';

// the Service Manager Container
/** @var \Zend\ServiceManager\ServiceManager $serviceContainer */
$container = Wire\Bootstrap::getInstance($config)->init();

ConfigFactory::getInstance($container->get('config'));

/** @var \Doctrine\ORM\EntityManager $em */
$em = $container->get(EntityManager::class);

return ConsoleRunner::createHelperSet($em);
