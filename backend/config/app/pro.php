<?php

declare(strict_types = 1);

use Zend\ConfigAggregator\ConfigAggregator;

return [
	'app' => [
		'id' => 'odc2',
		'name' => '',
	],
	'debug' => false,
	ConfigAggregator::ENABLE_CACHE => true
];
