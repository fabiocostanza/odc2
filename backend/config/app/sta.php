<?php

declare(strict_types = 1);

use Zend\ConfigAggregator\ConfigAggregator;

return [
	'app' => [
		'id' => 'odc2',
		'name' => '',
	],
	'debug' => true,
	ConfigAggregator::ENABLE_CACHE => true
];
