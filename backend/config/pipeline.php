<?php

declare(strict_types = 1);

use Zend\Expressive\Application;
use Zend\Expressive\MiddlewareFactory;
use Psr\Container\ContainerInterface;

/**
 * Setup middleware pipeline:
 */
return function (Application $app, MiddlewareFactory $factory, ContainerInterface $container): void {
    // list all the specific middleware of the app
};
