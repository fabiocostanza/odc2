<?php

declare(strict_types = 1);

use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ConfigAggregator\PhpFileProvider;

$env = getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'dev';

$aggregator = new ConfigAggregator([
	\Blast\BaseUrl\ConfigProvider::class,
	\Zend\Expressive\Session\Ext\ConfigProvider::class,
	\Zend\I18n\ConfigProvider::class,
	\Zend\Db\ConfigProvider::class,
	\Zend\Expressive\Session\ConfigProvider::class,
	\Zend\Expressive\Authorization\Acl\ConfigProvider::class,
	\Zend\Expressive\Authorization\ConfigProvider::class,
	\Zend\Expressive\Authentication\ConfigProvider::class,
	\Zend\Expressive\Hal\ConfigProvider::class,
	\Zend\Expressive\ConfigProvider::class,
	\Zend\HttpHandlerRunner\ConfigProvider::class,
	\Zend\Expressive\Router\ZendRouter\ConfigProvider::class,
	\Zend\Router\ConfigProvider::class,
// 	\Zend\Expressive\ZendView\ConfigProvider::class,
	\Zend\Expressive\Helper\ConfigProvider::class,
	\Zend\Expressive\Router\ConfigProvider::class,
	\Zend\Hydrator\ConfigProvider::class,
	\Zend\InputFilter\ConfigProvider::class,
	\Zend\Filter\ConfigProvider::class,
	\Zend\Validator\ConfigProvider::class,
	\Zend\Paginator\ConfigProvider::class,
	new PhpFileProvider(realpath(__DIR__) . "/app/$env.php"),
    new PhpFileProvider(realpath(__DIR__) . "/autoload/*.php"),
    App\ConfigProvider::class,
]);

return $aggregator->getMergedConfig();
