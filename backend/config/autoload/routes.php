<?php
return array(
    'routes' => array(
        0 => array(
            'path' => '/sso/oidccb',
            'middleware' => array(
                0 => 'Wire\\Access\\Middleware\\AccessTokenMiddleware',
                1 => 'Wire\\Access\\Middleware\\UserCredentialMiddleware',
                2 => 'Wire\\Access\\Middleware\\RoleProviderMiddleware',
                3 => 'Wire\\Access\\Middleware\\AuthorizationTokenMiddleware',
                4 => 'Wire\\Handlers\\Access\\AccessHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'sso/oidccb',
            'options' => array(
                'noauth' => true,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        1 => array(
            'path' => '/auth/oidc',
            'middleware' => array(
                0 => 'Wire\\Handlers\\Access\\AuthenticateHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'auth/oidc',
            'options' => array(
                'noauth' => true,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        2 => array(
            'path' => '/api/acl',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Authorization\\AclHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'authorization/acl#acl',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        3 => array(
            'path' => '/api/delegas',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\DeleteDelegasHandler',
            ),
            'allowed_methods' => array(
                0 => 'DELETE',
            ),
            'name' => 'credential/delegas#deletedelegas',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        4 => array(
            'path' => '/api/functionality',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\DeleteFunctionalitiesHandler',
            ),
            'allowed_methods' => array(
                0 => 'DELETE',
            ),
            'name' => 'credential/functionality#deletefunctionalities',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        5 => array(
            'path' => '/api/functionalityresource',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\DeleteFunctionalityResourceHandler',
            ),
            'allowed_methods' => array(
                0 => 'DELETE',
            ),
            'name' => 'credential/functionalityresource#deletefunctionalityresource',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        6 => array(
            'path' => '/api/rolefunctionality',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\DeleteRoleFunctionalityHandler',
            ),
            'allowed_methods' => array(
                0 => 'DELETE',
            ),
            'name' => 'credential/rolefunctionality#deleterolefunctionality',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        7 => array(
            'path' => '/api/role',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\DeleteRoleHandler',
            ),
            'allowed_methods' => array(
                0 => 'DELETE',
            ),
            'name' => 'credential/role#deleterole',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        8 => array(
            'path' => '/api/functionality',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\EditFunctionalitiesHandler',
            ),
            'allowed_methods' => array(
                0 => 'PATCH',
            ),
            'name' => 'credential/functionality#editfunctionalities',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        9 => array(
            'path' => '/api/role',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\EditRoleHandler',
            ),
            'allowed_methods' => array(
                0 => 'PUT',
            ),
            'name' => 'credential/role#editrole',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        10 => array(
            'path' => '/api/delegas',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\InsertDelegasHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'credential/delegas#insertdelegas',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        11 => array(
            'path' => '/api/functionality',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\InsertFunctionalitiesHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'credential/functionality#insertfunctionalities',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        12 => array(
            'path' => '/api/functionalityresource',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\InsertFunctionalityResourceHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'credential/functionalityresource#insertfunctionalityresource',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        13 => array(
            'path' => '/api/rolefunctionality',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\InsertRoleFunctionalityHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'credential/rolefunctionality#insertrolefunctionality',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        14 => array(
            'path' => '/api/role',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\InsertRoleHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'credential/role#insertrole',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        15 => array(
            'path' => '/api/hotlines',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\LeaveHotlinesHandler',
            ),
            'allowed_methods' => array(
                0 => 'DELETE',
            ),
            'name' => 'credential/hotlines#leavehotlines',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        16 => array(
            'path' => '/api/delegas',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListDelegasHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/delegas#listdelegas',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        17 => array(
            'path' => '/api/functionality',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListFunctionalitiesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/functionality#listfunctionalities',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        18 => array(
            'path' => '/api/functionalityresource',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListFunctionalityResourceHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/functionalityresource#listfunctionalityresource',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        19 => array(
            'path' => '/api/hotlines',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListHotlinesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/hotlines#listhotlines',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        20 => array(
            'path' => '/api/hotlinesjobs',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListHotlinesJobsHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/hotlinesjobs#listhotlinesjobs',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        21 => array(
            'path' => '/api/hotlinesoffices',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListHotlinesOfficesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/hotlinesoffices#listhotlinesoffices',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        22 => array(
            'path' => '/api/resource',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListResourcesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/resource#listresources',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        23 => array(
            'path' => '/api/rolefunctionality',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListRoleFunctionalitiesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/rolefunctionality#listrolefunctionalities',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        24 => array(
            'path' => '/api/role',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\ListRolesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/role#listroles',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        25 => array(
            'path' => '/api/resource',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Credential\\Middleware\\ResourceCredentialMiddleware',
                3 => 'Wire\\Handlers\\Credential\\ResourcesHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'credential/resource#resources',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        26 => array(
            'path' => '/api/hotlines',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\TakeHotlinesHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'credential/hotlines#takehotlines',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        27 => array(
            'path' => '/api/takerole',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\TakeRoleHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'credential/takerole#takerole',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        28 => array(
            'path' => '/api/role',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\UpdateRoleHandler',
            ),
            'allowed_methods' => array(
                0 => 'PATCH',
            ),
            'name' => 'credential/role#updaterole',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        29 => array(
            'path' => '/api/userdetails',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Credential\\UserDetailsHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'credential/userdetails#userdetails',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        30 => array(
            'path' => '/api/environment',
            'middleware' => array(
                0 => 'Wire\\Handlers\\Environment\\EnvironmentHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'environment/environment#environment',
            'options' => array(
                'noauth' => true,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        31 => array(
            'path' => '/api/employees',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Referential\\ListEmployeesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'referential/employees#listemployees',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        32 => array(
            'path' => '/api/employeesjobs',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Referential\\ListEmployeesJobsHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'referential/employeesjobs#listemployeesjobs',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        33 => array(
            'path' => '/api/employeesoffices',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Referential\\ListEmployeesOfficesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'referential/employeesoffices#listemployeesoffices',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        34 => array(
            'path' => '/api/jobs',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Referential\\ListJobsHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'referential/jobs#listjobs',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        35 => array(
            'path' => '/api/offices',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Referential\\ListOfficesHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'referential/offices#listoffices',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        36 => array(
            'path' => '/api/respuo',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\Referential\\ListRespuoHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'referential/respuo#listrespuo',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        37 => array(
            'path' => '/api/sm/anagrafe',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\AnagrafeHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/anagrafe#anagrafe',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        38 => array(
            'path' => '/api/sm/ccordinari',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\CcordinariHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/ccordinari#ccordinari',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        39 => array(
            'path' => '/api/sm/circolarita',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\CircolaritaHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/circolarita#circolarita',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        40 => array(
            'path' => '/api/sm/cointestati',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\CointestatiHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/cointestati#cointestati',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        41 => array(
            'path' => '/api/sm/contabile',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\ContabileHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/contabile#contabile',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        42 => array(
            'path' => '/api/sm/conto2creditori',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\Conto2CreditoriHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/conto2creditori#conto2creditori',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        43 => array(
            'path' => '/api/sm/cope',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\CopeHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/cope#cope',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        44 => array(
            'path' => '/api/sm/creditoridba',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\CreditoridbaHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/creditoridba#creditoridba',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        45 => array(
            'path' => '/api/sm/creditoridbp',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\CreditoridbpHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/creditoridbp#creditoridbp',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        46 => array(
            'path' => '/api/sm/customer',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\CustomerHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/customer#customer',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        47 => array(
            'path' => '/api/sm/customerinfo',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\CustomerInfoHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/customerinfo#customerinfo',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        48 => array(
            'path' => '/api/sm/domiciliazioni',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\DomiciliazioniHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/domiciliazioni#domiciliazioni',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        49 => array(
            'path' => '/api/sm/fase',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\FaseHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/fase#fase',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        50 => array(
            'path' => '/api/sm/mercato',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\MercatoHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/mercato#mercato',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        51 => array(
            'path' => '/api/sm/portafoglio',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\PortafoglioHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/portafoglio#portafoglio',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        52 => array(
            'path' => '/api/sm/rating',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\RatingHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/rating#rating',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        53 => array(
            'path' => '/api/sm/settorics',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\SettoriCSHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/settorics#settorics',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        54 => array(
            'path' => '/api/sm/sia2creditori',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'Wire\\Handlers\\ServiceManager\\Sia2CreditoriHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'servicemanager/sm/sia2creditori#sia2creditori',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        55 => array(
            'path' => '/test/service',
            'middleware' => array(
                0 => 'Wire\\Handlers\\TestServiceHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'testservice#testing',
            'options' => array(
                'noauth' => true,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        56 => array(
            'path' => '/api/hattivo',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\Attivo\\AnagraficaAttivoListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/attivo/hattivo#anagraficaattivolist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        57 => array(
            'path' => '/api/hazioni',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\Azioni\\AnagraficaAzioniListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/azioni/hazioni#anagraficaazionilist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        58 => array(
            'path' => '/api/hdestinatari',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\Destinatari\\AnagraficaDestinatariListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/destinatari/hdestinatari#anagraficadestinatarilist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        59 => array(
            'path' => '/api/hfunzioni',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\Funzioni\\AnagraficaFunzioniListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/funzioni/hfunzioni#anagraficafunzionilist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        60 => array(
            'path' => '/api/hmesi',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\Mesi\\AnagraficaMesiListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/mesi/hmesi#anagraficamesilist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        61 => array(
            'path' => '/api/horgani',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\Organi\\AnagraficaOrganiListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/organi/horgani#anagraficaorganilist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        62 => array(
            'path' => '/api/hperiodicita',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\Periodicita\\AnagraficaPeriodicitaListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/periodicita/hperiodicita#anagraficaperiodicitalist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        63 => array(
            'path' => '/api/hsessioni',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\Sessioni\\AnagraficaSessioniListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/sessioni/hsessioni#anagraficasessionilist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        64 => array(
            'path' => '/api/htipodoc',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\Anagrafica\\TipoDoc\\AnagraficaTipoDocListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'anagrafica/tipodoc/htipodoc#anagraficatipodoclist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        65 => array(
            'path' => '/api/doccensiti',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\DocCensiti\\DocCensitiEditGetHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'doccensiti/doccensiti#doccensitieditget',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        66 => array(
            'path' => '/api/doccensiti',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\DocCensiti\\DocCensitiEditUpdateHandler',
            ),
            'allowed_methods' => array(
                0 => 'PATCH',
            ),
            'name' => 'doccensiti/doccensiti#doccensitieditupdate',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        67 => array(
            'path' => '/api/doccensitiexport',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\DocCensiti\\DocCensitiExportHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'doccensiti/doccensitiexport#doccensitiexport',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        68 => array(
            'path' => '/api/doccensitilist',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\DocCensiti\\DocCensitiListHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'doccensiti/doccensitilist#doccensitilist',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        69 => array(
            'path' => '/api/doccensiti',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\DocCensiti\\DocCensitiNewInsertHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'doccensiti/doccensiti#doccensitinewinsert',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        70 => array(
            'path' => '/api/doccensitisetattivo',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\DocCensiti\\DocCensitiSetAttivoHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'doccensiti/doccensitisetattivo#doccensitisetattivo',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        71 => array(
            'path' => '/api/homedocedit',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocEditGetHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'homedoc/homedocedit#homedoceditget',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        72 => array(
            'path' => '/api/homedocedit',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocEditUpdateHandler',
            ),
            'allowed_methods' => array(
                0 => 'PATCH',
            ),
            'name' => 'homedoc/homedocedit#homedoceditupdate',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        73 => array(
            'path' => '/api/homedoclistmesi',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocListMesiHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'homedoc/homedoclistmesi#homedoclistmesi',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        74 => array(
            'path' => '/api/homedocnew',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocNewGetParamHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'homedoc/homedocnew#homedocnewgetparam',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        75 => array(
            'path' => '/api/homedocnew',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocNewInsertHandler',
            ),
            'allowed_methods' => array(
                0 => 'PATCH',
            ),
            'name' => 'homedoc/homedocnew#homedocnewinsert',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        76 => array(
            'path' => '/api/homedocprevisti',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocPrevistiHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'homedoc/homedocprevisti#homedocprevisti',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        77 => array(
            'path' => '/api/homedocsetdatariunione',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocSetDataRiunioneHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'homedoc/homedocsetdatariunione#homedocsetdatariunione',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        78 => array(
            'path' => '/api/hodocsetflagricevuto',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocSetFlagRicevutoHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'homedoc/hodocsetflagricevuto#homedocsetflagricevuto',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        79 => array(
            'path' => '/api/homedocsetpeso',
            'middleware' => array(
                0 => 'Zend\\Expressive\\Authentication\\AuthenticationMiddleware',
                1 => 'Zend\\Expressive\\Authorization\\AuthorizationMiddleware',
                2 => 'App\\Handlers\\HomeDoc\\HomeDocSetPesoHandler',
            ),
            'allowed_methods' => array(
                0 => 'POST',
            ),
            'name' => 'homedoc/homedocsetpeso#homedocsetpeso',
            'options' => array(
                'noauth' => false,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
        80 => array(
            'path' => '/api/ping',
            'middleware' => array(
                0 => 'App\\Handlers\\PingHandler',
            ),
            'allowed_methods' => array(
                0 => 'GET',
            ),
            'name' => 'ping#handle',
            'options' => array(
                'noauth' => true,
                'action_name' => 'handle',
                '__autoloaded' => true,
            ),
        ),
    ),
);
