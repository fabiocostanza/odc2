-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Creato il: Dic 06, 2019 alle 16:42
-- Versione del server: 5.7.26
-- Versione PHP: 7.2.18

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `odc2`
--
CREATE DATABASE IF NOT EXISTS `odc2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `odc2`;

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_delegate`
--

DROP TABLE IF EXISTS `_cred_delegate`;
CREATE TABLE IF NOT EXISTS `_cred_delegate` (
  `dl_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dl_uid` varchar(20) NOT NULL,
  `dl_delegate_uid` varchar(20) NOT NULL,
  `dl_start_date` datetime DEFAULT NULL,
  `dl_end_date` datetime DEFAULT NULL,
  `dl_creation_date` datetime DEFAULT NULL,
  `dl_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`dl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_function`
--

DROP TABLE IF EXISTS `_cred_function`;
CREATE TABLE IF NOT EXISTS `_cred_function` (
  `hf_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hf_function` varchar(50) NOT NULL DEFAULT '',
  `hf_args` varchar(200) DEFAULT NULL,
  `hf_role_id` int(13) UNSIGNED NOT NULL DEFAULT '0',
  `hf_start_date` datetime DEFAULT NULL,
  `hf_end_date` datetime DEFAULT NULL,
  `hf_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`hf_id`),
  KEY `hf_function` (`hf_function`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_functionality`
--

DROP TABLE IF EXISTS `_cred_functionality`;
CREATE TABLE IF NOT EXISTS `_cred_functionality` (
  `functionality_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `functionality` varchar(50) NOT NULL DEFAULT '',
  `administrable` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `heritable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`functionality_id`),
  UNIQUE KEY `fct_label` (`functionality`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dump dei dati per la tabella `_cred_functionality`
--

INSERT INTO `_cred_functionality` (`functionality_id`, `functionality`, `administrable`, `heritable`) VALUES
(1, 'Funzionalita di base', 1, 1),
(6, 'Gestione delle abilitazioni', 1, 1),
(11, 'Hotline', 1, 1),
(16, 'Intera applicazione', 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_functionality_acl_weight`
--

DROP TABLE IF EXISTS `_cred_functionality_acl_weight`;
CREATE TABLE IF NOT EXISTS `_cred_functionality_acl_weight` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `acl` enum('enabled','disabled','hidden') NOT NULL DEFAULT 'disabled',
  `weight` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `acl` (`acl`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `_cred_functionality_acl_weight`
--

INSERT INTO `_cred_functionality_acl_weight` (`id`, `acl`, `weight`) VALUES
(1, 'enabled', 3),
(2, 'disabled', 2),
(3, 'hidden', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_functionality_resource`
--

DROP TABLE IF EXISTS `_cred_functionality_resource`;
CREATE TABLE IF NOT EXISTS `_cred_functionality_resource` (
  `functionality_resource_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `functionality_id` int(13) UNSIGNED NOT NULL DEFAULT '0',
  `resource_id` varchar(32) NOT NULL DEFAULT '',
  `acl` enum('enabled','disabled','hidden') NOT NULL DEFAULT 'disabled',
  PRIMARY KEY (`functionality_resource_id`),
  UNIQUE KEY `fr_fct_id` (`functionality_id`,`resource_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2942 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dump dei dati per la tabella `_cred_functionality_resource`
--

INSERT INTO `_cred_functionality_resource` (`functionality_resource_id`, `functionality_id`, `resource_id`, `acl`) VALUES
(2215, 11, '0581cafacd398932e5b4f6418683f5b3', 'hidden'),
(2216, 11, '08e379ea658f873de6134443f4013741', 'hidden'),
(2217, 11, '118a46e71ee56379179b54d5d2e2993f', 'hidden'),
(2218, 11, '12e92aa992a11cd5475c0a8c3d1879ec', 'hidden'),
(2219, 11, '140671fa52a6a5be0633c0a148f3300d', 'hidden'),
(2220, 11, '145e5be43b3eaba5bd1c92d3a60dec71', 'hidden'),
(2221, 11, '19694aead869fc540ebbe73032e6fa18', 'hidden'),
(2222, 11, '1aded72a1b5106f475471351d9b10125', 'hidden'),
(2223, 11, '1b534692b60f8e3c7ff60333a1bbaa0e', 'hidden'),
(2224, 11, '2704e9a4422c8e40b29a9648adc82b99', 'hidden'),
(2225, 11, '28962ad7513a157825db87260e7c5afd', 'hidden'),
(2226, 11, '296387918789a39edf1812c633806079', 'hidden'),
(2227, 11, '29a1654dbe178eea563f585f2612f382', 'hidden'),
(2228, 11, '29fce0d108a31c8bc776c277ee541b5c', 'hidden'),
(2229, 11, '2a5a82ea79c0c311d3d29bbbe5b49f2f', 'enabled'),
(2230, 11, '2b4f26b42daabb492974dda18c983285', 'enabled'),
(2231, 11, '3066c0eaf127d8caf42b981331608cd0', 'hidden'),
(2232, 11, '35d46795b2c0311bf116d3abd0d1e4e7', 'hidden'),
(2233, 11, '3639b8a295c35f9e5258bb6490b8a534', 'enabled'),
(2234, 11, '386e5addb689225ff89a430fa2ba2b71', 'hidden'),
(2235, 11, '3ac86657a58fcacb13db2dd8de82f5e7', 'hidden'),
(2236, 11, '3b25586f69eb1fca8690d6d446c15d31', 'hidden'),
(2237, 11, '3f02808656c1519e8713dca3ec231f68', 'hidden'),
(2238, 11, '43b9de56d5aa207e3b10a51405187368', 'hidden'),
(2239, 11, '49e2dc76be27396b284ba9ccf724511e', 'enabled'),
(2240, 11, '4f663ae28447e8b7bfc7a97e47b6f1e8', 'enabled'),
(2241, 11, '51e67d44ceb6dde6b50dbfb3f8e197ac', 'enabled'),
(2242, 11, '579be0d578358e0754d37e04e4b57259', 'hidden'),
(2243, 11, '5ad859905a64fc08843cb15b3c70c632', 'hidden'),
(2244, 11, '5d8a3eb94bcca4ce3ef1d94d3a43b065', 'hidden'),
(2245, 11, '6037296b70ff1af3cb8a5651aa326c6b', 'enabled'),
(2246, 11, '63faf55a1b49511e7278d0ff6c5c8160', 'hidden'),
(2247, 11, '7285074fb14b56763cf3e7c1a7f781c9', 'hidden'),
(2248, 11, '737371d1c0df6acc1658d403181a850f', 'hidden'),
(2249, 11, '73fce4cb4414106912d0e1ea16baa2c4', 'hidden'),
(2250, 11, '761fd2f727bd2b6c68a737ddbea46c10', 'hidden'),
(2251, 11, '786eb34f5833eee727bb7549aa72afaa', 'enabled'),
(2252, 11, '7b8e2e92cd82019f40d65a827697537d', 'hidden'),
(2253, 11, '817727543c01d2023267c6e84a10da74', 'hidden'),
(2254, 11, '8af39a26f04fc9af720a8aecb4ab49ed', 'hidden'),
(2255, 11, '8ccaa416dd32884e49d035dd2907331d', 'hidden'),
(2256, 11, '8d60091184eac27cb9998884bd2141b0', 'hidden'),
(2257, 11, '8dba03f38706424efd6406b48cc7eb21', 'enabled'),
(2258, 11, '8dfe18dad8e8a98917471c96f7cd43cb', 'enabled'),
(2259, 11, '8e56844e531d32d4cc728d59ab8b5fe6', 'hidden'),
(2260, 11, '8eb6cdfee1c3638250feeba5c935c5dc', 'enabled'),
(2261, 11, '9297be1d44d7c94142b5e8e8c5a63c02', 'enabled'),
(2262, 11, '96c965dfe4dec71cd062e7ca7b0e3df5', 'hidden'),
(2263, 11, '98bab1ebdb20ed6ea007862c14614d1a', 'hidden'),
(2264, 11, 'a66f8a3bb371ee25df3ea3eb70c3e9ec', 'hidden'),
(2265, 11, 'abc3c9509c714b56f571f9784a8d5bdf', 'hidden'),
(2266, 11, 'ac0d212f9ca85f5723999094bc128827', 'enabled'),
(2267, 11, 'ad7a91d112ca4d888371870edc48baaa', 'hidden'),
(2268, 11, 'b1d58ad4da479e09f97d003e53fe6133', 'hidden'),
(2269, 11, 'b29fc5f00b60d176eb22a26e2b908bac', 'hidden'),
(2270, 11, 'ba0415f92ed6e1a7b80be7082c3a2f2c', 'hidden'),
(2271, 11, 'bb118ff1a9ad48d596073fb8a9699501', 'hidden'),
(2272, 11, 'bd8d5c06ffa739fc2546314898a4edcb', 'hidden'),
(2273, 11, 'c3f0c17f884a03ae85573cd8c8a0511a', 'hidden'),
(2274, 11, 'c51142fa0528bdf36debecd56fdb0826', 'hidden'),
(2275, 11, 'c5ef9c0ddb343325e026684fddcfbe88', 'hidden'),
(2276, 11, 'ce09dd43394afe5a90726a971943172c', 'hidden'),
(2277, 11, 'ce5cf58b1516f2386f561e19f6fe07d5', 'enabled'),
(2278, 11, 'd43e20eec4f6c3424d2506242c84f847', 'hidden'),
(2279, 11, 'd484e10174f0e232a936ebb129c4f7dd', 'hidden'),
(2280, 11, 'd54b9ac6e2332a4869d317b3a8103dbf', 'hidden'),
(2281, 11, 'd7c210b8e88c19cbce32fdbe57ccd2f5', 'hidden'),
(2282, 11, 'd7f92c13a5136fc1d350abc48fe8dc4f', 'hidden'),
(2283, 11, 'da1dc284cc7b27555e590d289bcf96ae', 'hidden'),
(2284, 11, 'e1814eb37b22a56fa5d7979b36266e73', 'hidden'),
(2285, 11, 'e26628cfcc6872ab02646928decc1dd2', 'hidden'),
(2286, 11, 'e2c889de811134b4ae155416a81df9db', 'hidden'),
(2287, 11, 'e56260185280be666234019ec8bc59e6', 'hidden'),
(2288, 11, 'e669f07a6a5d6ee9640b41aa8bdd3f7d', 'hidden'),
(2289, 11, 'ed38d3c46063a8dc4ad2083dad2f0b1b', 'enabled'),
(2290, 11, 'f1c1f7479d7282a888e83fe488b9d07f', 'hidden'),
(2291, 11, 'fa854a9d7c61184b7423ac0051c0c8cc', 'hidden'),
(2292, 11, 'fe3337b52404cd26026a814c614ec040', 'hidden'),
(2449, 1, '0581cafacd398932e5b4f6418683f5b3', 'hidden'),
(2450, 1, '08e379ea658f873de6134443f4013741', 'hidden'),
(2451, 1, '118a46e71ee56379179b54d5d2e2993f', 'hidden'),
(2452, 1, '12e92aa992a11cd5475c0a8c3d1879ec', 'hidden'),
(2453, 1, '140671fa52a6a5be0633c0a148f3300d', 'hidden'),
(2454, 1, '145e5be43b3eaba5bd1c92d3a60dec71', 'hidden'),
(2455, 1, '19694aead869fc540ebbe73032e6fa18', 'hidden'),
(2456, 1, '1aded72a1b5106f475471351d9b10125', 'hidden'),
(2457, 1, '1b534692b60f8e3c7ff60333a1bbaa0e', 'hidden'),
(2458, 1, '2704e9a4422c8e40b29a9648adc82b99', 'hidden'),
(2459, 1, '28962ad7513a157825db87260e7c5afd', 'hidden'),
(2460, 1, '296387918789a39edf1812c633806079', 'hidden'),
(2461, 1, '29a1654dbe178eea563f585f2612f382', 'hidden'),
(2462, 1, '29fce0d108a31c8bc776c277ee541b5c', 'hidden'),
(2463, 1, '2a5a82ea79c0c311d3d29bbbe5b49f2f', 'hidden'),
(2464, 1, '2b4f26b42daabb492974dda18c983285', 'hidden'),
(2465, 1, '3066c0eaf127d8caf42b981331608cd0', 'hidden'),
(2466, 1, '35d46795b2c0311bf116d3abd0d1e4e7', 'hidden'),
(2467, 1, '3639b8a295c35f9e5258bb6490b8a534', 'hidden'),
(2468, 1, '386e5addb689225ff89a430fa2ba2b71', 'hidden'),
(2469, 1, '3ac86657a58fcacb13db2dd8de82f5e7', 'hidden'),
(2470, 1, '3b25586f69eb1fca8690d6d446c15d31', 'hidden'),
(2471, 1, '3f02808656c1519e8713dca3ec231f68', 'hidden'),
(2472, 1, '43b9de56d5aa207e3b10a51405187368', 'enabled'),
(2473, 1, '49e2dc76be27396b284ba9ccf724511e', 'hidden'),
(2474, 1, '4f663ae28447e8b7bfc7a97e47b6f1e8', 'hidden'),
(2475, 1, '51e67d44ceb6dde6b50dbfb3f8e197ac', 'hidden'),
(2476, 1, '579be0d578358e0754d37e04e4b57259', 'hidden'),
(2477, 1, '5ad859905a64fc08843cb15b3c70c632', 'hidden'),
(2478, 1, '5d8a3eb94bcca4ce3ef1d94d3a43b065', 'hidden'),
(2479, 1, '6037296b70ff1af3cb8a5651aa326c6b', 'hidden'),
(2480, 1, '63faf55a1b49511e7278d0ff6c5c8160', 'hidden'),
(2481, 1, '7285074fb14b56763cf3e7c1a7f781c9', 'hidden'),
(2482, 1, '737371d1c0df6acc1658d403181a850f', 'hidden'),
(2483, 1, '73fce4cb4414106912d0e1ea16baa2c4', 'hidden'),
(2484, 1, '761fd2f727bd2b6c68a737ddbea46c10', 'hidden'),
(2485, 1, '786eb34f5833eee727bb7549aa72afaa', 'hidden'),
(2486, 1, '7b8e2e92cd82019f40d65a827697537d', 'hidden'),
(2487, 1, '817727543c01d2023267c6e84a10da74', 'enabled'),
(2488, 1, '8af39a26f04fc9af720a8aecb4ab49ed', 'hidden'),
(2489, 1, '8ccaa416dd32884e49d035dd2907331d', 'hidden'),
(2490, 1, '8d60091184eac27cb9998884bd2141b0', 'enabled'),
(2491, 1, '8dba03f38706424efd6406b48cc7eb21', 'hidden'),
(2492, 1, '8dfe18dad8e8a98917471c96f7cd43cb', 'hidden'),
(2493, 1, '8e56844e531d32d4cc728d59ab8b5fe6', 'hidden'),
(2494, 1, '8eb6cdfee1c3638250feeba5c935c5dc', 'hidden'),
(2495, 1, '9297be1d44d7c94142b5e8e8c5a63c02', 'hidden'),
(2496, 1, '96c965dfe4dec71cd062e7ca7b0e3df5', 'hidden'),
(2497, 1, '98bab1ebdb20ed6ea007862c14614d1a', 'hidden'),
(2498, 1, 'a66f8a3bb371ee25df3ea3eb70c3e9ec', 'hidden'),
(2499, 1, 'abc3c9509c714b56f571f9784a8d5bdf', 'hidden'),
(2500, 1, 'ac0d212f9ca85f5723999094bc128827', 'hidden'),
(2501, 1, 'ad7a91d112ca4d888371870edc48baaa', 'hidden'),
(2502, 1, 'b1d58ad4da479e09f97d003e53fe6133', 'enabled'),
(2503, 1, 'b29fc5f00b60d176eb22a26e2b908bac', 'hidden'),
(2504, 1, 'ba0415f92ed6e1a7b80be7082c3a2f2c', 'enabled'),
(2505, 1, 'bb118ff1a9ad48d596073fb8a9699501', 'hidden'),
(2506, 1, 'bd8d5c06ffa739fc2546314898a4edcb', 'hidden'),
(2507, 1, 'c3f0c17f884a03ae85573cd8c8a0511a', 'hidden'),
(2508, 1, 'c51142fa0528bdf36debecd56fdb0826', 'enabled'),
(2509, 1, 'c5ef9c0ddb343325e026684fddcfbe88', 'hidden'),
(2510, 1, 'ce09dd43394afe5a90726a971943172c', 'hidden'),
(2511, 1, 'ce5cf58b1516f2386f561e19f6fe07d5', 'hidden'),
(2512, 1, 'd43e20eec4f6c3424d2506242c84f847', 'hidden'),
(2513, 1, 'd484e10174f0e232a936ebb129c4f7dd', 'hidden'),
(2514, 1, 'd54b9ac6e2332a4869d317b3a8103dbf', 'hidden'),
(2515, 1, 'd7c210b8e88c19cbce32fdbe57ccd2f5', 'enabled'),
(2516, 1, 'd7f92c13a5136fc1d350abc48fe8dc4f', 'hidden'),
(2517, 1, 'da1dc284cc7b27555e590d289bcf96ae', 'hidden'),
(2518, 1, 'e1814eb37b22a56fa5d7979b36266e73', 'enabled'),
(2519, 1, 'e26628cfcc6872ab02646928decc1dd2', 'enabled'),
(2520, 1, 'e2c889de811134b4ae155416a81df9db', 'hidden'),
(2521, 1, 'e56260185280be666234019ec8bc59e6', 'hidden'),
(2522, 1, 'e669f07a6a5d6ee9640b41aa8bdd3f7d', 'hidden'),
(2523, 1, 'ed38d3c46063a8dc4ad2083dad2f0b1b', 'hidden'),
(2524, 1, 'f1c1f7479d7282a888e83fe488b9d07f', 'hidden'),
(2525, 1, 'fa854a9d7c61184b7423ac0051c0c8cc', 'hidden'),
(2526, 1, 'fe3337b52404cd26026a814c614ec040', 'enabled'),
(2693, 6, '0581cafacd398932e5b4f6418683f5b3', 'hidden'),
(2694, 6, '08e379ea658f873de6134443f4013741', 'enabled'),
(2695, 6, '118a46e71ee56379179b54d5d2e2993f', 'hidden'),
(2696, 6, '12e92aa992a11cd5475c0a8c3d1879ec', 'enabled'),
(2697, 6, '140671fa52a6a5be0633c0a148f3300d', 'enabled'),
(2698, 6, '145e5be43b3eaba5bd1c92d3a60dec71', 'enabled'),
(2699, 6, '16a75b8afc03203238a1ed9db33ef5cf', 'hidden'),
(2700, 6, '19694aead869fc540ebbe73032e6fa18', 'enabled'),
(2701, 6, '1aded72a1b5106f475471351d9b10125', 'enabled'),
(2702, 6, '1b534692b60f8e3c7ff60333a1bbaa0e', 'hidden'),
(2703, 6, '2704e9a4422c8e40b29a9648adc82b99', 'hidden'),
(2704, 6, '28962ad7513a157825db87260e7c5afd', 'enabled'),
(2705, 6, '296387918789a39edf1812c633806079', 'hidden'),
(2706, 6, '29a1654dbe178eea563f585f2612f382', 'hidden'),
(2707, 6, '29fce0d108a31c8bc776c277ee541b5c', 'hidden'),
(2708, 6, '2a5a82ea79c0c311d3d29bbbe5b49f2f', 'hidden'),
(2709, 6, '2b4f26b42daabb492974dda18c983285', 'hidden'),
(2710, 6, '3066c0eaf127d8caf42b981331608cd0', 'enabled'),
(2711, 6, '35d46795b2c0311bf116d3abd0d1e4e7', 'enabled'),
(2712, 6, '3639b8a295c35f9e5258bb6490b8a534', 'hidden'),
(2713, 6, '386e5addb689225ff89a430fa2ba2b71', 'hidden'),
(2714, 6, '3ac86657a58fcacb13db2dd8de82f5e7', 'hidden'),
(2715, 6, '3b25586f69eb1fca8690d6d446c15d31', 'enabled'),
(2716, 6, '3f02808656c1519e8713dca3ec231f68', 'hidden'),
(2717, 6, '43b9de56d5aa207e3b10a51405187368', 'hidden'),
(2718, 6, '49e2dc76be27396b284ba9ccf724511e', 'hidden'),
(2719, 6, '4f663ae28447e8b7bfc7a97e47b6f1e8', 'hidden'),
(2720, 6, '51e67d44ceb6dde6b50dbfb3f8e197ac', 'hidden'),
(2721, 6, '579be0d578358e0754d37e04e4b57259', 'enabled'),
(2722, 6, '5ad859905a64fc08843cb15b3c70c632', 'hidden'),
(2723, 6, '5d8a3eb94bcca4ce3ef1d94d3a43b065', 'enabled'),
(2724, 6, '6037296b70ff1af3cb8a5651aa326c6b', 'hidden'),
(2725, 6, '63faf55a1b49511e7278d0ff6c5c8160', 'hidden'),
(2726, 6, '7285074fb14b56763cf3e7c1a7f781c9', 'hidden'),
(2727, 6, '737371d1c0df6acc1658d403181a850f', 'enabled'),
(2728, 6, '73fce4cb4414106912d0e1ea16baa2c4', 'enabled'),
(2729, 6, '761fd2f727bd2b6c68a737ddbea46c10', 'enabled'),
(2730, 6, '786eb34f5833eee727bb7549aa72afaa', 'hidden'),
(2731, 6, '7b8e2e92cd82019f40d65a827697537d', 'enabled'),
(2732, 6, '817727543c01d2023267c6e84a10da74', 'hidden'),
(2733, 6, '895ca41abc8d6efe17634e5cc29dfcb1', 'hidden'),
(2734, 6, '8af39a26f04fc9af720a8aecb4ab49ed', 'hidden'),
(2735, 6, '8ccaa416dd32884e49d035dd2907331d', 'hidden'),
(2736, 6, '8d60091184eac27cb9998884bd2141b0', 'hidden'),
(2737, 6, '8dba03f38706424efd6406b48cc7eb21', 'hidden'),
(2738, 6, '8dfe18dad8e8a98917471c96f7cd43cb', 'hidden'),
(2739, 6, '8e56844e531d32d4cc728d59ab8b5fe6', 'enabled'),
(2740, 6, '8eb6cdfee1c3638250feeba5c935c5dc', 'hidden'),
(2741, 6, '9297be1d44d7c94142b5e8e8c5a63c02', 'hidden'),
(2742, 6, '96c965dfe4dec71cd062e7ca7b0e3df5', 'enabled'),
(2743, 6, '98bab1ebdb20ed6ea007862c14614d1a', 'enabled'),
(2744, 6, 'a2bfe5641689a53b1ab5bfed922f0b16', 'hidden'),
(2745, 6, 'a66f8a3bb371ee25df3ea3eb70c3e9ec', 'enabled'),
(2746, 6, 'abc3c9509c714b56f571f9784a8d5bdf', 'enabled'),
(2747, 6, 'ac0d212f9ca85f5723999094bc128827', 'hidden'),
(2748, 6, 'ad7a91d112ca4d888371870edc48baaa', 'enabled'),
(2749, 6, 'b1d58ad4da479e09f97d003e53fe6133', 'hidden'),
(2750, 6, 'b29fc5f00b60d176eb22a26e2b908bac', 'enabled'),
(2751, 6, 'ba0415f92ed6e1a7b80be7082c3a2f2c', 'hidden'),
(2752, 6, 'bb118ff1a9ad48d596073fb8a9699501', 'enabled'),
(2753, 6, 'bd8d5c06ffa739fc2546314898a4edcb', 'enabled'),
(2754, 6, 'c3f0c17f884a03ae85573cd8c8a0511a', 'enabled'),
(2755, 6, 'c51142fa0528bdf36debecd56fdb0826', 'hidden'),
(2756, 6, 'c5ef9c0ddb343325e026684fddcfbe88', 'enabled'),
(2757, 6, 'ce09dd43394afe5a90726a971943172c', 'hidden'),
(2758, 6, 'ce5cf58b1516f2386f561e19f6fe07d5', 'hidden'),
(2759, 6, 'cef83aadfe577381c082e7d9047f5352', 'hidden'),
(2760, 6, 'd43e20eec4f6c3424d2506242c84f847', 'enabled'),
(2761, 6, 'd484e10174f0e232a936ebb129c4f7dd', 'hidden'),
(2762, 6, 'd54b9ac6e2332a4869d317b3a8103dbf', 'hidden'),
(2763, 6, 'd7c210b8e88c19cbce32fdbe57ccd2f5', 'hidden'),
(2764, 6, 'd7f92c13a5136fc1d350abc48fe8dc4f', 'hidden'),
(2765, 6, 'da1dc284cc7b27555e590d289bcf96ae', 'hidden'),
(2766, 6, 'e1814eb37b22a56fa5d7979b36266e73', 'hidden'),
(2767, 6, 'e26628cfcc6872ab02646928decc1dd2', 'hidden'),
(2768, 6, 'e2c889de811134b4ae155416a81df9db', 'hidden'),
(2769, 6, 'e56260185280be666234019ec8bc59e6', 'hidden'),
(2770, 6, 'e669f07a6a5d6ee9640b41aa8bdd3f7d', 'enabled'),
(2771, 6, 'ed38d3c46063a8dc4ad2083dad2f0b1b', 'hidden'),
(2772, 6, 'f1c1f7479d7282a888e83fe488b9d07f', 'hidden'),
(2773, 6, 'fa854a9d7c61184b7423ac0051c0c8cc', 'hidden'),
(2774, 6, 'fe3337b52404cd26026a814c614ec040', 'hidden'),
(2775, 6, 'fe616b4c554f0e2e63c397b6098e7946', 'hidden'),
(2859, 16, '0581cafacd398932e5b4f6418683f5b3', 'hidden'),
(2860, 16, '08e379ea658f873de6134443f4013741', 'hidden'),
(2861, 16, '118a46e71ee56379179b54d5d2e2993f', 'hidden'),
(2862, 16, '12e92aa992a11cd5475c0a8c3d1879ec', 'hidden'),
(2863, 16, '140671fa52a6a5be0633c0a148f3300d', 'hidden'),
(2864, 16, '145e5be43b3eaba5bd1c92d3a60dec71', 'hidden'),
(2865, 16, '16a75b8afc03203238a1ed9db33ef5cf', 'enabled'),
(2866, 16, '19694aead869fc540ebbe73032e6fa18', 'hidden'),
(2867, 16, '1aded72a1b5106f475471351d9b10125', 'hidden'),
(2868, 16, '1b534692b60f8e3c7ff60333a1bbaa0e', 'hidden'),
(2869, 16, '2704e9a4422c8e40b29a9648adc82b99', 'hidden'),
(2870, 16, '28962ad7513a157825db87260e7c5afd', 'hidden'),
(2871, 16, '296387918789a39edf1812c633806079', 'hidden'),
(2872, 16, '29a1654dbe178eea563f585f2612f382', 'hidden'),
(2873, 16, '29fce0d108a31c8bc776c277ee541b5c', 'hidden'),
(2874, 16, '2a5a82ea79c0c311d3d29bbbe5b49f2f', 'hidden'),
(2875, 16, '2b4f26b42daabb492974dda18c983285', 'hidden'),
(2876, 16, '3066c0eaf127d8caf42b981331608cd0', 'hidden'),
(2877, 16, '35d46795b2c0311bf116d3abd0d1e4e7', 'hidden'),
(2878, 16, '3639b8a295c35f9e5258bb6490b8a534', 'hidden'),
(2879, 16, '386e5addb689225ff89a430fa2ba2b71', 'enabled'),
(2880, 16, '3ac86657a58fcacb13db2dd8de82f5e7', 'hidden'),
(2881, 16, '3b25586f69eb1fca8690d6d446c15d31', 'hidden'),
(2882, 16, '3f02808656c1519e8713dca3ec231f68', 'hidden'),
(2883, 16, '43b9de56d5aa207e3b10a51405187368', 'hidden'),
(2884, 16, '49e2dc76be27396b284ba9ccf724511e', 'hidden'),
(2885, 16, '4f663ae28447e8b7bfc7a97e47b6f1e8', 'hidden'),
(2886, 16, '51e67d44ceb6dde6b50dbfb3f8e197ac', 'hidden'),
(2887, 16, '579be0d578358e0754d37e04e4b57259', 'hidden'),
(2888, 16, '5ad859905a64fc08843cb15b3c70c632', 'enabled'),
(2889, 16, '5d8a3eb94bcca4ce3ef1d94d3a43b065', 'hidden'),
(2890, 16, '6037296b70ff1af3cb8a5651aa326c6b', 'hidden'),
(2891, 16, '63faf55a1b49511e7278d0ff6c5c8160', 'enabled'),
(2892, 16, '7285074fb14b56763cf3e7c1a7f781c9', 'hidden'),
(2893, 16, '737371d1c0df6acc1658d403181a850f', 'hidden'),
(2894, 16, '73fce4cb4414106912d0e1ea16baa2c4', 'hidden'),
(2895, 16, '761fd2f727bd2b6c68a737ddbea46c10', 'hidden'),
(2896, 16, '786eb34f5833eee727bb7549aa72afaa', 'hidden'),
(2897, 16, '7b8e2e92cd82019f40d65a827697537d', 'hidden'),
(2898, 16, '817727543c01d2023267c6e84a10da74', 'hidden'),
(2899, 16, '895ca41abc8d6efe17634e5cc29dfcb1', 'enabled'),
(2900, 16, '8af39a26f04fc9af720a8aecb4ab49ed', 'hidden'),
(2901, 16, '8ccaa416dd32884e49d035dd2907331d', 'hidden'),
(2902, 16, '8d60091184eac27cb9998884bd2141b0', 'hidden'),
(2903, 16, '8dba03f38706424efd6406b48cc7eb21', 'hidden'),
(2904, 16, '8dfe18dad8e8a98917471c96f7cd43cb', 'hidden'),
(2905, 16, '8e56844e531d32d4cc728d59ab8b5fe6', 'hidden'),
(2906, 16, '8eb6cdfee1c3638250feeba5c935c5dc', 'hidden'),
(2907, 16, '9297be1d44d7c94142b5e8e8c5a63c02', 'hidden'),
(2908, 16, '96c965dfe4dec71cd062e7ca7b0e3df5', 'hidden'),
(2909, 16, '98bab1ebdb20ed6ea007862c14614d1a', 'hidden'),
(2910, 16, 'a2bfe5641689a53b1ab5bfed922f0b16', 'enabled'),
(2911, 16, 'a66f8a3bb371ee25df3ea3eb70c3e9ec', 'hidden'),
(2912, 16, 'abc3c9509c714b56f571f9784a8d5bdf', 'hidden'),
(2913, 16, 'ac0d212f9ca85f5723999094bc128827', 'hidden'),
(2914, 16, 'ad7a91d112ca4d888371870edc48baaa', 'hidden'),
(2915, 16, 'b1d58ad4da479e09f97d003e53fe6133', 'hidden'),
(2916, 16, 'b29fc5f00b60d176eb22a26e2b908bac', 'hidden'),
(2917, 16, 'ba0415f92ed6e1a7b80be7082c3a2f2c', 'hidden'),
(2918, 16, 'bb118ff1a9ad48d596073fb8a9699501', 'hidden'),
(2919, 16, 'bd8d5c06ffa739fc2546314898a4edcb', 'hidden'),
(2920, 16, 'c3f0c17f884a03ae85573cd8c8a0511a', 'hidden'),
(2921, 16, 'c51142fa0528bdf36debecd56fdb0826', 'hidden'),
(2922, 16, 'c5ef9c0ddb343325e026684fddcfbe88', 'hidden'),
(2923, 16, 'ce09dd43394afe5a90726a971943172c', 'enabled'),
(2924, 16, 'ce5cf58b1516f2386f561e19f6fe07d5', 'hidden'),
(2925, 16, 'cef83aadfe577381c082e7d9047f5352', 'enabled'),
(2926, 16, 'd43e20eec4f6c3424d2506242c84f847', 'hidden'),
(2927, 16, 'd484e10174f0e232a936ebb129c4f7dd', 'hidden'),
(2928, 16, 'd54b9ac6e2332a4869d317b3a8103dbf', 'hidden'),
(2929, 16, 'd7c210b8e88c19cbce32fdbe57ccd2f5', 'hidden'),
(2930, 16, 'd7f92c13a5136fc1d350abc48fe8dc4f', 'hidden'),
(2931, 16, 'da1dc284cc7b27555e590d289bcf96ae', 'enabled'),
(2932, 16, 'e1814eb37b22a56fa5d7979b36266e73', 'hidden'),
(2933, 16, 'e26628cfcc6872ab02646928decc1dd2', 'hidden'),
(2934, 16, 'e2c889de811134b4ae155416a81df9db', 'hidden'),
(2935, 16, 'e56260185280be666234019ec8bc59e6', 'hidden'),
(2936, 16, 'e669f07a6a5d6ee9640b41aa8bdd3f7d', 'hidden'),
(2937, 16, 'ed38d3c46063a8dc4ad2083dad2f0b1b', 'hidden'),
(2938, 16, 'f1c1f7479d7282a888e83fe488b9d07f', 'hidden'),
(2939, 16, 'fa854a9d7c61184b7423ac0051c0c8cc', 'hidden'),
(2940, 16, 'fe3337b52404cd26026a814c614ec040', 'hidden'),
(2941, 16, 'fe616b4c554f0e2e63c397b6098e7946', 'enabled');

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_job`
--

DROP TABLE IF EXISTS `_cred_job`;
CREATE TABLE IF NOT EXISTS `_cred_job` (
  `hj_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hj_job` char(8) NOT NULL DEFAULT '',
  `hj_role_id` int(13) UNSIGNED NOT NULL DEFAULT '0',
  `hj_start_date` datetime DEFAULT NULL,
  `hj_end_date` datetime DEFAULT NULL,
  `hj_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`hj_id`),
  KEY `hj_job` (`hj_job`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_office`
--

DROP TABLE IF EXISTS `_cred_office`;
CREATE TABLE IF NOT EXISTS `_cred_office` (
  `ho_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ho_office` char(8) NOT NULL DEFAULT '',
  `ho_role_id` int(13) UNSIGNED NOT NULL DEFAULT '0',
  `ho_start_date` datetime DEFAULT NULL,
  `ho_end_date` datetime DEFAULT NULL,
  `ho_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`ho_id`),
  KEY `ho_office` (`ho_office`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `_cred_office`
--

INSERT INTO `_cred_office` (`ho_id`, `ho_office`, `ho_role_id`, `ho_start_date`, `ho_end_date`, `ho_active`) VALUES
(1, '50203164', 1, '2019-02-08 14:13:34', '2099-02-08 14:13:35', 1),
(6, '50276497', 1, '2019-02-21 16:37:25', '2099-02-21 16:37:27', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_resource`
--

DROP TABLE IF EXISTS `_cred_resource`;
CREATE TABLE IF NOT EXISTS `_cred_resource` (
  `resource_id` varchar(32) NOT NULL,
  `resource` varchar(100) DEFAULT '',
  `type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dump dei dati per la tabella `_cred_resource`
--

INSERT INTO `_cred_resource` (`resource_id`, `resource`, `type`) VALUES
('0581cafacd398932e5b4f6418683f5b3', 'servicemanager/sm/ccordinari#ccordinari', 'wire-resource'),
('08e379ea658f873de6134443f4013741', 'plug/functionalities/tree/tree#save', 'plug-resource'),
('118a46e71ee56379179b54d5d2e2993f', 'servicemanager/sm/fase#fase', 'wire-resource'),
('12e92aa992a11cd5475c0a8c3d1879ec', 'plug/resources/resources#ngoninit', 'plug-resource'),
('140671fa52a6a5be0633c0a148f3300d', 'credential/role#listroles', 'wire-resource'),
('145e5be43b3eaba5bd1c92d3a60dec71', 'credential/functionality#editfunctionalities', 'wire-resource'),
('16a75b8afc03203238a1ed9db33ef5cf', 'profiles/profile/appprofile#save', 'plug-resource'),
('19694aead869fc540ebbe73032e6fa18', 'plug/functionalities/functionalities#remove', 'plug-resource'),
('1aded72a1b5106f475471351d9b10125', 'credential/rolefunctionality#insertrolefunctionality', 'wire-resource'),
('1b534692b60f8e3c7ff60333a1bbaa0e', 'servicemanager/sm/anagrafe#anagrafe', 'wire-resource'),
('2704e9a4422c8e40b29a9648adc82b99', 'servicemanager/sm/domiciliazioni#domiciliazioni', 'wire-resource'),
('28962ad7513a157825db87260e7c5afd', 'plug/profiles/show/show#ngoninit', 'plug-resource'),
('296387918789a39edf1812c633806079', 'credential/takerole#takerole', 'wire-resource'),
('29a1654dbe178eea563f585f2612f382', 'servicemanager/sm/settorics#settorics', 'wire-resource'),
('29fce0d108a31c8bc776c277ee541b5c', 'servicemanager/sm/customer#customer', 'wire-resource'),
('2a5a82ea79c0c311d3d29bbbe5b49f2f', 'credential/delegas#deletedelegas', 'wire-resource'),
('2b4f26b42daabb492974dda18c983285', 'credential/hotlinesjobs#listhotlinesjobs', 'wire-resource'),
('3066c0eaf127d8caf42b981331608cd0', 'plug/profiles/profiles#remove', 'plug-resource'),
('35d46795b2c0311bf116d3abd0d1e4e7', 'credential/functionality#deletefunctionalities', 'wire-resource'),
('3639b8a295c35f9e5258bb6490b8a534', 'plug/delega/delega#new', 'plug-resource'),
('386e5addb689225ff89a430fa2ba2b71', 'contenuto1/contenuto1#submit', 'plug-resource'),
('3ac86657a58fcacb13db2dd8de82f5e7', 'servicemanager/sm/cointestati#cointestati', 'wire-resource'),
('3b25586f69eb1fca8690d6d446c15d31', 'credential/role#insertrole', 'wire-resource'),
('3f02808656c1519e8713dca3ec231f68', 'servicemanager/sm/creditoridba#creditoridba', 'wire-resource'),
('43b9de56d5aa207e3b10a51405187368', 'referential/employeesjobs#listemployeesjobs', 'wire-resource'),
('49e2dc76be27396b284ba9ccf724511e', 'plug/delega/delega#ngoninit', 'plug-resource'),
('4f663ae28447e8b7bfc7a97e47b6f1e8', 'plug/hotline/hotline#ngoninit', 'plug-resource'),
('51e67d44ceb6dde6b50dbfb3f8e197ac', 'credential/hotlines#listhotlines', 'wire-resource'),
('579be0d578358e0754d37e04e4b57259', 'plug/functionalities/tree/tree#ngoninit', 'plug-resource'),
('5ad859905a64fc08843cb15b3c70c632', 'contenuto1/contenuto1#ngoninit', 'plug-resource'),
('5d8a3eb94bcca4ce3ef1d94d3a43b065', 'plug/profiles/profiles#ngoninit', 'plug-resource'),
('6037296b70ff1af3cb8a5651aa326c6b', 'plug/delega/delega#remove', 'plug-resource'),
('63faf55a1b49511e7278d0ff6c5c8160', 'contenuto2/contenuto2#take', 'plug-resource'),
('7285074fb14b56763cf3e7c1a7f781c9', 'servicemanager/sm/creditoridbp#creditoridbp', 'wire-resource'),
('737371d1c0df6acc1658d403181a850f', 'plug/profiles/profile/profile#ngoninit', 'plug-resource'),
('73fce4cb4414106912d0e1ea16baa2c4', 'plug/functionalities/show/show#ngoninit', 'plug-resource'),
('761fd2f727bd2b6c68a737ddbea46c10', 'credential/functionalityresource#listfunctionalityresource', 'wire-resource'),
('786eb34f5833eee727bb7549aa72afaa', 'plug/hotline/hotline#take', 'plug-resource'),
('7b8e2e92cd82019f40d65a827697537d', 'plug/profiles/profile/profile#save', 'plug-resource'),
('817727543c01d2023267c6e84a10da74', 'plug/role/role#ngoninit', 'plug-resource'),
('895ca41abc8d6efe17634e5cc29dfcb1', 'profiles/appprofiles#remove', 'plug-resource'),
('8af39a26f04fc9af720a8aecb4ab49ed', 'servicemanager/sm/cope#cope', 'wire-resource'),
('8ccaa416dd32884e49d035dd2907331d', 'servicemanager/sm/portafoglio#portafoglio', 'wire-resource'),
('8d60091184eac27cb9998884bd2141b0', 'plug/omarello/omarello#ngoninit', 'plug-resource'),
('8dba03f38706424efd6406b48cc7eb21', 'credential/hotlines#takehotlines', 'wire-resource'),
('8dfe18dad8e8a98917471c96f7cd43cb', 'plug/delega/delega#save', 'plug-resource'),
('8e56844e531d32d4cc728d59ab8b5fe6', 'credential/role#editrole', 'wire-resource'),
('8eb6cdfee1c3638250feeba5c935c5dc', 'credential/delegas#listdelegas', 'wire-resource'),
('9297be1d44d7c94142b5e8e8c5a63c02', 'credential/hotlines#leavehotlines', 'wire-resource'),
('96c965dfe4dec71cd062e7ca7b0e3df5', 'credential/role#deleterole', 'wire-resource'),
('98bab1ebdb20ed6ea007862c14614d1a', 'plug/resources/resources#load', 'plug-resource'),
('a2bfe5641689a53b1ab5bfed922f0b16', 'profiles/show/appprofileshow#ngoninit', 'plug-resource'),
('a66f8a3bb371ee25df3ea3eb70c3e9ec', 'credential/functionalityresource#deletefunctionalityresource', 'wire-resource'),
('abc3c9509c714b56f571f9784a8d5bdf', 'plug/functionalities/functionalities#ngoninit', 'plug-resource'),
('ac0d212f9ca85f5723999094bc128827', 'credential/delegas#insertdelegas', 'wire-resource'),
('ad7a91d112ca4d888371870edc48baaa', 'credential/resource#resources', 'wire-resource'),
('b1d58ad4da479e09f97d003e53fe6133', 'referential/jobs#listjobs', 'wire-resource'),
('b29fc5f00b60d176eb22a26e2b908bac', 'credential/functionality#listfunctionalities', 'wire-resource'),
('ba0415f92ed6e1a7b80be7082c3a2f2c', 'authorization/acl#acl', 'wire-resource'),
('bb118ff1a9ad48d596073fb8a9699501', 'credential/role#updaterole', 'wire-resource'),
('bd8d5c06ffa739fc2546314898a4edcb', 'credential/rolefunctionality#listrolefunctionalities', 'wire-resource'),
('c3f0c17f884a03ae85573cd8c8a0511a', 'credential/functionality#insertfunctionalities', 'wire-resource'),
('c51142fa0528bdf36debecd56fdb0826', 'referential/employees#listemployees', 'wire-resource'),
('c5ef9c0ddb343325e026684fddcfbe88', 'credential/rolefunctionality#deleterolefunctionality', 'wire-resource'),
('ce09dd43394afe5a90726a971943172c', 'homepage/homepage#ngoninit', 'plug-resource'),
('ce5cf58b1516f2386f561e19f6fe07d5', 'plug/hotline/hotline#leave', 'plug-resource'),
('cef83aadfe577381c082e7d9047f5352', 'profiles/profile/appprofile#ngoninit', 'plug-resource'),
('d43e20eec4f6c3424d2506242c84f847', 'credential/functionalityresource#insertfunctionalityresource', 'wire-resource'),
('d484e10174f0e232a936ebb129c4f7dd', 'servicemanager/sm/mercato#mercato', 'wire-resource'),
('d54b9ac6e2332a4869d317b3a8103dbf', 'servicemanager/sm/contabile#contabile', 'wire-resource'),
('d7c210b8e88c19cbce32fdbe57ccd2f5', 'credential/userdetails#userdetails', 'wire-resource'),
('d7f92c13a5136fc1d350abc48fe8dc4f', 'servicemanager/sm/conto2creditori#conto2creditori', 'wire-resource'),
('da1dc284cc7b27555e590d289bcf96ae', 'contenuto2/contenuto2#ngoninit', 'plug-resource'),
('e1814eb37b22a56fa5d7979b36266e73', 'referential/respuo#listrespuo', 'wire-resource'),
('e26628cfcc6872ab02646928decc1dd2', 'referential/employeesoffices#listemployeesoffices', 'wire-resource'),
('e2c889de811134b4ae155416a81df9db', 'servicemanager/sm/circolarita#circolarita', 'wire-resource'),
('e56260185280be666234019ec8bc59e6', 'servicemanager/sm/customerinfo#customerinfo', 'wire-resource'),
('e669f07a6a5d6ee9640b41aa8bdd3f7d', 'credential/resource#listresources', 'wire-resource'),
('ed38d3c46063a8dc4ad2083dad2f0b1b', 'credential/hotlinesoffices#listhotlinesoffices', 'wire-resource'),
('f1c1f7479d7282a888e83fe488b9d07f', 'servicemanager/sm/rating#rating', 'wire-resource'),
('fa854a9d7c61184b7423ac0051c0c8cc', 'servicemanager/sm/sia2creditori#sia2creditori', 'wire-resource'),
('fe3337b52404cd26026a814c614ec040', 'referential/offices#listoffices', 'wire-resource'),
('fe616b4c554f0e2e63c397b6098e7946', 'profiles/appprofiles#ngoninit', 'plug-resource');

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_role`
--

DROP TABLE IF EXISTS `_cred_role`;
CREATE TABLE IF NOT EXISTS `_cred_role` (
  `role_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_level` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `role_administrable` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `role_label` varchar(128) NOT NULL,
  `role_description` text NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `_cred_role`
--

INSERT INTO `_cred_role` (`role_id`, `role_level`, `role_administrable`, `role_label`, `role_description`) VALUES
(1, 0, 0, 'Amministratore', 'Amministratore dell\'applicazione'),
(6, 1, 1, 'Utente', 'Utente dell\'applicazione'),
(11, 7, 1, 'Ospite', 'Utente Ospite');

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_role_functionality`
--

DROP TABLE IF EXISTS `_cred_role_functionality`;
CREATE TABLE IF NOT EXISTS `_cred_role_functionality` (
  `role_functionality_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` int(13) UNSIGNED NOT NULL DEFAULT '0',
  `functionality_id` int(13) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`role_functionality_id`),
  UNIQUE KEY `UNIQUE` (`role_id`,`functionality_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `_cred_role_functionality`
--

INSERT INTO `_cred_role_functionality` (`role_functionality_id`, `role_id`, `functionality_id`) VALUES
(1, 1, 1),
(6, 1, 6),
(11, 1, 11),
(16, 1, 16),
(34, 6, 1),
(36, 6, 11),
(35, 6, 16),
(37, 11, 1),
(38, 11, 11);

-- --------------------------------------------------------

--
-- Struttura della tabella `_cred_user`
--

DROP TABLE IF EXISTS `_cred_user`;
CREATE TABLE IF NOT EXISTS `_cred_user` (
  `hu_id` int(13) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hu_uid` varchar(20) NOT NULL DEFAULT '',
  `hu_role_id` int(13) UNSIGNED NOT NULL DEFAULT '0',
  `hu_start_date` datetime DEFAULT NULL,
  `hu_end_date` datetime DEFAULT NULL,
  `hu_active` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  PRIMARY KEY (`hu_id`),
  KEY `hu_uid` (`hu_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `_cred_user`
--

INSERT INTO `_cred_user` (`hu_id`, `hu_uid`, `hu_role_id`, `hu_start_date`, `hu_end_date`, `hu_active`) VALUES
(1, '720188', 6, NULL, NULL, 1);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
