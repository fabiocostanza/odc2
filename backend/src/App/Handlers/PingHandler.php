<?php

declare(strict_types = 1);

namespace App\Handlers;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;

/**
 * @Handler(
 *  path = "ping",
 *  methods = {"GET"},
 *  noauth="TRUE"
 * )
 * Undocumented class
 */
class PingHandler implements RequestHandlerInterface 
{
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return new JsonResponse(['app' => time()]);
    }
}
