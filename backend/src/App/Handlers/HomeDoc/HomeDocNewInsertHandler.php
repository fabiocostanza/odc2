<?php

declare(strict_types=1);

namespace App\Handlers\HomeDoc;

use App\Utility\GestioneDocInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Zend\Db\Adapter\Adapter;

//old path="insertdocinforiunioni",

/**
 * @Handler(
 *  path="homedocnew",
 *  methods = {"PATCH"},
 * )
 * @author d41618
 *
 */
class HomeDocNewInsertHandler implements RequestHandlerInterface
{
    private $_adapter;

    public function __construct(Adapter $adapter)
    {
        $this->_adapter = $adapter;
    }


    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $pquery = $request->getParsedBody();
       //var_dump($pquery);
       //die();
       
        $_mod = new GestioneDocInfo($this->_adapter);
        if(!$pquery['id_doc_info'])
        {
          $id_riunione = $_mod->insertNewDocInRiunione($pquery);
         
        }else{
            $id_riunione = $_mod->updateDocInRiunione($pquery);
        }
      
       return new JsonResponse(['count' => 0, 'data' => ['id_riunione' => $id_riunione ]], 200);
    }
}
