<?php

declare(strict_types = 1);

namespace App\Handlers\HomeDoc;



use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use Wire\Annotation\Elements\Handler;
use Wire\Data\Builder\Sql\Select;
use Wire\Data\Handler\AbstractHandler;

use Wire\Db\DbFactory;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Diactoros\Response\JsonResponse;

/**
 * @Handler(
 *  path = "homedocprevisti",
 *  methods = {"GET"},
 * )
 */


 class HomeDocPrevistiHandler extends AbstractHandler implements RequestHandlerInterface 
{
    protected $table = ['di'=>'_vdoctot'];
  
    protected $filters = [
      'organo' => 'di.id_organo = :organo',
      'mese' => 'di.mese = :mese',
      'anno' => '(anno = :anno or anno is null)',
  ];

    public function select(ServerRequestInterface $request): Select
    {


    return parent::select($request)
      ->columns(['n_verbale', 'mese','cod_tipo_doc','anno','id_riunione'
      ,'data_riunione','id_doc_info', 'id_documento', 'titolo', 'ricevuto'=>  'flag_ricevuto' , 
      'peso', 'codici_funzioni'=> new Expression("GROUP_CONCAT(DISTINCT di.sigla ORDER BY di.sigla ASC SEPARATOR ',')")
      , 'sessioni'=> new Expression("GROUP_CONCAT(DISTINCT di.sessione ORDER BY di.sessione ASC SEPARATOR ',')") ])
      ->join(["f" => "funzioni"], "f.sigla = di.sigla", ["funzioni" => new Expression("GROUP_CONCAT(DISTINCT descrizione ORDER BY descrizione ASC SEPARATOR ',')")],Select::JOIN_LEFT)
      ->join(["o"=>"organi"],"o.id_organo = di.id_organo",["id_organo"=>"id_organo", "nome_organo"=>"descrizione_organo"],Select::JOIN_LEFT)
      ->group('id_doc_info')
      ->order('id_riunione','mese', 'peso')
      ;
    
    }


    public function handle(ServerRequestInterface $request): ResponseInterface
    {
   // echo( $this->select($request)->getSqlString());
   // exit;
    return $this->handleRequest($request);
  
    }
}