<?php

declare(strict_types=1);

namespace App\Handlers\HomeDoc;

use App\Utility\AbstractBd;
use App\Utility\GestioneDocInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Wire\Annotation\Elements\Handler;
use Zend\Db\Adapter\Adapter;
use Zend\Diactoros\Response\JsonResponse;

// old path="editdocriunione",
/**
 * @Handler(
 *  path="homedocedit",
 *  methods = {"GET"},
 * )
 * @author d41618
 *
 */
class HomeDocEditGetHandler implements RequestHandlerInterface
{
    private $_adapter;

    public function __construct(Adapter $adapter)
    {
        $this->_adapter = $adapter;
    }


    /** s
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $pquery = $request->getQueryParams();

        $_mod = new GestioneDocInfo($this->_adapter);


        $data = $_mod->getdoc($pquery);

        $array_assoc = ['funzioni', 'destinatari', 'presentazione'];
        foreach ($array_assoc as $v) {
          

            $ris =[];
            if($data[$v])
                {
                 $data_arr = explode(",", $data[$v]) ; 
                $ris = is_array($data_arr)?$data_arr:[$data_arr];
                }
             $data[$v] = $ris;

        }
        return new JsonResponse(['count' => count($data), 'data' => $data], 200);
    }
}
