<?php

declare(strict_types=1);

namespace App\Handlers\HomeDoc;


use App\Utility\GestioneDocInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Zend\Db\Adapter\Adapter;

//old path="insertdocinforiunioni",
/**
 * @Handler(
 *  path="homedocnew",
 *  methods = {"GET"},
 * )
 * @author d41618
 *
 */
class HomeDocNewGetParamHandler implements RequestHandlerInterface
{
    
    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $pquery = $request->getQueryParams();
        return new JsonResponse(['count' => 0, 'data' => $pquery], 200);
    }
}
