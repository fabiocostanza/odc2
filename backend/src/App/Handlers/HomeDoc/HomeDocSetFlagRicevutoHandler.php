<?php

declare(strict_types=1);

namespace App\Handlers\HomeDoc;


use App\Utility\GestioneDocInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Zend\Db\Adapter\Adapter;
// old  path="setflagricevuto",

/**
 * @Handler(
 *  path="homedocsetflagricevuto",
 *  methods = {"POST"},
 * )
 * @author d41618
 *
 */
class HomeDocSetFlagRicevutoHandler implements RequestHandlerInterface
{
    private $_adapter;

    public function __construct(Adapter $adapter)
    {
        $this->_adapter = $adapter;
    }


    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // $params = $request->getParsedBody();
        $pquery = $request->getQueryParams();
     
        
        $_mod = new GestioneDocInfo($this->_adapter);
        
        $pquery['id_riunione'] =  $_mod->creaCopia($pquery);
        
        $_mod->updateFlagRicevuto($pquery);


         return new JsonResponse(['count' => 0, 'data' => ['id_riunione' =>$pquery['id_riunione']]], 200);
    }
}
