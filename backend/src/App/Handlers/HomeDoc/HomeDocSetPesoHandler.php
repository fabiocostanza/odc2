<?php

declare(strict_types=1);

namespace App\Handlers\HomeDoc;

use App\Utility\GestioneDocInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Zend\Db\Adapter\Adapter;
// old path path="setpeso",
/**
 * @Handler(
 *  path="homedocsetpeso",
 *  methods = {"POST"},
 * )
 * @author d41618
 *
 */
class HomeDocSetPesoHandler implements RequestHandlerInterface
{
    private $_adapter;

    public function __construct(Adapter $adapter)
    {
        $this->_adapter = $adapter;
    }


    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
       // $params = $request->getParsedBody();
        $pquery = $request->getQueryParams();
        $arr_id_doc_info =explode(",", $pquery['arr_id_doc_info']);
        $i=0;
        foreach($arr_id_doc_info as $id_doc_info)
        {
        $pquery['peso'] = $i++;
        $_mod = new GestioneDocInfo($this->_adapter);
        $pquery['id_doc_info'] = $id_doc_info;
        $id_riunione =  $_mod->creaCopia($pquery);         
        $_mod->updatePesoDoc($pquery);      
        }

         return new JsonResponse(['count' => 0, 'data' => [$id_riunione]], 200);
    }
}
