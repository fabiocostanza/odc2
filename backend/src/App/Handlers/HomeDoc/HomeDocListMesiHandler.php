<?php

declare(strict_types=1);

namespace App\Handlers\HomeDoc;

use App\Handlers\Anagrafica\Mesi\AnagraficaMesiListHandler;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\BaseAbstractHandler;

/**
 * @Handler(
 *  path = "homedoclistmesi",
 *  methods = {"GET"},
 * )
 */


class HomeDocListMesiHandler extends BaseAbstractHandler implements RequestHandlerInterface
{

  protected $filters = [
    'organo' => 'di.id_organo = :organo',
    'anno' => '(anno = :anno or anno is null)',
  ];    



  protected function getStages(ServerRequestInterface $request = null): array
  {
    return [
      AnagraficaMesiListHandler::class,
      [HomeDocPrevistiHandler::class, 'withMulti' => 'mesi', 'withAttribute' => 'docprevisti']     
    ];
  }


  public function handle(ServerRequestInterface $request): ResponseInterface
  {
    $handle = $this->processPipeline($request);

    $docMese = $handle->getAttribute('docprevisti');
    //  $docriunioni = $handle->getAttribute('docriunioni');
    //$docMese = array_merge($docPrevisti,$docriunioni);

    $month = array("Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"); 

    $ret = [];
    $c = 0;
    foreach ($docMese as $v) {
      $anno = date("Y");
      $i = (int) ($v['mese'] - 1);
      $ret[$i]['id_riunione'] = $v['id_riunione'];
      $ret[$i]['mese'] = $v['mese'];
      $ret[$i]['nomemese'] = $month[(int)$v['mese']-1]. ' '.$anno;
      $ret[$i]['years'] = date("Y");

      $ret[$i]['nome_organo'] = $v['nome_organo'];
      $ret[$i]['id_organo'] = $v['id_organo'];
      $ret[$i]['data_riunione'] = $v['data_riunione'] ? $v['data_riunione'] : NULL;
      $ret[$i]['n_verbale'] = $v['n_verbale'];
      $ret[$i]['cod_tipo_doc'] = $v['cod_tipo_doc'];
      $ret[$i]['anno'] = $anno;
      $c++;
      if($v['id_doc_info']){
        $ret[$i]['documenti'][] = [
          'codice_doc' => (string)$anno.$v['mese'].$v['cod_tipo_doc'].$v['id_doc_info'],
          'id_doc_info' => $v['id_doc_info'],
          'titolo' => strip_tags($v['titolo']),
          // 'titolo' => $v['titolo'],
          'ricevuto' => $v['ricevuto'],
          'funzioni' => $v['funzioni'],
          'sessioni' => $v['sessione'],
          
        ];
      }
      
    }

    return new JsonResponse(['count' => count($ret), 'data' => $ret], 200);

  }

}
