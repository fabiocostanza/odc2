<?php

declare(strict_types = 1);

namespace App\Handlers\Anagrafica\Azioni;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Builder\Sql\Select;
use Wire\Data\Handler\AbstractHandler;

/**
 * @Handler(
 *  path = "hazioni",
 *  methods = {"GET"},
 * )
 */


 class AnagraficaAzioniListHandler extends AbstractHandler implements RequestHandlerInterface 
{
    protected $table=['a'=>'azioni'];

    public function select(ServerRequestInterface $request): Select
    {
        return parent::select($request)
        ->columns([ 'descrizione','id_azione']);
    }
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->handleRequest($request);
    }
}