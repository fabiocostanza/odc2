<?php

declare(strict_types = 1);

namespace App\Handlers\Anagrafica\Attivo;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Data\Builder\Sql\Select;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\AbstractHandler;
use Zend\Db\Sql\Expression;

/**
 * @Handler(
 *  path = "hattivo",
 *  methods = {"GET"},
 * )
 */


 class AnagraficaAttivoListHandler extends AbstractHandler implements RequestHandlerInterface 
{
  
    protected $table = ['a'=>'attivo'];

    protected $filters = [
        'attivo' => 'a.attivo = :attivo',
    ];

    public function select(ServerRequestInterface $request): Select
    {
        return parent::select($request)
            ->columns( [
                "attivo_str" =>  new Expression("IF (attivo=1, 'si', 'no')"),
                "attivo"
            ]);
    }
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->handleRequest($request);
    }
    
}