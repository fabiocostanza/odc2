<?php

declare(strict_types = 1);

namespace App\Handlers\Anagrafica\Destinatari;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\AbstractHandler;

/**
 * @Handler(
 *  path = "hdestinatari",
 *  methods = {"GET"},
 * )
 *  @author d41618
 */


 class AnagraficaDestinatariListHandler extends AbstractHandler implements RequestHandlerInterface 
{
    protected $table=['d'=>'destinatari'];
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->handleRequest($request);
    }
}