<?php

declare(strict_types = 1);

namespace App\Handlers\Anagrafica\Organi;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\AbstractHandler;

/**
 * @Handler(
 *  path = "horgani",
 *  methods = {"GET"},
 * )
 */


 class AnagraficaOrganiListHandler extends AbstractHandler implements RequestHandlerInterface 
{
    protected $table=['o'=>'organi'];
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->handleRequest($request);
    }
}