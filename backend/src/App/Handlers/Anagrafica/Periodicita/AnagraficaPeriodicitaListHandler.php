<?php

declare(strict_types = 1);

namespace App\Handlers\Anagrafica\Periodicita;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\AbstractHandler;
use Wire\Data\Builder\Sql\Select;

/**
 * @Handler(
 *  path = "hperiodicita",
 *  methods = {"GET"},
 * )
 */


 class AnagraficaPeriodicitaListHandler extends AbstractHandler implements RequestHandlerInterface 
{
    protected $table=['p'=>'periodicita'];

    public function select(ServerRequestInterface $request): Select
    {
        return parent::select($request)
        ->columns(['id_periodicita', 'descrizione_periodicita'])
        ->order(('p.ordine ASC'));
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->handleRequest($request);
    }
    
}