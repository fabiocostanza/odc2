<?php

declare(strict_types = 1);

namespace App\Handlers\Anagrafica\Mesi;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\AbstractHandler;

/**
 * @Handler(
 *  path = "hmesi",
 *  methods = {"GET"},
 * )
 */


 class AnagraficaMesiListHandler extends AbstractHandler implements RequestHandlerInterface 
{
  
    protected $table = ['m'=>'mesi'];
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
       


        return $this->handleRequest($request);
    }
    
}