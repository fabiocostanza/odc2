<?php

declare(strict_types = 1);

namespace App\Handlers\Anagrafica\Sessioni;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\AbstractHandler;

/**
 * @Handler(
 *  path = "hsessioni",
 *  methods = {"GET"},
 * )
 */


 class AnagraficaSessioniListHandler extends AbstractHandler implements RequestHandlerInterface 
{
    protected $table=['s'=>'sessioni'];

    protected $filters = [
        'id_organo' => 'id_organo = :id_organo',
    ];
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->handleRequest($request);
    }
}