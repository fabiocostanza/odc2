<?php

declare(strict_types = 1);

namespace App\Handlers\Anagrafica\Funzioni;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\AbstractHandler;

/**
 * @Handler(
 *  path = "hfunzioni",
 *  methods = {"GET"},
 * )
 * @author d41618
 */


 class AnagraficaFunzioniListHandler extends AbstractHandler implements RequestHandlerInterface 
{
    protected $table=['f'=>'funzioni'];
    
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return $this->handleRequest($request);
    }
}