<?php

declare(strict_types=1);

namespace App\Handlers\DocCensiti;


use App\Utility\GestioneDocInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Zend\Db\Adapter\Adapter;



/**
 * @Handler(
 *  path="doccensiti",
 *  methods = {"PATCH"},
 * )
 * @author d41618
 *
 */
class DocCensitiEditUpdateHandler implements RequestHandlerInterface
{
    private $_adapter;

    public function __construct(Adapter $adapter)
    {
        $this->_adapter = $adapter;
    }


    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $pquery = $request->getParsedBody();

        $_mod = new GestioneDocInfo($this->_adapter);
      
        $_mod->updateDocCensito($pquery);

        return new JsonResponse(['count' => 0, 'data' => $pquery], 200);
    }
}