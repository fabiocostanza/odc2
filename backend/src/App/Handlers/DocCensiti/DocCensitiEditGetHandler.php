<?php

declare(strict_types=1);

namespace App\Handlers\DocCensiti;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Handler\AbstractHandler;


/**
 * @Handler(
 *  path="doccensiti",
 *  methods = {"GET"},
 * )
 * @author d41618
 *
 */
class DocCensitiEditGetHandler extends AbstractHandler implements RequestHandlerInterface 
{
    protected $table=['d'=>'_vdoccensitilist'];

      protected $filters=[
         'id_doc_info'=>"id_doc_info =:id_doc_info",          
     ];

    


     public function handle(ServerRequestInterface $request): ResponseInterface
     {
       $handle = $this->handleRequest($request);
        $ret = $handle->getPayload();
        $data = $ret['data'];

        $array_assoc = ['funzioni', 'destinatari', 'presentazione', 'sessioni', 'contributrici'];
        foreach ($array_assoc as $v) {
            $array_tmp = $data[0][$v] ? explode(",", $data[0][$v]) : [];
            $data[0][$v] = $array_tmp;
        }   
        
        $array_assoc = ['organi'];
        foreach ($array_assoc as $v) {
            $array_tmp = $data[0][$v] ? explode(",", $data[0][$v]) : false;
            
            if($array_tmp)
            {
            foreach($array_tmp as $organo)
                {
                list($id_organo, $id_azione) = explode("#", $organo );

                $data[0]['organo'.$id_organo][] = $id_azione;
                }
            }
        }  

        return new JsonResponse(['count' => count($data), 'data' => $data], 200);
        // return $this->handleRequest($request);
    }

}