<?php

declare(strict_types=1);

namespace App\Handlers\DocCensiti;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Builder\Sql\Select;
use Wire\Data\Handler\AbstractHandler;
use Zend\Db\Sql\Expression as SqlExpression;
use Zend\Db\Sql\Predicate\Expression;
use Zend\Diactoros\Response\JsonResponse;

// old path = "exportdocumenti",

/**
 * @Handler(
 *  path = "doccensitiexport",
 *  methods = {"GET"},
 * )
 */


class DocCensitiExportHandler extends AbstractHandler implements RequestHandlerInterface
{
  protected $table = ['d' => '_vdoccensitilist'];

  protected $pipeResult;

  protected $filters = [
    'codiceDocumento' => "d.codiceDocumento LIKE '%?u:codiceDocumento%'",
    'titolo' => "d.titolo LIKE '%?u:titolo%'",
    'descrizione_organo' => "d.descrizione_organo LIKE '%?u:descrizione_organo%'",
    'descrizione_funzioni' => "d.descrizione_funzioni LIKE '%?u:descrizione_funzioni%'",
    'mesi' => "d.mesi LIKE '%?u:mesi%'",
    'descrizione_periodicita' => "d.descrizione_periodicita LIKE '%?u:descrizione_periodicita%'",

  ];

  public function select(ServerRequestInterface $request): Select
  {

  return parent::select($request)
    ->columns(['Codice Documento'=>'codiceDocumento',  
    'Titolo'=> new Expression("strip_tags(titolo)"),
    'Finalità'=> new Expression("strip_tags(finalita)"), 
    
    'Funzioni' => new Expression("REPLACE(descrizione_funzioni, '<br>', ',')"),
    'Funzioni' => new Expression("REPLACE(descrizione_contributrici, '<br>', ',')"),
    'Organi' => new Expression("REPLACE(descrizione_organo, '<br>', ',')"),
    'Norma Esterna' => new Expression("strip_tags(norma_esterna)"),
    'Norma Interna' => new Expression("strip_tags(norma_interna)"), 
    'Note' => new Expression("strip_tags(note)"),
    'Periodicità' => 'descrizione_periodicita',
    'Mesi' => new Expression("REPLACE(mesi, '<br>', ',')"),
    'Note Periodicità' => new Expression("strip_tags( note_presentazione)")])
    ->where (['attivo' => 1]);

    //->group('id_doc_info')
    //->order('mese', 'peso')
    ;
  }

  protected function processPipeline(\Psr\Http\Message\ServerRequestInterface $request)
    {
        return $this->pipeResult = parent::getPipeline($this->$request)->process($request);
    }

  public function handle(ServerRequestInterface $request): ResponseInterface
  {

    $handle =  $this->handleRequest($request);

    var_dump($handle);
  
  exit();

    //$pquery = explode("," );
    //var_dump(json_decode($_COOKIE['queryParams']));
    $cpquery = json_decode($_COOKIE['queryParams'],true);
    
    /*var_dump($cpquery['mesi']); */
    $pquery = $request->getQueryParams();


    
    
   
    $cpquery['/limit'] = $this->handleRequest($request)->getPayload()['count'];
    /*$cpquery['/offset'] = 200;*/
    /*die();*/
    //$this->ignoreLOO = true;
    
    $request = $request->withQueryParams($cpquery);
   //setcookie("queryParams",$pquery);

    //return $this->handleRequest($request);
  }
}
