<?php

declare(strict_types=1);

namespace App\Handlers\DocCensiti;


use App\Utility\GestioneDocInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response\JsonResponse;
use Wire\Annotation\Elements\Handler;
use Zend\Db\Adapter\Adapter;
// old  path="setflagricevuto",

/**
 * @Handler(
 *  path="doccensitisetattivo",
 *  methods = {"POST"},
 * )
 * @author d41618
 *
 */
class DocCensitiSetAttivoHandler implements RequestHandlerInterface
{
    private $_adapter;

    public function __construct(Adapter $adapter)
    {
        $this->_adapter = $adapter;
    }


    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        // $params = $request->getParsedBody();
        $pquery = $request->getQueryParams();
     
        
        $_mod = new GestioneDocInfo($this->_adapter);
        
        $attivo = $_mod->updateAttivo($pquery);

       

         return new JsonResponse(['count' => 0, 'data' => ['attivo' =>$attivo]], 200);
    }
}
