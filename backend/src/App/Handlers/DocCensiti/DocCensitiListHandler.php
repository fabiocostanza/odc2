<?php

declare(strict_types=1);

namespace App\Handlers\DocCensiti;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Wire\Annotation\Elements\Handler;
use Wire\Data\Builder\Sql\Select;
use Wire\Data\Handler\AbstractHandler;
use Zend\Db\Sql\Predicate\Expression;
//old path = "anadocumenti",

/**
 * @Handler(
 *  path = "doccensitilist",
 *  methods = {"GET"},
 * )
 */


class DocCensitiListHandler extends AbstractHandler implements RequestHandlerInterface
{
  protected $table = ['d' => '_vdoccensitilist'];

  public $ignoreLOO = true;

  protected $filters = [
    'codiceDocumento' => "d.codiceDocumento LIKE '%?u:codiceDocumento%'",
    'titolo' => "d.titolo LIKE '%?u:titolo%'",
    'descrizione_organo' => "d.descrizione_organo LIKE '%?u:descrizione_organo%'",
    'descrizione_funzioni' => "d.descrizione_funzioni LIKE '%?u:descrizione_funzioni%'",
    'mesi' => "d.mesi LIKE '%?u:mesi%'",
    //'presentazione' => 'd.presentazione IN(?a:presentazione)',
    'descrizione_periodicita' => "d.descrizione_periodicita LIKE '%?u:descrizione_periodicita%'",
    'attivo' => 'd.attivo = :attivo',

  ];



  public function handle(ServerRequestInterface $request): ResponseInterface
  {
    $pquery = $request->getQueryParams();

  /*  if (count($pquery) > 2) {
      $this->ignoreLOO = true;
      $pquery['/offset'] = 0;
      $pquery['/limit'] = 100000;
    }*/
    //var_dump($pquery);
    if (isset($pquery['attivo'])) {
      $pquery['attivo'] = ($pquery['attivo']) == "si" ? 1 : 0;
    }
    $request = $request->withQueryParams($pquery);
    setcookie("queryParams", json_encode($pquery), time() + 13600, '/');

    return $this->handleRequest($request);
  }
}
