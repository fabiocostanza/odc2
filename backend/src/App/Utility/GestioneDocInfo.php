<?php

declare(strict_types=1);

namespace App\Utility;


class GestioneDocInfo extends AbstractBd
{

    public function insertAssRiunione($pquery, $nomeAss, $nome_id): int
    {

        $tableName = 'assoc_doc_info_' . $nomeAss;
        $indice = 'id_riunione';
        $uid_modifica = $pquery['uid_modifica'];
        $ret = 0;
        if (isset($pquery[$nomeAss])) {
            // $temp = explode(",", $pquery[$nomeAss]);
            $temp = $pquery[$nomeAss];
            $id_riunione = $pquery['id_riunione'] ? $pquery['id_riunione'] : null;
            $id_doc_info = $pquery['id_doc_info'];
            $where = ['id_riunione' => $id_riunione, 'id_doc_info' => $id_doc_info];

            $this->_delete($tableName, $where);
            if (is_array($temp)) {
                foreach ($temp as $id) {
                    $values = ['id_riunione' => $id_riunione, 'id_doc_info' => $id_doc_info, $nome_id => $id, 'uid_modifica' => $uid_modifica];
                    $ret = $this->_insert($tableName, $values, $indice);
                }
            }
        }
        return  $ret;
    }

    public function insertAss($pquery, $nomeAss, $nome_id): int
    {

        $tableName = 'assoc_doc_info_' . $nomeAss;
        $indice = 'id_doc_info';
        $uid_modifica = $pquery['uid_modifica'];
        $ret = 0;
        $id_doc_info = $pquery['id_doc_info'];
        if (isset($pquery[$nomeAss])) {
            // $temp = explode(",", $pquery[$nomeAss]);
            $temp = $pquery[$nomeAss];
            // var_dump($nomeAss);
            $where = ['id_doc_info' => $id_doc_info, 'id_riunione is null'];
            $this->_delete($tableName, $where);

            foreach ($temp as $id) {
                $values = ['id_doc_info' => $id_doc_info, $nome_id => $id, 'uid_modifica' => $uid_modifica];
                // var_dump($values);

                $ret = $this->_insert($tableName, $values, $indice);
            }
            // die(); 

        }
        return  $ret;
    }

    public function insertAssOrgani($pquery): int
    {

        $tableName = 'assoc_doc_info_organi';
        $indice = 'id_doc_info';
        $uid_modifica = $pquery['uid_modifica'];
        $ret = 0;
        $id_doc_info = $pquery['id_doc_info'];

        $where = ['id_doc_info' => $id_doc_info, 'id_riunione is null'];
        $this->_delete($tableName, $where);


        $organi = $this->_selectAll(['o' => 'organi'], ['id_organo'], ['1=1']);

        $i = 0;
        foreach ($organi as $organo) {
            $id_organo = $organo['id_organo'];
            $id_azioni = $pquery['organo' . $id_organo] ? $pquery['organo' . $id_organo] : null;
            if ($id_azioni) {
                foreach ($id_azioni as $id_azione) {
                    $values = ['id_doc_info' => $id_doc_info, 'id_azione' => $id_azione, 'id_organo' => $id_organo, 'uid_modifica' => $uid_modifica];
                    $i++;
                    $ret = $this->_insert($tableName, $values, $indice);
                }
            }
        }


        return  $i;
    }






    public function insertNewDocInRiunione($pquery): int
    {
        $pquery['id_documento'] =  $this->insertdocumento($pquery);

        //die();
        $pquery['id_doc_info'] = $this->insertdocInfo($pquery);
        //   echo $pquery['id_doc_info'];

        $this->insertAssRiunione($pquery, 'funzioni', 'id_funzione');
        $this->insertAssRiunione($pquery, 'destinatari', 'id_destinatario');
        $this->insertAssRiunione($pquery, 'sessioni', 'id_sessione');
        //die();
        return $this->creaCopia($pquery);
    }
    public function disableDocCensito($pquery): int
    {
        $tableName = "doc_info";
        $values = ['attivo' => 0];
        $where = ['id_doc_info' => $pquery['id_doc_info']];
        $this->_update($tableName, $values, $where);
    }
    /**
     * 
     */

    public function updateDocCensito($pquery): int
    {
        $id_doc_info =  $pquery['id_doc_info'];

        $tableName = "doc_info";
        $where = ['id_doc_info' => $id_doc_info];
        $values = ['attivo' => 0, 'flag_copia' => 1];

        $this->_update($tableName, $values, $where);

        //die();
        $pquery['id_doc_info'] = $this->insertdocInfo($pquery);
        //   echo $pquery['id_doc_info'];

       // var_dump($pquery);die();
        $this->insertAss($pquery, 'funzioni', 'id_funzione');
        $this->insertAss($pquery, 'contributrici', 'id_funzione');
        $this->insertAss($pquery, 'destinatari', 'id_destinatario');
        $this->insertAss($pquery, 'sessioni', 'id_sessione');
        $this->insertAss($pquery, 'presentazione', 'numero_mese');        
        $this->insertAssOrgani($pquery);
        //die();
        return (int)$pquery['id_documento'];
    }

    public function insertDocCensito($pquery): int
    {
        $pquery['id_documento'] =  $this->insertdocumento($pquery);

        //die();
        $pquery['id_doc_info'] = $this->insertdocInfo($pquery);
        //   echo $pquery['id_doc_info'];

        $this->insertAss($pquery, 'funzioni', 'id_funzione');
        $this->insertAss($pquery, 'contributrici', 'id_funzione');
        $this->insertAss($pquery, 'destinatari', 'id_destinatario');
        $this->insertAss($pquery, 'sessioni', 'id_sessione');
        $this->insertAss($pquery, 'presentazione', 'numero_mese');
        $this->insertAssOrgani($pquery);
        //die();
        return $pquery['id_documento'];
    }





    public function updateDocInRiunione($pquery): int
    {
        $pquery['id_riunione'] =  $this->creaCopia($pquery);

        $this->updateDocInfoRiunioni($pquery);
        $this->insertAssRiunione($pquery, 'funzioni', 'id_funzione');
        $this->insertAssRiunione($pquery, 'destinatari', 'id_destinatario');
        $this->insertAssRiunione($pquery, 'sessioni', 'id_sessione');

        return $this->creaCopia($pquery);
    }



    public function _newcreaCopia($pquery): int
    {
        $anno = $pquery['anno'];
        $mese = $pquery['mese'];
        $id_organo = $pquery['id_organo'];
        $id_doc_info = $pquery['id_doc_info'];
        $uid_modifica = $pquery['uid_modifica'];

        $where = ['id_organo' => $id_organo, 'mese' => $mese, 'anno' => $anno];
        $tabella = 'riunioni';
        $coloums = ['id_riunione'];
        $indice = 'id_riunione';
        $id_riunione = $this->_selectIf($tabella, $coloums, $where, $indice);
        if (!$id_riunione) {
            $values = ['id_organo' => $id_organo, 'mese' => $mese, 'anno' => $anno, 'uid_modifica' => $uid_modifica];
            $id_riunione = $this->_insert($tabella, $values, $indice);
        }
        /*  var_dump($id_riunione);
        die();*/

        //doc_info_riunioni r where r.id_doc_info = p_id_doc_info and r.id_riunione = n_id_riunione);
        $tabella = 'doc_info_riunioni';
        $coloums = ['id_riunione'];
        $where = ['id_doc_info' => $id_doc_info, 'id_riunione' => $id_riunione];
        $indice = 'id';

        $id_doc_info_riunioni = $this->_selectIf($tabella, $coloums, $where, $indice);
        if (!$id_doc_info_riunioni) {


            $tabella = 'doc_info_riunioni';
            $where = ['id_doc_info' => $id_doc_info, 'id_riunione' => $id_riunione];
            $this->_delete($tabella, $where);
            $coloums = [
                'id_doc_info',
                'id_riunione',
                'id_documento', 'periodicita',
                'titolo', 'descrizione',
                'norma_esterna', 'norma_interna',
                'finalita', 'uid_modifica',
                'data_modifica', 'cod_tipo_doc'
            ];
            $where = ['id_doc_info' => $id_doc_info];
            $this->_insertSelect($tabella, $coloums, $where);


            $tabella = 'assoc_riunioni_doc_info';
            $where = ['id_doc_info' => $id_doc_info, 'id_riunione' => $id_riunione];
            $this->_delete($tabella, $where);
            $values = ['id_riunione' => $id_riunione, 'id_doc_info' => $id_riunione, 'uid_modifica' => $id_riunione];
            $indice = 'id_riunione';
            $this->_insert($tabella, $values, $indice);

            $assoc_array = ['funzioni', 'destinatari', 'sessioni'];
            $assoc_id_array = ['id_funzione', 'id_destinatario', 'id_sessione'];

            foreach ($assoc_array as $k => $tab) {
                $tabella = 'assoc_doc_info_' . $tab;
                $where = ['id_doc_info' => $id_doc_info, 'id_riunione' => $id_riunione];
                $this->_delete($tabella, $where);
                $coloums = ['id_riunione', $assoc_id_array[$k], 'id_doc_info', 'uid_modifica'];
                $where = ['id_doc_info' => $id_doc_info, 'id_riunione' => null];
                $this->_insertSelect($tabella, $coloums, $where);
            }
        }
        return $id_riunione;
    }

    public function updateAttivo($pquery): int
    {
        
        $id_doc_info = $pquery['id_doc_info'];
        $attivo =  $pquery['attivo']? 0 : 1;
        $uid_modifica = $pquery['uid_modifica'];
        $tableName = 'doc_info';

        $values = ['attivo' => $attivo, 'uid_modifica'=> $uid_modifica ];
        $where = ['id_doc_info' => $id_doc_info];
        $ret = $this->_update($tableName, $values, $where);
        return$attivo;
    }




    /*
BEGIN
DECLARE v_id_riunione int DEFAULT NULL;
DECLARE n_id_riunione int DEFAULT NULL;
DECLARE v_doc_info_riunioni int DEFAULT NULL;

START TRANSACTION;

SET v_id_riunione := (select id_riunione from riunioni r where r.id_organo=p_id_organo and r.mese = p_mese and r.anno = p_anno);

IF(v_id_riunione IS NULL) THEN
    insert into riunioni (id_organo, mese, anno) values (p_id_organo,p_mese,p_anno);
END IF;

SET n_id_riunione := (select id_riunione from riunioni r where r.id_organo=p_id_organo and r.mese = p_mese and r.anno = p_anno);
SET o_id_riunione := n_id_riunione;
-- SELECT concat('n_id_riunione is ', n_id_riunione);
SET v_doc_info_riunioni := (select id from doc_info_riunioni r where r.id_doc_info = p_id_doc_info and r.id_riunione = n_id_riunione);

IF(v_doc_info_riunioni IS NULL) THEN    

-- SELECT concat('v_doc_info_riunioni is ', v_doc_info_riunioni);

delete from doc_info_riunioni where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO doc_info_riunioni
(id_doc_info, id_riunione, id_documento, periodicita, titolo, descrizione, norma_esterna, norma_interna, finalita, uid_modifica, data_modifica,cod_tipo_doc)
(SELECT di.id_doc_info, n_id_riunione id_riunione, di.id_documento, di.periodicita, di.titolo, di.descrizione, di.norma_esterna, di.norma_interna, di.finalita, di.uid_modifica, di.data_modifica, 
di.cod_tipo_doc
FROM  doc_info di 
where di.id_doc_info = p_id_doc_info);

delete from assoc_riunioni_doc_info where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO assoc_riunioni_doc_info (id_riunione, id_doc_info, uid_modifica)
values (n_id_riunione, p_id_doc_info,'B07128');

delete from assoc_doc_info_funzioni where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO assoc_doc_info_funzioni (id_riunione, id_funzione, id_doc_info, uid_modifica)
(SELECT n_id_riunione id_riunione, di.id_funzione,di.id_doc_info,'B07128'
FROM assoc_doc_info_funzioni di 
where di.id_doc_info = p_id_doc_info and di.id_riunione is null);

delete from assoc_doc_info_destinatari where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO assoc_doc_info_destinatari 
(id_riunione, id_destinatario,id_doc_info, uid_modifica)
(SELECT
n_id_riunione id_riunione, di.id_destinatario,di.id_doc_info,'B07128'
FROM assoc_doc_info_destinatari di 
where di.id_doc_info = p_id_doc_info and di.id_riunione is null);

delete from assoc_doc_info_sessioni where id_doc_info = p_id_doc_info and id_riunione = n_id_riunione; 
INSERT INTO assoc_doc_info_sessioni
(id_riunione, id_sessione,id_doc_info, uid_modifica)
(SELECT n_id_riunione id_riunione, di.id_sessione ,di.id_doc_info,'B07128'
FROM assoc_doc_info_sessioni di
inner join sessioni se on se.id_sessione = di.id_sessione and se.id_organo = p_id_organo
where di.id_doc_info = p_id_doc_info and di.id_riunione is null);

END IF;
COMMIT;

END


*/


    public function creaCopia($pquery): int
    {
        $anno = $pquery['anno'];
        $mese = $pquery['mese'];
        $id_organo = $pquery['id_organo'];
        $id_doc_info = $pquery['id_doc_info'];

        $stmt = $this->_adapter->createStatement();
        $stmt->prepare('CALL _pinsert_doc_riunione(' . $id_doc_info . ', ' . $id_organo . ', ' . $mese . ', ' . $anno . ',@id_riunioni);');
        $stmt->execute();

        $stmt2 = $this->_adapter->createStatement();
        $stmt2->prepare("SELECT @id_riunioni AS id_riunione");
        $result = $stmt2->execute();
        $riunione = $result->current();

        return (int) $riunione['id_riunione'];
    }

    public function updatePesoDoc($pquery): int
    {
        $peso = $pquery['peso'];
        $id_doc_info = $pquery['id_doc_info'];
        $id_riunione =  $pquery['id_riunione '];
        $tableName = 'assoc_riunioni_doc_info';
        $uid_modifica = $pquery['uid_modifica'];
        $values = ['peso' => $peso, 'uid_modifica' => $uid_modifica];
        $where = ['id_doc_info' => $id_doc_info, 'id_riunione' => $id_riunione];
        $ret = $this->_update($tableName, $values, $where);
        return $ret;
    }

    public function updateFlagRicevuto($pquery): int
    {
        $flag_ricevuto = $pquery['flag_ricevuto'] ? 0 : 1;
        $id_doc_info = $pquery['id_doc_info'];
        $id_riunione =  $pquery['id_riunione'];
        $uid_modifica = $pquery['uid_modifica'];
        $tableName = 'assoc_riunioni_doc_info';

        $values = ['flag_ricevuto' => $flag_ricevuto, 'uid_modifica' => $uid_modifica];
        $where = ['id_doc_info' => $id_doc_info, 'id_riunione' => $id_riunione];
        $ret = $this->_update($tableName, $values, $where);
        return $ret;
    }


    public function getdoctipo($pquery, $tipo)
    {
        $tableName = '_vgetdocinfo' . $tipo;
        $anno = $pquery['anno'];
        $mese = $pquery['mese'];
        $id_organo = $pquery['id_organo'];
        //$uid_modifica = $pquery['uid_modifica'];
        $id_doc_info = $pquery['id_doc_info'];

        $where = ['id_doc_info' => $id_doc_info, "(anno is null or anno = " . $anno . ")", 'mese' => $mese, 'id_organo' => $id_organo];
        $columns = ['*'];

        return $this->_selectRow($tableName, $columns, $where);
    }


    public function getdoc($pquery)
    {
        $doc = $this->getdoctipo($pquery, 'riunione');
        if (!$doc) {
            $doc = $this->getdoctipo($pquery, 'previsti');
        }

        return $doc;
    }





    public function insertRiunione($pquery): int
    {
        $tableName = 'riunioni';

        $anno = $pquery['anno'];
        $mese = $pquery['mese'];
        $id_organo = $pquery['id_organo'];
        $uid_modifica = $pquery['uid_modifica'];

        $where = ['anno' => $anno, 'mese' => $mese, 'id_organo' => $id_organo];
        $columns = ['id_riunione'];
        $indice = 'id_riunione';
        $id_riunione = $this->_selectIf($tableName, $columns, $where, $indice);

        if (!$id_riunione) {
            $values = ['anno' => $anno, 'mese' => $mese, 'id_organo' => $id_organo, 'uid_modifica' => $uid_modifica];
            $id_riunione =  $this->_insert($tableName, $values, $indice);
        }

        return (int) $id_riunione;
    }



    public function updateDataRiunione($pquery): int
    {
        $data_ora = $pquery['data_riunione'];
        $n_verbale = $pquery['n_verbale'] ? $pquery['n_verbale'] : null;
        $id_riunione =  $pquery['id_riunione'];
        $uid_modifica = $pquery['uid_modifica'];
        $tableName = 'riunioni';

        $values = ['data_ora' => $data_ora, 'n_verbale' => $n_verbale, 'uid_modifica' => $uid_modifica];
        $where = ['id_riunione' => $id_riunione];
        $ret = $this->_update($tableName, $values, $where);

        return $ret;
    }

    public function updateDocInfoRiunioni($pquery): int
    {
        $tableName = "doc_info_riunioni";
        $id_doc_info = $pquery['id_doc_info'];
        $uid_modifica = $pquery['uid_modifica'];
        $titolo = $pquery['titolo'] ? $pquery['titolo'] : null;
        $descrizione = $pquery['descrizione'] ? $pquery['descrizione'] : null;
        $norma_esterna = $pquery['norma_esterna'] ? $pquery['norma_esterna'] : null;
        $norma_interna = $pquery['norma_interna'] ? $pquery['norma_interna'] : null;
        $finalita = $pquery['finalita'] ? $pquery['finalita'] : null;
        $id_riunione = $pquery['id_riunione'] ? $pquery['id_riunione'] : null;
        $cod_tipo_doc = $pquery['cod_tipo_doc'] ? $pquery['cod_tipo_doc'] : null;



        $values = [
            'titolo' => $titolo, 'descrizione' => $descrizione,
            'norma_esterna' => $norma_esterna, 'norma_interna' => $norma_interna, 'finalita' => $finalita, 'uid_modifica' => $uid_modifica,
            'cod_tipo_doc' => $cod_tipo_doc
        ];
        $where = ['id_doc_info' => $id_doc_info, 'id_riunione' => $id_riunione];
        $ret = $this->_update($tableName, $values, $where);

        return $ret;
    }


    public function insertdocumento($pquery): int
    {
        $tableName = "documenti";
        $indice = "id_doc_info";
        $uid_modifica = $pquery['uid_modifica'];
        $titolo = $pquery['titolo'] ? $pquery['titolo'] : null;
        $values = ['titolo' => $titolo, 'uid_modifica' => $uid_modifica];
        $ret = $this->_insert($tableName, $values, $indice);
        return (int) $ret;
    }


    public function insertdocInfo($pquery): int
    {
        $tableName = "doc_info";
        $indice = "id_doc_info";

        $values = [
            'id_documento' => $pquery['id_documento'],
            'periodicita' => $pquery['periodicita'] ? $pquery['periodicita'] : 0,
            'titolo' => $pquery['titolo'] ? $pquery['titolo'] : null,
            'descrizione' => $pquery['note'] ? $pquery['note'] : null,
            'norma_esterna' => $pquery['norma_esterna'] ? $pquery['norma_esterna'] : null,
            'norma_interna' => $pquery['norma_interna'] ? $pquery['norma_interna'] : null,
            'finalita' => $pquery['finalita'] ? $pquery['finalita'] : null,
            'note_presentazione' => $pquery['note_presentazione'] ? $pquery['note_presentazione'] : null,
            'uid_modifica' => $pquery['uid_modifica'] ? $pquery['uid_modifica'] : null,
            'cod_tipo_doc' => $pquery['cod_tipo_doc'] ? $pquery['cod_tipo_doc'] : null
        ];
        $ret = $this->_insert($tableName, $values, $indice);
        return (int) $ret;
    }
}
