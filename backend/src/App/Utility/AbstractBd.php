<?php

declare(strict_types=1);

namespace App\Utility;

use phpDocumentor\Reflection\Types\Array_;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;

class AbstractBd
{
    protected $_adapter;
    protected $_sql;

    public function __construct(Adapter $adapter)
    {
        $this->_adapter = $adapter;
        $this->_sql = new Sql($this->_adapter);
    }








    public function _selectIf($tableName, $columns, $where, $indice): int
    {

        $select = $this->_sql->select($tableName)->columns($columns)->where($where);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $riunione = $result->current();
        return (int) $riunione[$indice];
    }

    public function _selectRow($tableName, $columns, $where)
    {

        $select = $this->_sql->select($tableName)->columns($columns)->where($where);
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        // echo $this->_sql->getSqlStringForSqlObject($select);
        $ret = $result->current();
        return  $ret;
    }

    public function _selectAll($tableName, $columns, $where)
    {

        $select = $this->_sql->select($tableName)->columns($columns)->where($where);
        
        $statement = $this->_sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        $resultSet = new \Zend\Db\ResultSet\ResultSet;
        $resultSet->initialize($result);
               
        return $resultSet->toArray();   
    }


    public function _update($tableName, $values, $where): int
    {
        $update = $this->_sql->update($tableName)->set($values)->where($where);
        $statement = $this->_sql->prepareStatementForSqlObject($update);
        $statement->execute();
        return 1;
    }


    public function _insert($tableName, $values, $indice): int
    {
        $insert = $this->_sql->insert($tableName)->values($values);
        $statement = $this->_sql->prepareStatementForSqlObject($insert);
        $statement->execute();
        return (int) $this->_adapter->getDriver()->getLastGeneratedValue($indice);
    }

    public function _insertSelect($tableName, $columns, $where): int
    {

        $select = $this->_sql->select($tableName)->columns($columns)->where($where);
        $insert = $this->_sql->insert($tableName)->columns($columns)->select($select);

        $statement = $this->_sql->prepareStatementForSqlObject($insert);
        $statement->execute();
        return 1;
    }

    public function _beginTransaction()
    {
        $this->_adapter->getDriver()->getConnection()->beginTransaction();
    }

    public function _commit()
    {
        $this->_adapter->getDriver()->getConnection()->commit();
    }



    public function _delete($tableName, $where): int
    {
        $delete = $this->_sql->delete($tableName)->where($where);
        $statement = $this->_sql->prepareStatementForSqlObject($delete);
        $statement->execute();
        return 1;
    }
}
