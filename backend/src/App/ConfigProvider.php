<?php

declare (strict_types = 1);

namespace App;

/**
 * The configuration provider for the App module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{

    /**
     * Returns the configuration array
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array<string>
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'input_filter_specs' => $this->getInputFilter()
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array<string>
     */
    public function getDependencies(): array
    {
        return [
            'invokables' => [
                // here the invokables of the app
            ],
            'factories' => [
                // here the factory of the app
            ],
        ];
    }

    /**
     * Undocumented function
     *
     * @return string[]
     */
    public function getInputFilter(): array
    {
        return [
            // Example of form filter/validation
            //
            // '<model-name>' => [
            //     [
            //         'name' => '<field-name>',
            //         'filters' => [
            //             ['name' => 'StringTrim']
            //         ],
            //         'validators' => [
            //             [
            //                 'name' => 'StringLength',
            //                 'options' => [
            //                     'min' => 8
            //                 ],
            //             ]
            //         ],
            //     ],
            //     [
            //         'name' => '<field-name>',
            //         'required' => true,
            //         'filters' => [
            //             ['name' => 'StringTrim']
            //         ],
            //     ],
            // ],
        ];
    }
}
